# Feedback on assignment 4

- Four levels seemed a bit much maybe? - Have a look
- Rephrase concepts from description into more descriptive names of the features themselves
  - Procedures
  - User -> User model
  - type -> Interaction model
- Add a level to the technical user part
- Link Technical user with UI through a dependency relationship, and other where dependencies can be more descriptive.
- Ask yourself "What can we provide to the consumer?" when creating the model
