# Project Description

[Back to top](./README.md)

The purpose of this project is to create an architecture for a robotic **rover** system, modeled after the rovers’ autonomous movement capabilities, and to implement a simple demonstrator of this system.

The system is meant to give _non-technical_ operators the capability to monitor the status of the rovers’ environment, with information coming from different sources. These sources are **proximity sensors**, **GPS sensors** and a **front-mounted camera** on top of the rover. Communication with the rover can be done through a **networking device**, which can be used for both communication with the **central station** and with other rovers.

The robots should _perform_ a set of possible **missions**, which _award_ **points**. The missions are position-based in the form of sequences, which are fulfilled when all of the **positions** have been _visited_ by the robot. Every mission can be fulfilled using different **strategies**, which may change over time. These strategies refer to the order of which the positions are visited.

Robots can perform these missions in different **environments**, which are closed spaces limited by **boundaries**. Each environment is partitioned into **areas**, which in turn can be nested together. Areas come in two types - physical areas and logical areas - both of which can coincide with each other.

The information sent to **interfaces** should _describe_ the position of the robots within the environment, which should be sufficient for different kinds of interfaces, graphical, voice-based, etc… The interfaces themselves are passive and only show representative data and meant for _non-technical_ operators.

As these interfaces are only **observable**, there is no possible interaction with the system. Therefore, there should also be some form of **actionable** interface for which _technical_ operators can interact and manage the ROVU platform.

Points are computed in 20 second intervals and are dependent on the **procedure** which the system uses at that time. These procedures can be of two kinds - A or B - where procedure A is based on physical areas, and procedure B is based on logical areas. Different amounts points are given depending on the **priority** of the area the robot is within. The system always starts in procedure A. The change rules are as given below:

- If the current procedure is procedure A and there is at least a robot inside a logical area, then the procedure should change to procedure B.
- If the current procedure is procedure B and there is at least a robot inside a physical area, then the procedure should change to procedure A.

The ROVU platform should also _maintain_ a desired degree of **resilience**:

- If there is something wrong with a rover it should handle it based on the “type of fault” - wheel fault, camera dysfunction, etc.
- The system should also support run-time changes during missions, such as stopping the whole mission due to some unpredicted circumstance.
