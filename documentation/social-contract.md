# Social Contract

[Back to top](./README.md)

- [Social Contract](#social-contract)
  - [Responsibilities of a Team Member](#responsibilities-of-a-team-member)
  - [Meetings](#meetings)
  - [Goals and Objectives of the Team](#goals-and-objectives-of-the-team)

## Responsibilities of a Team Member

- A team member should be on time (max 10 minutes late), if the team member is late 5 times or more that team member should bring cookies. 
- If one is late, the person should inform the other members at least 10 minutes before the meeting begins.
- “Daily Scrum” every meeting where the team members say: 1) What they have done so far, 2) What they are working with, 3) If they need help.
- We will give continuous, constructive feedback on each members' performance
  - If a member won't adapt to the feedback given, further actions will be taken
- Respect each other
  - We have zero tolerance for bullying
  - Listen to each other and don't interrupt each other
- All members should be up to date with the course material, and feel free to ask the others for a recap/further details if there is something they don’t fully understand.

## Meetings

- Whole group meetings will be held twice a week
  - Once on Mondays between 08.00-12.00 am
  - Once on Wednesdays between 08.00-12.00 am
- When a discussion is held on a specific subject the following rules apply:
  - Switching subject is prohibited until the current discussion is finished
  - No request for the right of speech will be made if it will be unrelated

## Goals and Objectives of the Team

- The majority of the group feels like passing the grade should be the main goal, but that higher grades would be welcome.
- Those who are striving for higher grades are encouraged to do so, but can’t demand this from the other team members.