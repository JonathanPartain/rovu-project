# Feedback on assignment 1

- Gather deltas between the models and update towards the end of the course for the report.
- Include more details regarding the faults of the rover.
  - Add types of faults as conceptual inheritance.
- Good discussion regarding the separation of strategy and missions.
- Use Cases are good, but:
  - There is no clear use case describing when the mission is executed.
  - Maybe add more use cases, there is no "hard" use case limit.

## Assignment 2

- Operations = methods on the interfaces
- Try to think about operations to help with the specification of different components.
- Don't be afraid to experiment with the model and fail!
