# Feedback on assignment 2

Add new member use cases into our model.

## Component diagram

- Very big and complicated component diagram - not all has to be covered until the end.
- Make sure that the elicited use cases are incorporated into the class diagram.
- May need to implement a user interface - kill me...
- Make things clearer and follow the interface paths for each use case.
- Clarify the http-requests api for the user interface by modelling two functions and comments.
- Remove unnecessary components.
