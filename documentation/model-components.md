# Model components

[Back to top](./README.md)

This document describes the different components which are necessary to construct the different kinds of models for the project.

- [Model components](#model-components)
  - [Concepts](#concepts)
  - [Actors](#actors)
  - [Use Cases](#use-cases)
    - [Supply mission](#supply-mission)
    - [Execute objective](#execute-objective)
    - [Act on fault](#act-on-fault)
    - [Connect to Central station](#connect-to-central-station)
    - [Stop rover](#stop-rover)
    - [Assign strategy](#assign-strategy)
    - [Visualise user interface](#visualise-user-interface)

## Concepts

- Central system
- Rover
  - Senses
    - Satellite sensor
    - Proximity sensor
    - Front-mounted camera
  - Networking device
- Position
- Point
- Boundary
- Environment
- Area
  - Physical
  - Logical
- Interface
  - Observerable
  - Actionable
- Mission
  - Objective
- Strategy
- Procedure
  - A (Physical)
  - B (Logical)
- Resilience
  - Fault recognition
  - Run-time Mission change

## Actors

- Non-technincal operators
- Technical operators
- Rover

## Use Cases

### Supply mission

- Name: Supply mission
- Actor: Technical operator
- Goal: Add the given mission to the assignment queue.
- Description: The technical operator supplies a defined mission to the system. If the mission is valid, the mission is added to assignment queue to be assigned to the optimal rover.

### Execute objective

- Name: Execute objective
- Actor: Rover
- Goal: Have to rover execute a mission.
- Description: A rover signals the central station that it is ready for its next assignment. If there is a fitting mission available, or if the rover's current mission still has objectives, it is assigned to the rover. If a strategy is supplied with the mission/objective, the rover's strategy is changed accordingly.

### Act on fault

- Name: Act on fault
- Actor: Rover
- Goal: Have the rover act accordingly to its presented fault.
- Description: A rover notices a system fault which affects mission execution. The rover determines the optimal action and acts accordingly, if the action is critical to the mission or the rover's health, the central station and operators are informed.

### Connect to Central station

- Name: Connect to Central station
- Actor: Non-technical- and technical operator
- Goal: View (and interact) with the state of the system through the interface
- Description: The operator identifies themself and requests to connect to the central station. The system verifies the operator and displays valuable information to the interface.

### Stop rover

- Name: Stop rover
- Actor: Technical operator
- Goal: Abort mission execution and stop the given rover
- Description: The operator gives the command to stop the rovers execution. The system requests the rover to stop further execution

### Assign strategy

- Name: Assign strategy
- Actor: Technical operator
- Goal: Get a specific mission to be executed using the given strategy
- Description: The technical operator supplies a defined strategy for a specific mis-sion to the system. If the strategy is valid, the strategy is added to the mission andthen applied the next time a rover is going to execute an objective of that mission.

### Visualise user interface

- Name: Visualise information of rovers
- Actor: Any operator
- Goal: Get information about a mission in a visualised manner
- Description: An operator selects which mission they want information about. This selection is sent to the central station, which then responds with the requested information. The response is then visualised to the operator.
