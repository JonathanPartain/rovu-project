# ROVU-project

A platform software with the purpose of monitoring robots with regards to their movement capabilities and the environment surrounding them.

## Installation

To install the project, you need to have gradle installed. If you are using IntelliJ, then this should already be bundled together with the IDE.
Otherwise, you can find it at <https://gradle.org/install/>.

If you are using IntelliJ, then you only need to open the project and the IDE will recognize that it is a gradle project and automatically install the needed dependencies.

For other IDEs, please check out their documentation for gradle support, or run the gradle CLI.

## Useful links

- [Social Contract](documentation/social-contract.md)  
- [Project Description](documentation/project-description.md) 
- [Model Components](documentation/model-components.md)
- [Git workflow guide](./git-workflow.md)
