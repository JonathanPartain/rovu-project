package rovu.centralstation.information;

public interface Proxy<I, O> {
  I decodeMessage(O message);
  O encodeMessage(I response);
}
