package rovu.centralstation.information;

import rovu.centralstation.Timer;
import rovu.centralstation.TimerObserver;
import rovu.communication.Connection;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class MessageSender<R> implements TimerObserver {

  private final List<R> messages;
  private final List<Integer> delays;
  private final List<Connection<R>> connections;

  private final Timer timer;
  public MessageSender(Timer timer) {
    this.timer = timer;
    this.messages = new ArrayList<>();
    this.delays = new ArrayList<>();
    this.connections = new LinkedList<>();
  }

  @Override
  public void update(int currentTime) {
    for (int i = 0; i < delays.size(); i++) {
      if (delays.get(i) < currentTime) {
        sendMessage(messages.get(i));

        messages.remove(i);
        delays.remove(i);
        i--;
      }
    }
  }

  public void submitMessage(R message, int delay) {
    if (delay == 0) {
      sendMessage(message);
      return;
    }

    messages.add(message);
    delays.add(timer.getTime() + delay);
  }

  private void sendMessage(R message) {
    for (Connection<R> connection: connections) {
      connection.send(message);
    }
  }


  public void connect(Connection<R> connection) {
    this.connections.add(connection);
  }
}