package rovu.centralstation.information;

import rovu.centralstation.Layer;
import rovu.communication.Master;

public interface InformationLayer<E> extends Layer, Master<E> {
	void processRoverMessage(E message);
	void processUserMessage(E message);
}
