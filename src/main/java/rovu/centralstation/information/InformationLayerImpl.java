package rovu.centralstation.information;

import rovu.centralstation.Layer;
import rovu.communication.Connection;
import rovu.centralstation.TimerObservable;
import rovu.communication.Protocol;
import rovu.communication.Slave;

import java.util.*;

public class InformationLayerImpl<E> extends Observable implements InformationLayer<E>, Connection<E> {
	private final Proxy<Map<String, Object>, E> userProxy;
	private final Proxy<Map<String, Object>, E> roverProxy;
	private final Layer businessLayer;
	private final MessageSender<E> userMessageSender;
	private final MessageSender<E> roverMessageSender;

	public InformationLayerImpl(Proxy<Map<String, Object>, E> userProxy,
	                            Proxy<Map<String, Object>, E> roverProxy,
	                            Layer businessLayer,
	                            TimerObservable timer) {
		this.userProxy = userProxy;
		this.roverProxy = roverProxy;
		this.businessLayer = businessLayer;
		this.userMessageSender = new MessageSender<>(timer);
		timer.subscribe(userMessageSender);
		this.roverMessageSender = new MessageSender<>(timer);
		timer.subscribe(roverMessageSender);
	}

	public void processRoverMessage(E message) {
		Map<String, Object> encodedMessage = this.roverProxy.decodeMessage(message);
		this.businessLayer.processData(encodedMessage);
	}

	public void processUserMessage(E message) {
		Map<String, Object> decodedMessage = this.userProxy.decodeMessage(message);
		this.businessLayer.processData(decodedMessage);
	}

	@Override
	public void connect(Slave<E> connection) {
		this.roverMessageSender.connect(connection);
		connection.connect(this);
		Map<String, Object> connectionMessage = new HashMap<>();
		connectionMessage.put(Protocol.COMMAND, Protocol.Commands.ROVER_CONNECTION);
		this.businessLayer.processData(connectionMessage);
	}

	@Override
	public void processData(Map<String, Object> data) {
		switch ( (Protocol.Commands) data.get(Protocol.COMMAND) ) {
			case NEXT_OBJECTIVE:
				this.roverMessageSender.submitMessage(
						this.roverProxy.encodeMessage(data),
						(int) data.get(Protocol.DELAY)
				                                     );
				break;
			case ROVER_CONNECTION:
				this.roverMessageSender.submitMessage(
						this.roverProxy.encodeMessage(data),
						(int) data.get(Protocol.DELAY)
				                                     );

			case VISUALIZE:
				this.userMessageSender.submitMessage(
						this.userProxy.encodeMessage(data),
						(int) data.get(Protocol.DELAY)
				                                    );
			default:
				break;
		}
	}

	@Override
	public void send(E data) {
		this.processRoverMessage(data);
	}
}
