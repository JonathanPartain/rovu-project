package rovu.centralstation.information;

import rovu.communication.Connection;
import rovu.communication.Decoder;
import rovu.communication.Encoder;

public class ProxyImpl<I, E, T> implements Proxy<I, E>, Connection<E> {

  private final Decoder<E, I> dataDecoder;
  private final Encoder<I, E> dataEncoder;

  public ProxyImpl(Decoder<E, I> dataDecoder,
                   Encoder<I, E> dataEncoder) {
    this.dataDecoder = dataDecoder;
    this.dataEncoder = dataEncoder;
  }

  @Override
  public I decodeMessage(E message) {
    return dataDecoder.decode(message);
  }

  @Override
  public E encodeMessage(I response) {
    return dataEncoder.encode(response);
  }

  @Override
  public void send(E data) {

  }
}
