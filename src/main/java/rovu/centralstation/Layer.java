package rovu.centralstation;

import java.util.Map;

public interface Layer {
	void processData(Map<String, Object> data);
}
