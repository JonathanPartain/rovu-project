package rovu.centralstation;

public interface Timer {
    int getTime();
}
