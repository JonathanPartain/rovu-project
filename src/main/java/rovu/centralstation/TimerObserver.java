package rovu.centralstation;

public interface TimerObserver {
    void update(int time);
}
