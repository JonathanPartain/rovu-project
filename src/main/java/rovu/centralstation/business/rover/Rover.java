package rovu.centralstation.business.rover;

import org.la4j.Vector;
import rovu.centralstation.business.domain.Area;
import rovu.communication.SeverityLevel;

import java.util.LinkedList;
import java.util.Queue;


public class Rover {
  private final int roverId;
  private Queue<Vector> currentPath;
  private Queue<Queue<Vector>> paths;
  private Area currentPhysicalArea;
  private RoverStatus roverStatus;

  public static Rover create(int roverId) {
    return new Rover(roverId);
  }

  @Override
  public String toString() {
    return "Rover{" +
            "RoverId=" + roverId +
            ", CurrentPath=" + currentPath +
            ", Paths=" + paths +
            ", CurrentPhysicalArea=" + currentPhysicalArea +
            ", RoverStatus=" + roverStatus +
            '}';
  }

  public static class RoverStatus {
    private final Vector objective;
    private final Vector position;
    private final SeverityLevel faultSeverity;

    public static RoverStatus create() {
      return new RoverStatus(null, null, null);
    }

    private RoverStatus(Vector objective, Vector position, SeverityLevel faultSeverity) {
      this.objective = objective;
      this.position = position;
      this.faultSeverity = faultSeverity;
    }

    public Vector getObjective() {
      return this.objective;
    }

    public Vector getPosition() {
      return this.position;
    }

    public SeverityLevel getFaultSeverity() {
      return this.faultSeverity;
    }

    @Override
    public String toString() {
      return "RoverStatus{" +
              "Objective=" + objective +
              ", Position=" + position +
              ", FaultSeverity=" + faultSeverity +
              '}';
    }
  }

  public static RoverStatus createRoverStatus(Vector objective, Vector position, SeverityLevel fault) {
    return new RoverStatus(objective, position, fault);
  }

  public Area getCurrentPhysicalArea() {
    return this.currentPhysicalArea;
  }

  public void setCurrentPhysicalArea(Area newArea) {
    this.currentPhysicalArea = newArea;
  }

  public Queue<Vector> getCurrentPath() {
    return this.currentPath;
  }

  public void setCurrentPath(Queue<Vector> path) {
    this.currentPath = path;
  }

  public Queue<Queue<Vector>> getPaths() {
    return this.paths;
  }

  public void setPaths(Queue<Queue<Vector>> paths) {
    this.paths = paths;
  }

  public int getId() {
    return this.roverId;
  }

  public Vector getObjective() {
    return this.roverStatus.objective;
  }

  public Vector getPosition() {
    return this.roverStatus.position;
  }

  public SeverityLevel getFault() {
    return this.roverStatus.faultSeverity;
  }

  public RoverStatus getRoverStatus() {
    return roverStatus;
  }

  public void setRoverStatus(RoverStatus roverStatus) {
    this.roverStatus = roverStatus;
  }

  private Rover(int roverId) {
    this.roverId = roverId;
    this.currentPath = new LinkedList<>();
    this.paths = new LinkedList<>();
    this.roverStatus = RoverStatus.create();
  }
}

