package rovu.centralstation.business.rover;

import java.util.*;

import org.la4j.Vector;
import rovu.centralstation.business.domain.Area;
import rovu.communication.SeverityLevel;


public class RoverModel implements RoverObservable {

  private int roverCount = 0;
  private final SortedSet<Rover> rovers;
  private final List<RoverObserver> observers;

  public RoverModel(List<RoverObserver> list) {
    this.observers = list;
    this.rovers = new TreeSet<>(Comparator.comparingInt(Rover::getId));
  }

  public int createRover() {
    this.rovers.add(Rover.create(roverCount));
    System.out.println("Rover " + roverCount + " tracked");

    return roverCount++;
  }

  public Optional<Rover> getRover(int roverId) {
    for ( Rover rover : this.rovers ) {
      int id = rover.getId();
      if ( id == roverId ) {
        return Optional.of(rover);
      } else if ( id > roverId ) {
        break;
      }
    }
    return Optional.empty();
  }

  public Map<Integer, Rover> getAllRovers() {
    Map<Integer, Rover> result = new HashMap<>();

    for ( Rover r : this.rovers ) {
      result.put(r.getId(), r);
    }

    return result;
  }

  public List<Rover> getListOfRovers(){
    List<Rover> rovers = new ArrayList<>();

    for(Rover r: this.rovers){
      rovers.add(r);
    }

    return rovers;
  }




  public void notifyObservers(Rover newState) {
    for ( RoverObserver observer : this.observers ) {
      observer.onRoverChange(newState);
    }
  }

  public void subscribe(RoverObserver observer) {
    this.observers.add(observer);
  }

  public void unsubscribe(RoverObserver observer) {
    this.observers.remove(observer);
  }


  public void assignPathSchedule(int roverId, Queue<Vector> currentPath, Queue<Queue<Vector>> paths) {
    this.getRover(roverId).ifPresent(rover -> {
      rover.setCurrentPath(currentPath);
      rover.setPaths(paths);
    });
  }

  public void setCurrentArea(int roverId, Area newArea) {
    this.getRover(roverId).ifPresent(rover -> rover.setCurrentPhysicalArea(newArea));
  }

  public List<Vector> getLocations() {
    List<Vector> result = new ArrayList<>();

    for ( Rover r : this.rovers ) {
      result.add(r.getPosition());
    }

    return result;
  }

  public void setRoverStatus(int roverId, Vector position, Vector objective, SeverityLevel fault) {
    getRover(roverId).ifPresent(rover -> {
      rover.setRoverStatus(Rover.createRoverStatus(objective, position, fault));
      this.notifyObservers(rover);
    });
  }
}
