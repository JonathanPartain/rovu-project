package rovu.centralstation.business.rover;

public interface RoverObservable {

     void notifyObservers(Rover newState);
     void subscribe(RoverObserver observer);
     void unsubscribe(RoverObserver observer);
}
