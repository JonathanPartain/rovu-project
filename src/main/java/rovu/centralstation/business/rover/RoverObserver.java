package rovu.centralstation.business.rover;

public interface RoverObserver {

     void onRoverChange(Rover newState);
}
