package rovu.centralstation.business;

import org.la4j.Vector;
import rovu.centralstation.Layer;
import rovu.centralstation.business.mission.strategy.StrategyType;

public class AssignmentController {

	private final StrategyAdministrator strategyAdministrator;
	private final MissionAdministrator missionAdministrator;


	public AssignmentController(StrategyAdministrator strategyAdministrator, MissionAdministrator missionAdministrator) {
		this.strategyAdministrator = strategyAdministrator;
		this.missionAdministrator = missionAdministrator;
	}

	public void setOwnerLayer(Layer layer) {
		this.missionAdministrator.setLayerToNotify(layer);
	}

	public void assignStrategy(int missionID, StrategyType strategy) {
		strategyAdministrator.assignStrategy(missionID, strategy);
	}

	public void supplyMission(Vector[] mission) {
		missionAdministrator.supplyMission(mission);
	}

	public void supplyMission(Vector[] mission, StrategyType strategy) {
		missionAdministrator.supplyMission(mission, strategy);
	}
}
