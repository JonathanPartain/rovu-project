package rovu.centralstation.business;

import org.la4j.Vector;
import rovu.centralstation.business.mission.strategy.StrategyType;
import rovu.util.Pair;

public interface MissionProvider {
	 void supplyMission(Vector[] mission);
	 void supplyMission(Vector[] mission, StrategyType strategy);

	 Pair<Vector, Integer> nextObjective(int roverId);
}
