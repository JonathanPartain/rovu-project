package rovu.centralstation.business;

import org.la4j.Vector;
import rovu.centralstation.Layer;
import rovu.communication.Protocol;
import rovu.communication.SeverityLevel;

import java.util.HashMap;
import java.util.Map;

public class RoverController {

    private final RoverProvider roverProvider;
    public RoverController(RoverProvider roverProvider){
        this.roverProvider = roverProvider;
    }

    public void updateRoverStatus(int roverId, Vector position, Vector objective, SeverityLevel faultSeverity){
        this.roverProvider.setRoverStatus(roverId, position, objective, faultSeverity);
    }

    public Map<String, Object> connectRover() {
        int roverId = this.roverProvider.addRover();
        Map<String, Object> roverIdResponse = new HashMap<>();
        roverIdResponse.put(Protocol.COMMAND, Protocol.Commands.ROVER_CONNECTION);
        roverIdResponse.put(Protocol.DELAY, 0);
        roverIdResponse.put(Protocol.ROVER_ID, roverId);
        return roverIdResponse;
    }

}
