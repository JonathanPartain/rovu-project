package rovu.centralstation.business;

import org.la4j.Vector;
import rovu.centralstation.Layer;
import rovu.centralstation.TimerObserver;
import rovu.centralstation.business.mission.strategy.StrategyType;
import rovu.communication.Protocol;
import rovu.communication.SeverityLevel;

import java.util.Map;

public class BusinessLayerImpl implements Layer{

	private final RoverController roverController;
	private final AssignmentController assignmentController;

	private Layer previousLayer;
	private int lastRewardCalculationTime;

	public BusinessLayerImpl(RoverController roverController, AssignmentController assignmentController) {
		this.roverController = roverController;
		this.assignmentController = assignmentController;
		this.lastRewardCalculationTime = 0;

		this.assignmentController.setOwnerLayer(this);
	}


    // Should be the informationLayer
	public void setPreviousLayer(Layer layer) {
		this.previousLayer = layer;
	}


	@Override
	public void processData(Map<String, Object> data) {
		if (data.containsKey(Protocol.COMMAND)) {
			int roverId;
			SeverityLevel faultSeverity = (SeverityLevel) data.get(Protocol.FAULT);
			Vector position = (Vector) data.get(Protocol.POSITION);
			Vector objective = (Vector) data.get(Protocol.OBJECTIVE);
			switch ((Protocol.Commands) data.get(Protocol.COMMAND)) {

				case ROVER_CONNECTION:
					Map<String, Object> response = roverController.connectRover();
					this.previousLayer.processData(response);
					break;

				case MISSION_ASSIGNMENT:
					Vector[] mission = (Vector[]) data.get(Protocol.MISSION);
					if (data.containsKey(Protocol.STRATEGY)) {
						StrategyType strategy = (StrategyType) data.get(Protocol.STRATEGY);
						assignmentController.supplyMission(mission, strategy);
					} else {
						assignmentController.supplyMission(mission);
					}
					break;

				case STRATEGY_UPDATE:
					int missionID = (int) data.get(Protocol.MISSION_ID);
					StrategyType strategy = (StrategyType) data.get(Protocol.STRATEGY);
					assignmentController.assignStrategy(missionID, strategy);
					break;

				case ROVER_UPDATE:
					roverId = (int) data.get(Protocol.ROVER_ID);
					roverController.updateRoverStatus(roverId, position, objective, faultSeverity);
					break;

				case STOP_ROVER:
					roverId = (int) data.get(Protocol.ROVER_ID);
					roverController.updateRoverStatus(roverId, position, null, faultSeverity);
					break;

				case NEXT_OBJECTIVE:
					this.previousLayer.processData(data);
					break;

				default:
					break;
			}
		}
	}
}
