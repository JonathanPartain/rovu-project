package rovu.centralstation.business;

import rovu.centralstation.business.mission.strategy.StrategyType;

public interface StrategyProvider {

	void assignStrategy(int missionId, StrategyType strategy);

}
