package rovu.centralstation.business.domain;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class MapFactory {
	public static Graph<Vector, DefaultEdge> createMap(Vector start, Vector end, double density) {
		Graph<Vector, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
		Vector[][] vectors = createVertices(start, end, density);
		addVertices(g, vectors);
		addEdges(g, vectors);

		return g;
	}

	public static Graph<Vector, DefaultEdge> createMapWithObstacle(Vector start, Vector end, double density, Set<Area> obstructedAreas) {
		Graph<Vector, DefaultEdge> g = createMap(start, end, density);
		removeObstructedVertices(g, obstructedAreas);
		removeObstructedEdges(g, obstructedAreas);
		return g;
	}

	private static void removeObstructedVertices(Graph<Vector, DefaultEdge> g, Set<Area> obstructedAreas) {
		for (Area area: obstructedAreas) {
			Set<Vector> locations = g.vertexSet()
					.stream()
					.filter(area::containsLocation)
					.collect(Collectors.toSet());
			
			g.removeAllVertices(locations);
		}
	}

	private static void removeObstructedEdges(Graph<Vector, DefaultEdge> g, Set<Area> obstructedAreas) {
		for (Area area: obstructedAreas) {
			Set<DefaultEdge> edges = g.edgeSet();
			Set<DefaultEdge> toRemove = new HashSet<>();

			for (DefaultEdge edge: edges) {
				Vector source = g.getEdgeSource(edge);
				Vector target = g.getEdgeTarget(edge);


				// We need the +0.1 to make sure the has some kind of thickness
				Line2D edgeLine;
				if (source.get(0) == target.get(0))
					edgeLine = new Line2D.Double(source.get(0), source.get(1), target.get(0)+0.1, target.get(1));
				else if (source.get(1) == target.get(1))
					edgeLine = new Line2D.Double(source.get(0), source.get(1), target.get(0), target.get(1)+0.1);
				else
					edgeLine = new Line2D.Double(source.get(0), source.get(1), target.get(0), target.get(1));

				Rectangle2D lineBounds = edgeLine.getBounds2D();

				Shape areaBound = area.getBoundary();
				if (area.getBoundary().intersects(lineBounds)) {
					toRemove.add(edge);
				}
			}
			
			g.removeAllEdges(toRemove);
		}
	}

	private static void addVertices(Graph<Vector, DefaultEdge> g, Vector[][] vectors) {
		int maxI = vectors.length;
		int maxJ = vectors[0].length;
		for (int i = 0; i < maxI; i++) {
			for (int j = 0; j < maxJ; j++) {
				g.addVertex(vectors[i][j]);
			}
		}
	}

	private static void addEdges(Graph<Vector, DefaultEdge> g, Vector[][] vectors) {
		int maxI = vectors.length;
		int maxJ = vectors[0].length;
		for (int i = 0; i < maxI; i++) {
			for (int j = 0; j < maxJ; j++) {
				Vector v = vectors[i][j];
				List<Vector> neighbors = new ArrayList<>();
				if (i + 1 < maxI) {
					Vector n = vectors[i + 1][j];
					neighbors.add(n);
				}
				if (j + 1 < maxJ) {
					Vector n = vectors[i][j + 1];
					neighbors.add(n);
				}
				for (Vector neighbor : neighbors) {
					g.addEdge(v, neighbor);
				}
			}
		}
	}

	private static Vector[][] createVertices(Vector start, Vector end, double density) {
		double startX = start.get(0);
		double startY = start.get(1);
		double endX = end.get(0);
		double endY = end.get(1);
		int xs = (int) Math.floor((endX - startX) / density) + 1;
		int ys = (int) Math.floor((endY - startY) / density) + 1;
		Vector[][] vectors = new Vector[xs][ys];
		for (int i = 0; i < xs; i++) {
			for (int j = 0; j < ys; j++) {
				double x = startX + i * density;
				double y = startY + j * density;
				vectors[i][j] = (new BasicVector(new double[]{x, y}));
			}
		}
		return vectors;
	}


}
