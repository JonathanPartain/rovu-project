package rovu.centralstation.business.domain;

import com.sun.org.apache.xml.internal.utils.DefaultErrorHandler;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import rovu.util.Pair;
import rovu.util.Triple;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

public class DomainModel {

	private final Set<Area> PHYSICAL_AREAS;
	private final Set<Area> LOGICAL_AREAS;
    private Set<Area> obstructedAreas = null;
	private final Map<String, Set<Area>> CATEGORIES;
	private final Graph<Vector, DefaultEdge> LOCATION_GRID;
	private final LocationMasker LOCATION_MASKER;

	public DomainModel(Set<Area> physical, Set<Area> logical, Map<String, Set<Area>> categories, Graph<Vector, DefaultEdge> map, LocationMasker locationMasker) {
		this.PHYSICAL_AREAS = physical;
		this.LOGICAL_AREAS = logical;
		this.CATEGORIES = categories;
		this.LOCATION_GRID = map;
		this.LOCATION_MASKER = locationMasker;
	}

	public DomainModel(File source) throws SAXException, ParserConfigurationException, IOException {
	    this.PHYSICAL_AREAS = new HashSet<>();
        this.LOGICAL_AREAS = new HashSet<>();
        this.CATEGORIES = new HashMap<>();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setIgnoringElementContentWhitespace(true);


        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        File xsd = new File("src/main/java/rovu/centralstation/business/domain/validationSchema.xsd");
        final Schema schema = sf.newSchema(xsd);
        dbf.setSchema(schema);
        DocumentBuilder db = dbf.newDocumentBuilder();
        db.setErrorHandler(new DefaultErrorHandler());
        Document doc = db.parse(source);

        // First node is the root element <domain>
        NodeList list = doc.getChildNodes();
        list = list.item(0).getChildNodes();

        Graph<Vector, DefaultEdge> map = null;
        int numberOfAllowedVisiter = 0;
        for(int i = 0; i < list.getLength(); i++)
        {
            Node node = list.item(i);

            if (node.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            switch (node.getNodeName()) {
                case "physicalAreas":
                    addAreaFromNode(getPhysicalAreas(), getCategories(), node);
                    break;
                case "logicalAreas":
                    addAreaFromNode(getLogicalAreas(), getCategories(), node);
                    break;
                case "map":
                    map = getMapFromNode(node);
                    break;
                case "locationMasker":
                    Element element = (Element) node;
                    numberOfAllowedVisiter = Integer.decode(element.getAttribute("numberOfAllowedVisitors"));
                    break;
                default:
                    // Could throw an error but already validated with an XMLSchema
                    break;

            }
        }

        this.LOCATION_GRID = map;

        List<String> areasNames = new ArrayList<>();
        this.PHYSICAL_AREAS.forEach(area -> areasNames.add(area.getName()));
        this.LOGICAL_AREAS.forEach(area -> areasNames.add(area.getName()));
        this.LOCATION_MASKER = new LocationMasker(areasNames, numberOfAllowedVisiter);
    }

	private void addAreaFromNode(Set<Area> areas, Map<String, Set<Area>> categories, Node node) {
        NodeList list = node.getChildNodes();

        for(int i = 0; i < list.getLength(); i++)
        {
            Node subNode = list.item(i);
            if (subNode.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            Shape shape = null;
            Element area = (Element)subNode;
            String name = area.getAttribute("name");
            String type = area.getTagName();
            switch (type) {
                case "polygon":
                    shape = getPolygon(list.item(i));
                    break;
                case "rectangle":
                    shape = getRectangle(list.item(i));
                    break;
                case "ellipse":
                    shape = getEllipse(list.item(i));
                    break;
                default:
                    // Could throw an error or parse the document with an XMLSchema
                    break;
            }

            Area tmp = new Area(shape, name);
            areas.add(tmp);

            String category = area.getAttribute("category");

            if (!category.isEmpty()) {
                if (categories.containsKey(category)) {
                    categories.get(category).add(tmp);
                } else {
                    Set<Area> categorySet = new HashSet<>();
                    categorySet.add(tmp);
                    categories.put(category, categorySet);
                }
            }


        }
    }

    private Graph<Vector, DefaultEdge> getMapFromNode(Node node) {
        NodeList list = node.getChildNodes();

        float density = 0;
        Vector start = null, end = null;
        Point2D.Float tmp = null;
        double[] pointArray = new double[2];
        Set<Area> obstructedAreas = new HashSet<>();


        for(int i = 0; i < list.getLength(); i++)
        {
            Node subNode = list.item(i);
            if (subNode.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            Element element = (Element)subNode;
            String name = element.getTagName();
            switch (name) {
                case "start":
                    tmp = getPoint(subNode);
                    pointArray = new double[2];
                    pointArray[0] = tmp.x;
                    pointArray[1] = tmp.y;

                    start = new BasicVector(pointArray);
                    break;
                case "end":
                    tmp = getPoint(subNode);
                    pointArray = new double[2];

                    pointArray[0] = tmp.x;
                    pointArray[1] = tmp.y;

                    end = new BasicVector(pointArray);
                    break;
                case "density":
                    density = Float.parseFloat(subNode.getTextContent());
                    break;
                case "obstructedAreas":
                    addAreaFromNode(obstructedAreas, getCategories(), subNode);
                    break;
            }
        }

        this.obstructedAreas = obstructedAreas;

        return MapFactory.createMapWithObstacle(start, end, density, obstructedAreas);
    }

    private Shape getPolygon(Node node) {
        Polygon result = new Polygon();

        List<Point2D.Float> points = new ArrayList<>();
        NodeList list = node.getChildNodes();

        for (int i = 0; i < list.getLength(); i++) {
            Node subNode = list.item(i);

            switch (subNode.getNodeName()) {
                case "point":
                    points.add(getPoint(subNode));
                    break;
            }
        }

        /*
            The reset MUST BE DONE
            cause the constructor new Polygon(); initialise the number of point to 4 by default...
            so it does not work when we have only 3 points (triangle)
         */
        result.reset();
        for(Point2D.Float p: points)
            result.addPoint((int)p.x, (int)p.y);

        return result;
    }

    private Shape getEllipse(Node node) {
        Triple<Point2D.Float, Float, Float> tmp = getPointWidthHeight(node);

        Point2D.Float topLeft = tmp.getFirst();
        float width = tmp.getSecond(), height = tmp.getThird();

        return new Ellipse2D.Double(topLeft.x, topLeft.y, width, height);
    }

    private Shape getRectangle(Node node) {
        Triple<Point2D.Float, Float, Float> tmp = getPointWidthHeight(node);

        Point2D.Float topLeft = tmp.getFirst();
        float width = tmp.getSecond(), height = tmp.getThird();

        return new Rectangle2D.Double(topLeft.x, topLeft.y, width, height);
    }

    private Triple<Point2D.Float, Float, Float> getPointWidthHeight(Node node) {
        Point2D.Float topLeft = null;
        float width = 0, height = 0;
        NodeList list = node.getChildNodes();

        for (int i = 0; i < list.getLength(); i++) {
            Node subNode = list.item(i);

            switch (subNode.getNodeName()) {
                case "point":
                    topLeft = getPoint(subNode);
                    break;
                case "width":
                    width = Float.parseFloat(subNode.getTextContent());
                    break;
                case "height":
                    height = Float.parseFloat(subNode.getTextContent());
                    break;
            }
        }

	    return new Triple<>(topLeft, width, height);
    }

	private Point2D.Float getPoint(Node node) {
	    Point2D.Float result = new Point2D.Float();
        NodeList list = node.getChildNodes();

        for (int i = 0; i < list.getLength(); i++) {
            Node subNode = list.item(i);

            switch (subNode.getNodeName()) {
                case "x":
                    result.x = Float.parseFloat(subNode.getTextContent());
                    break;
                case "y":
                    result.y = Float.parseFloat(subNode.getTextContent());
                    break;
            }
        }

        return result;
    }




	public Graph<Vector, DefaultEdge> getCurrentMap() {
		return this.LOCATION_MASKER.calculateMap(PHYSICAL_AREAS, this.LOCATION_GRID);
	}

	public void updateMap(List<Pair<Vector, Queue<Vector>>> roverToLocationList) {
		this.LOCATION_MASKER.updateMap(getPhysicalAreas(), roverToLocationList);
	}

	public Set<Area> getPhysicalAreas() {
		return PHYSICAL_AREAS;
	}

    public Set<Area> getLogicalAreas() {
        return LOGICAL_AREAS;
    }

    public Set<Area> getObstructedAreas() {
        return obstructedAreas;
    }

    public Map<String, Set<Area>> getCategories() {
        return CATEGORIES;
    }

	public Map<String, Set<Area>> getCategoryAreas() {
		return CATEGORIES;
	}

	public Triple<Set<Area>, Set<Area>, Map<String, Set<Area>>> getAllAreas() {
		return new Triple<>(PHYSICAL_AREAS, LOGICAL_AREAS, CATEGORIES);
	}

}
