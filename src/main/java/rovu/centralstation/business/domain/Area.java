package rovu.centralstation.business.domain;

import org.la4j.Vector;
import java.awt.*;

public class Area {
    private final Shape boundary;
    private final String name;

    // Shape could be Polygon, Rectangle2D, Ellipse2D
    public Area(Shape boundary, String name) {
        this.boundary = boundary;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean containsLocation(Vector location) {
        final double x = location.get(0);
        final double y = location.get(1);

        return this.boundary.contains(x, y);
    }

    public Shape getBoundary() {
        return this.boundary;
    }
}

