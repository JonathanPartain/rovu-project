package rovu.centralstation.business.domain;

import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.AsSubgraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.la4j.Vector;
import rovu.util.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class LocationMasker {
    private final Map<String, Integer> allowedVisitorsMap;

    public LocationMasker(List<String> areaNames, int numberOfAllowedVisitors) {
        this.allowedVisitorsMap = new HashMap<>();
        for (String name : areaNames) {
            allowedVisitorsMap.put(name, numberOfAllowedVisitors);
        }
    }

    // Returns a graph where the nodes in the areas with 0 allowedVisitorsMap are removed
    public Graph<Vector, DefaultEdge> calculateMap(Set<Area> physicalAreas, Graph<Vector, DefaultEdge> g) {
        Graph<Vector, DefaultEdge> subgraph = new SimpleGraph<>(DefaultEdge.class);
        Graphs.addGraph(subgraph, g);
        Set<Vector> occupiedNodes = new HashSet<>();
        for (Area area : physicalAreas) {
            int allowedVisitors = this.allowedVisitorsMap.get(area.getName());
            if (allowedVisitors == 0) {
                Set<Vector> locations = g.vertexSet().stream().filter(area::containsLocation).collect(Collectors.toSet());
                occupiedNodes.addAll(locations);
            }
        }
        subgraph.removeAllVertices(occupiedNodes);
        return subgraph;
    }

    // Updates the map of allowedVisitorsMap so that if a rover leaves an area the visitor count is increased
    // if a rover is in an area the count is decreased
    public void updateMap(Set<Area> physicalAreas, List<Pair<Vector, Queue<Vector>>> roverToLocationList) {
        Set<Vector> allLocations = getAllLocations(roverToLocationList);
        for (Area area : physicalAreas) {
            int numberOfAllowedVisitors = calculateAllowedNumberOfVisitors(area, allLocations);
            this.allowedVisitorsMap.replace(area.getName(), numberOfAllowedVisitors);
        }
    }

    private int calculateAllowedNumberOfVisitors(Area area, Set<Vector> locations) {
        int numberOfAllowedVisitors = 0;
        boolean isOneRoverInArea = false;
        for (Vector location : locations) {
            if (area.containsLocation(location)) {
                isOneRoverInArea = true;
            }
        }
        if (!isOneRoverInArea) {
            numberOfAllowedVisitors = 1;
        }
        return numberOfAllowedVisitors;
    }

    private Set<Vector> getAllLocations(List<Pair<Vector, Queue<Vector>>> roverToLocationList) {
        Set<Vector> allLocations = new HashSet<>();
        for (Pair<Vector, Queue<Vector>> rover : roverToLocationList) {
            allLocations.addAll(rover.getSecond());
            allLocations.add(rover.getFirst());
        }
        return allLocations;
    }
}
