package rovu.centralstation.business.domain;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.la4j.Vector;
import rovu.centralstation.business.domain.Area;

import java.util.Map;
import java.util.Set;

public interface DomainProvider {
    Set<Area> getPhysicalAreas();
    Set<Area> getLogicalAreas();
    Map<String, Set<Area>> getCategories();

    Graph<Vector, DefaultEdge> getCurrentMap();
}
