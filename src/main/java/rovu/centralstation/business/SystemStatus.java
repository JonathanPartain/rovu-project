package rovu.centralstation.business;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.la4j.Vector;
import rovu.centralstation.TimerObservable;
import rovu.centralstation.TimerObserver;
import rovu.centralstation.business.domain.Area;
import rovu.centralstation.business.domain.DomainModel;
import rovu.centralstation.business.domain.DomainProvider;
import rovu.centralstation.business.mission.strategy.Strategy;
import rovu.centralstation.business.mission.strategy.StrategyType;
import rovu.centralstation.business.reward.RewardModel;
import rovu.centralstation.business.mission.Mission;
import rovu.centralstation.business.mission.MissionModel;
import rovu.centralstation.business.rover.Rover;
import rovu.centralstation.business.rover.RoverModel;
import rovu.communication.SeverityLevel;
import rovu.util.Pair;

import java.util.*;

public class SystemStatus implements RoverProvider, MissionProvider, DomainProvider, StrategyProvider,
                                     TimerObserver, SystemStatusObservable {

  private final RoverModel roverModel;
  private final DomainModel domainModel;
  private final RewardModel rewardModel;
  private final MissionModel missionModel;


  private int timeSinceRewardCalculation;
  private List<SystemStatusObserver> observers;

  @Override
  public void supplyMission(Vector[] mission, StrategyType strategy) {
    this.missionModel.supplyMission(Arrays.asList(mission));
  }

  public SystemStatus(RoverModel roverM, DomainModel domainM, RewardModel rewardM, MissionModel missionM, TimerObservable timer) {
    this.roverModel = roverM;
    this.domainModel = domainM;
    this.rewardModel = rewardM;
    this.missionModel = missionM;
    timer.subscribe(this);
    observers = new ArrayList<>();

  }

  @Override
  public void supplyMission(Vector[] mission) {

    this.missionModel.supplyMission(new ArrayList<>(Arrays.asList(mission)));
    this.notifyObservers();

  }

  @Override
  public Pair<Vector, Integer> nextObjective(int roverId) {
    // RoverID as parameter or a rover status ?
    Rover rover = this.roverModel.getRover(roverId).orElseThrow(() -> new IllegalArgumentException("No rover with id " + roverId + " found"));

    List<Pair<Vector, Queue<Vector>>> roverToLocation = getRoverLocationToObjectives();

    Vector nextObjective;
    // violet
    if ( rover.getCurrentPath().isEmpty() ) {

      // red
      if ( rover.getPaths().isEmpty() ) {
        return caseRoverPathsIsEmpty(roverId, rover, roverToLocation);
      } else {
        this.missionModel.updateMission(roverId);
        this.domainModel.updateMap(roverToLocation);

        Queue<Vector> firstPath = rover.getPaths().poll();
        nextObjective = firstPath.poll();
        this.roverModel.assignPathSchedule(roverId, firstPath, rover.getPaths());

        int delay = strategyChangeDelay(rover);

        return new Pair<>(nextObjective, delay);
      }
    } else {
      int delay = strategyChangeDelay(rover);

      nextObjective = rover.getCurrentPath().poll();
      return new Pair<>(nextObjective, delay);
    }
  }

  @Override
  public void update(int time) {
    if(time - this.timeSinceRewardCalculation > 20) {
      this.awardPoints();
      this.timeSinceRewardCalculation = time;
    }
  }

  private Pair<Vector, Integer> caseRoverPathsIsEmpty(int roverID,
                                                      Rover rover,
                                                      List<Pair<Vector, Queue<Vector>>> roverToLocation) {
    this.domainModel.updateMap(roverToLocation);

    Graph<Vector, DefaultEdge>              currentMap     = this.domainModel.getCurrentMap();
    Mission                                 mission        = null;
    Pair<List<Vector>, List<Queue<Vector>>> strategyResult = null;

    List<Vector>        newObjectives;
    List<Queue<Vector>> paths;
    // blue
    if (this.missionModel.hasMission(roverID) ) {
      this.missionModel.updateMission(roverID);

      if (!this.missionModel.hasMission(roverID))
      {
        return null;
      }
      mission = this.missionModel.getMissionByRoverId(roverID);
      Strategy strategy = mission.getStrategy();


      Vector position = rover.getPosition();
      double x = Math.floor(position.get(0)), y = Math.floor(position.get(1));
      Vector roundedPosition = Vector.fromArray(new double[]{x, y});

      strategyResult = strategy.apply(
              roundedPosition,
              mission.getObjectives(),
              currentMap
                                     );

      newObjectives = strategyResult.getFirst();
      paths = strategyResult.getSecond();
    } else {
      Iterator<Mission> missions = this.missionModel.getMissions();

      if ( !missions.hasNext() ) {
        // System.out.println("No mission in the queue");
        return null;
      }

      while ( missions.hasNext() ) {
        mission = missions.next();

        Strategy strategy = mission.getStrategy();

        Vector position = rover.getPosition();
        double x = Math.floor(position.get(0)), y = Math.floor(position.get(1));
        Vector roundedPosition = Vector.fromArray(new double[]{x, y});

        strategyResult = strategy.apply(
                roundedPosition,
                mission.getObjectives(),
                currentMap
                                       );

        // Check if it was successful
        if ( !strategyResult.getSecond().isEmpty() ) {
          break;
        }
      }
      newObjectives = strategyResult.getFirst();
      paths = strategyResult.getSecond();
      if ( !newObjectives.isEmpty() && !paths.isEmpty()) {
        this.missionModel.assignMission(mission, roverID);
      }
    }

    if (paths.isEmpty()) {
      // System.out.println("No mission executable");
      return null;
    }

    if ( !newObjectives.isEmpty() ) {


      Queue<Vector> firstPath    = paths.remove(0);
      Vector        newObjective = firstPath.poll();
      this.roverModel.assignPathSchedule(
              roverID,
              firstPath,
              new LinkedList(paths)
                                        );

      mission.changeObjectivesOrder(newObjectives);
      int delay = strategyChangeDelay(rover);

      return new Pair<>(newObjective, delay);
    }

    return null;
  }

  private int strategyChangeDelay(Rover rover) {
    Area area = rover.getCurrentPhysicalArea();

    if (area==null || area.containsLocation(rover.getPosition()) ) {
      return 0;
    }

    Set<Area> areas = this.domainModel.getPhysicalAreas();

    for ( Area ar : areas ) {
      if ( ar.containsLocation(rover.getPosition()) ) {
        this.roverModel.setCurrentArea(rover.getId(), ar);
        return 2;
      }
    }

    throw new IllegalStateException("The rover is not in a known Physical area");
  }

  private List<Pair<Vector, Queue<Vector>>> getRoverLocationToObjectives() {
    List<Pair<Vector, Queue<Vector>>> result = new ArrayList<>();
    Map<Integer, Rover>               rovers = this.roverModel.getAllRovers();

    for ( Integer roverId : rovers.keySet() ) {
      Rover         rover       = rovers.get(roverId);
      Queue<Vector> currentPath = rover.getCurrentPath();

      if ( currentPath.isEmpty() ) {
        continue;
      }

      result.add(new Pair<>(rover.getPosition(), currentPath));
      Vector start = currentPath.peek();

      for ( Queue<Vector> path : rover.getPaths() ) {
        result.add(new Pair<>(start, path));
        start = getLastElement(path);
      }

    }
    return result;
  }

  private Vector getLastElement(Queue<Vector> queue) {
    final Iterator<Vector> itr         = queue.iterator();
    Vector                 lastElement = itr.next();

    while ( itr.hasNext() ) {
      lastElement = itr.next();
    }

    return lastElement;
  }

  public void awardPoints() {
    Map<Integer, Vector> mappingRoverLocation = new HashMap<>();
    List<Vector>         vectorLocations      = this.roverModel.getLocations();
    Set<Area>            physicalAreas        = this.domainModel.getPhysicalAreas();
    Set<Area>            logicalAreas         = this.domainModel.getLogicalAreas();

    int i = 0;
    for ( Vector location : vectorLocations ) {
      mappingRoverLocation.put(i, location);
      i++;
    }

    if ( this.rewardModel.shouldSwitch(vectorLocations, physicalAreas, logicalAreas) ) {
      this.rewardModel.switchProcedure();
    }


    this.rewardModel.calculatePoints(
            mappingRoverLocation,
            physicalAreas,
            logicalAreas,
            this.domainModel.getCategories()
                                    );
  }

  @Override
  public Rover.RoverStatus getRoverStatus(int roverId) {
    return this.roverModel.getRover(roverId).orElseThrow(() -> new IllegalArgumentException("No rover with id " + roverId + " found" )).getRoverStatus();
  }

  @Override
  public void setRoverStatus(int roverId, Vector position, Vector objective, SeverityLevel fault) {
    Rover actualStatus = this.roverModel.getRover(roverId).get();
    if (actualStatus.getCurrentPhysicalArea() == null) {
      Set<Area> areas = this.domainModel.getPhysicalAreas();
      for ( Area ar : areas ) {
        if ( ar.containsLocation(position) ) {
          actualStatus.setCurrentPhysicalArea(ar);
        }
      }
    }
    this.roverModel.setRoverStatus(roverId, position, objective, fault);
    this.notifyObservers();
  }

  @Override
  public int addRover() {
    int tmp = this.roverModel.createRover();
    this.notifyObservers();
    return tmp;
  }

  @Override
  public Set<Area> getPhysicalAreas() {
    return this.domainModel.getPhysicalAreas();
  }

  @Override
  public Set<Area> getLogicalAreas() {
    return this.domainModel.getLogicalAreas();
  }

  @Override
  public Map<String, Set<Area>> getCategories() {
    return this.domainModel.getCategories();
  }

  @Override
  public Graph<Vector, DefaultEdge> getCurrentMap() {
    return this.domainModel.getCurrentMap();
  }

  @Override
  public void assignStrategy(int missionId, StrategyType strategy) {
    this.notifyObservers();

  }



  @Override
  public void subscribe(SystemStatusObserver observer) {
    this.observers.add(observer);
  }

  @Override
  public void unsubscribe(SystemStatusObserver observer) {
    this.observers.remove(observer);
  }

  @Override
  public void notifyObservers() {
    Map data = new HashMap<>();

    data.put("ROVERS", roverModel.getListOfRovers());
    data.put("MISSIONS", missionModel.getActiveMissions());
    data.put("MISSIONS_NOT_ASSIGNED", missionModel.getMissionsNotAssigned());
    data.put("PHYSICAL_AREAS", domainModel.getPhysicalAreas());
    data.put("LOGICAL_AREAS", domainModel.getLogicalAreas());
    data.put("OBSTRUCTED_AREAS", domainModel.getObstructedAreas());

    for(SystemStatusObserver observer : observers){
      observer.onSystemChange(data);
    }
  }

}
