package rovu.centralstation.business;

import rovu.centralstation.business.mission.strategy.StrategyType;

public class StrategyAdministratorImpl implements StrategyAdministrator {

	private final StrategyProvider strategyProvider;

	public StrategyAdministratorImpl(StrategyProvider strategyProvider) {
		this.strategyProvider = strategyProvider;
	}


    @Override
    public void assignStrategy(int missionId, StrategyType strategy) {
			this.strategyProvider.assignStrategy(missionId, strategy);
    }
}
