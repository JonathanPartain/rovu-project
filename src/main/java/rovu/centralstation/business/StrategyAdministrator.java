package rovu.centralstation.business;

import rovu.centralstation.business.mission.strategy.StrategyType;



public interface StrategyAdministrator {
    void assignStrategy(int missionId, StrategyType strategy );
}

