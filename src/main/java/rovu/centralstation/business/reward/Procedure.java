package rovu.centralstation.business.reward;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.la4j.Vector;
import rovu.centralstation.business.domain.Area;

public class Procedure {

     private Map<String, Integer> pointsPerCategory;

     public Procedure(Map<String, Integer> pointsPerCategory) {
          this.pointsPerCategory = pointsPerCategory;
     }

     int calculatePoints(Vector roverLocation, Set<Area> areas, Map<String, Set<Area>> categories) {
          int points = 0;
          for (Map.Entry<String, Integer> areaCategory: pointsPerCategory.entrySet()) {
               // 1 - Conjunction between Areas and categories for each sort of category that is defined for this sort of procedure
               Set<Area> conjunction = new HashSet<>(categories.get(areaCategory.getKey()));
               conjunction.retainAll(areas);

               //Go through the conjunction and add points for each area that contains the roverLocation
               for (Area area: conjunction) {
                    if(area.containsLocation(roverLocation)) {
                         points += areaCategory.getValue();
                    }
               }
          }
          return points;
     }
}
