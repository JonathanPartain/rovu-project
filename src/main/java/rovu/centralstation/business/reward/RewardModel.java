package rovu.centralstation.business.reward;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.la4j.Vector;
import rovu.centralstation.TimerObservable;
import rovu.centralstation.TimerObserver;
import rovu.centralstation.business.domain.Area;


public class RewardModel{

    private final Procedure physicalProcedure;
    private final Procedure logicalProcedure;
    private Procedure currentProcedure;
    private int[] points;
    private int timeSinceRewardCalculation;

    public RewardModel(int nbrRover, Map<String, Integer> pointsPerCategory) {
        physicalProcedure = new Procedure(pointsPerCategory);
        logicalProcedure = new Procedure(pointsPerCategory);

        currentProcedure = physicalProcedure;

        points = new int[nbrRover];
    }

    public void calculatePoints(Map<Integer, Vector> mappingRoverLocation, Set<Area> physicalAreas, Set<Area> logicalAreas,
                                Map<String,Set<Area>>categories){

        for(Map.Entry<Integer, Vector> rover:  mappingRoverLocation.entrySet()) {
            Set<Area> testedAreas;
            if (currentProcedure == physicalProcedure)
                testedAreas = physicalAreas;
            else
                testedAreas = logicalAreas;
            points[rover.getKey()] += currentProcedure.calculatePoints(rover.getValue(), testedAreas, categories);
        }
    }


    public void switchProcedure(){
        if(currentProcedure == physicalProcedure){
            currentProcedure = logicalProcedure;
        }else if(currentProcedure == logicalProcedure){
            currentProcedure = physicalProcedure;
        }
    }


    public boolean shouldSwitch(List<Vector> roverLocations, Set<Area> physicalAreas, Set<Area> logicalAreas){
        if(currentProcedure == physicalProcedure){
            return (areaSetContainsRover(roverLocations, logicalAreas));
        }else if(currentProcedure == logicalProcedure){
            return (areaSetContainsRover(roverLocations, physicalAreas));
        }
        return false;
    }

    private boolean areaSetContainsRover(List<Vector> roverLocations, Set<Area> setOfAreas){
        for(Vector location: roverLocations){
            for(Area area: setOfAreas){
                if(area.containsLocation(location)){
                    return true;
                }
            }
        }
        return false;
    }


    public int[] getAllPointsPerRoverId(){
        return points;
    }

    public int getPointsSpecificRover(int roverId){
        return points[roverId];
    }

}
