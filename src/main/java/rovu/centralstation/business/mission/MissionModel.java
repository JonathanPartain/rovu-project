package rovu.centralstation.business.mission;

import java.util.*;

import org.la4j.Vector;
import rovu.centralstation.business.mission.strategy.Strategy;
import rovu.centralstation.business.mission.strategy.StrategyProvider;
import rovu.centralstation.business.mission.strategy.StrategyProviderImpl;
import rovu.centralstation.business.mission.strategy.StrategyType;

public class MissionModel {

	private final Queue<Mission> missionQueue;
	private final MissionLog[] assignedMissions;
	private final List<MissionLog> completedMissions;
	private final StrategyProvider strategyProvider;

	public MissionModel(Queue<Mission> queue, int nbrRover) {
		this.missionQueue = queue;
		this.assignedMissions = new MissionLog[nbrRover];
		this.completedMissions = new ArrayList<>();
		this.strategyProvider = new StrategyProviderImpl();
	}

	public void updateMission(int roverId) {
		MissionLog mLog = this.assignedMissions[roverId];
		Vector cleared = mLog.pollMissionObjective();

		mLog.addClearedObjective(cleared);

		List<Vector> missionObjectives = mLog.getMission().getObjectives();
		if (missionObjectives.isEmpty()) {
			mLog.setCleared();
			this.completedMissions.add(mLog);
			this.assignedMissions[roverId] = null;
		}
	}

	public Iterator<Mission> getMissions() {
		return this.missionQueue.iterator();
	}

	public List<Mission> getMissionsNotAssigned(){
		List missions = new ArrayList();
		for(Mission m : this.missionQueue){
			missions.add(m);
		}
		return missions;
	}

	public List<Mission> getActiveMissions(){
		List missions = new ArrayList();
		for(MissionLog ml: this.assignedMissions){
			if(ml != null) {
				missions.add(ml.getMission());
			}
		}
		return missions;
	}



	public void supplyMission(List<Vector> objectives) {
		if (objectives.isEmpty()) {
			throw new IllegalArgumentException("List of objectives cannot be empty");
		}

		Strategy defaultStrat = strategyProvider.getStrategy(StrategyType.REWARDING_PATH);
		Mission tmp = MissionFactory.createMission(objectives, defaultStrat);
		this.missionQueue.add(tmp);
	}

	public void supplyMission(List<Vector> objectives, StrategyType type) {
		if (objectives.isEmpty()) {
			throw new IllegalArgumentException("List of objectives cannot be empty");
		}

		Strategy strat = strategyProvider.getStrategy(type);
		Mission tmp = MissionFactory.createMission(objectives, strat);
		this.missionQueue.add(tmp);
	}

	public boolean hasMission(int roverId) {
		if (this.assignedMissions[roverId] != null)
			return true;
		return false;
	}

	public Mission getMissionByRoverId(int roverId) {
		MissionLog tmp = this.assignedMissions[roverId];
		if (tmp == null)
			return null;
		return tmp.getMission();
	}

	public Mission getMissionByMissionId(int missionId) {
		for (Mission m : missionQueue) {
			if (m.getId() == missionId) {
				return m;
			}
		}
		Mission mission;
		for (int i = 0; i < assignedMissions.length; i++) {
			mission = assignedMissions[i].getMission();
			if (mission.getId() == missionId) {
				return mission;
			}
		}
		for (MissionLog log : completedMissions) {
			mission = log.getMission();
			if (mission.getId() == missionId) {
				return mission;
			}
		}
		return null;
	}

	public void assignMission(Mission mission, int roverId) {
		if (this.assignedMissions[roverId] != null) {
			throw new UnsupportedOperationException("Cannot override current mission of a Rover");
		}
		this.assignedMissions[roverId] = new MissionLog(mission);
	}

}
