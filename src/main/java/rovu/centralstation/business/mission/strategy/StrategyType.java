package rovu.centralstation.business.mission.strategy;

public enum StrategyType {
    SHORTEST_PATH, FASTEST_PATH, REWARDING_PATH
}
