package rovu.centralstation.business.mission.strategy;

public interface StrategyProvider {

    Strategy getStrategy(StrategyType type);
}
