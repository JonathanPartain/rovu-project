package rovu.centralstation.business.mission.strategy;

class StrategyFactory {
    static Strategy createStrategy(StrategyType type) {
        switch (type) {
            case SHORTEST_PATH:
                return new ShortestPath();
            case FASTEST_PATH:
                return new FastestPath();
            case REWARDING_PATH:
                return new RewardingPath();
            default:
                // Throw exception ?
                return null;
        }
    }
}
