package rovu.centralstation.business.mission.strategy;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.la4j.Vector;
import rovu.util.Pair;

import java.util.List;
import java.util.Queue;

public interface Strategy {

    Pair<List<Vector>, List<Queue<Vector>>> apply(Vector roverLocation, List<Vector> MissionObjectives, Graph<Vector, DefaultEdge> currentEnvironment);
}
