package rovu.centralstation.business.mission.strategy;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.la4j.Vector;
import rovu.util.Pair;

import java.util.List;
import java.util.Queue;

class ShortestPath implements Strategy {

	private Graph<Vector, DefaultEdge> originalEnvironment;
	private final Strategy fastestPath = StrategyFactory.createStrategy(StrategyType.FASTEST_PATH);


	/**
	 * Tries to apply the shortest path in the original environment,
	 * if not possible then it returns missionObjectives and empty paths.
	 *
	 * @param roverLocation      the current roverLocation
	 * @param missionObjectives  the objectives for the mission
	 * @param currentEnvironment the current graph the strategy is applied on
	 * @return A Pair of a List of objectives and a List of paths (empty if not actual shortest path).
	 */
	@Override
	public Pair<List<Vector>, List<Queue<Vector>>> apply(Vector roverLocation, List<Vector> missionObjectives, Graph<Vector, DefaultEdge> currentEnvironment) {
		Pair<List<Vector>, List<Queue<Vector>>> shortestPathResult = fastestPath.apply(roverLocation, missionObjectives, originalEnvironment);
		Pair<List<Vector>, List<Queue<Vector>>> fastestPathResult = fastestPath.apply(roverLocation, missionObjectives, currentEnvironment);

		if (!shortestPathResult.equals(fastestPathResult)) {
			for (Queue<Vector> path : shortestPathResult.getSecond()) {
				path.clear();
			}
			return new Pair<>(missionObjectives, shortestPathResult.getSecond());
		}
		return shortestPathResult;
	}

	public void setOriginalEnvironment(Graph<Vector, DefaultEdge> originalEnvironment) {
		this.originalEnvironment = originalEnvironment;
	}
}
