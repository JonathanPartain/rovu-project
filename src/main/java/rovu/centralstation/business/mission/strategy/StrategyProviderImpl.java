package rovu.centralstation.business.mission.strategy;

import java.util.HashMap;
import java.util.Map;

public class StrategyProviderImpl implements StrategyProvider {

    private Map<StrategyType, Strategy> strategies;

    public StrategyProviderImpl() {
        this.strategies = new HashMap<>();
    }

    @Override
    public Strategy getStrategy(StrategyType type) {
        if (strategies.containsKey(type))
            return strategies.get(type);

        Strategy newStrategy = StrategyFactory.createStrategy(type);
        strategies.put(type, newStrategy);
        return newStrategy;
    }
}
