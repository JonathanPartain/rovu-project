package rovu.centralstation.business.mission.strategy;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.la4j.Vector;
import rovu.util.Pair;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class FastestPath implements Strategy {

	@Override
	public Pair<List<Vector>, List<Queue<Vector>>> apply(Vector roverLocation, List<Vector> missionObjectives, Graph<Vector, DefaultEdge> currentEnvironment) {
		List<Vector> objectives = new ArrayList<>(missionObjectives);
		List<Vector> orderedObjectives = new ArrayList<>();
		List<Queue<Vector>> paths = new ArrayList<>();

		Vector start = roverLocation;
		while (!objectives.isEmpty()) {
			List<GraphPath<Vector, DefaultEdge>> pathToObjectives = new ArrayList<>();
			for (Vector o : objectives) {
				if (!currentEnvironment.containsVertex(o)) {
					for (Queue<Vector> path : paths) {
						path.clear();
					}
					return new Pair<>(missionObjectives, paths);
				}
				pathToObjectives.add(DijkstraShortestPath.findPathBetween(currentEnvironment, start, o));
			}
			Queue<Vector> shortestPath = new LinkedList<>();
			GraphPath<Vector, DefaultEdge> shortestSoFar = pathToObjectives.get(0);
			for (GraphPath<Vector, DefaultEdge> path : pathToObjectives) {
				if (path.getWeight() < shortestSoFar.getWeight()) {
					shortestSoFar = path;
				}

			}
			List<Vector> shortest = shortestSoFar.getVertexList();
			shortest.remove(0);
			shortestPath.addAll(shortest);
			paths.add(shortestPath);
			int objectiveIndex = pathToObjectives.indexOf(shortestSoFar);
			Vector obj = objectives.get(objectiveIndex);
			orderedObjectives.add(obj);
			start = obj;
			objectives.remove(obj);
		}
		return new Pair<>(orderedObjectives, paths);
	}

}
