package rovu.centralstation.business.mission;

import java.util.List;
import org.la4j.Vector;
import rovu.centralstation.business.mission.strategy.Strategy;

public interface Mission {

    int getId();
    List<Vector> getObjectives();
    void  setStrategy(Strategy strategy);
    Strategy  getStrategy();
    Vector pollObjective();
    void changeObjectivesOrder(List<Vector> newOrder);
}
