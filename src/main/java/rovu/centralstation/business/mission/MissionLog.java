package rovu.centralstation.business.mission;

import java.util.ArrayList;
import java.util.List;
import org.la4j.Vector;

public class MissionLog {

    private final Mission mission;
    private final List<Vector> clearedObjectives;
    private boolean cleared;

    public MissionLog(Mission mission) {
        this.mission = mission;
        this.clearedObjectives = new ArrayList<>();
        this.cleared = false;
    }

    public void addClearedObjective(Vector objective) {
        this.clearedObjectives.add(objective);
    }

    public Vector pollMissionObjective() {
        return this.mission.pollObjective();
    }

    public boolean isCleared() {
        return this.cleared;
    }

    public void setCleared(){
        this.cleared = true;
    }

    public Mission getMission() {
        return this.mission;
    }

}
