package rovu.centralstation.business.mission;

import org.la4j.Vector;
import rovu.centralstation.business.mission.strategy.Strategy;

import java.util.List;

public class MissionFactory {

    private static int missionCounter = 0;

    static Mission createMission(List<Vector> objectives){
        missionCounter++;
        return new MissionImpl(missionCounter, objectives);
    }

    static Mission createMission(List<Vector> objectives, Strategy strategy){
        missionCounter++;
        return new MissionImpl(missionCounter, objectives, strategy);
    }
}
