package rovu.centralstation.business.mission;

import java.util.List;
import org.la4j.Vector;
import rovu.centralstation.business.mission.strategy.Strategy;

public class MissionImpl implements Mission {

    private final int missionId;
    private final List<Vector> objectives;
    private Strategy strategy;

    public MissionImpl(int id, List<Vector> objectives) {
        this.missionId = id;
        this.objectives = objectives;
    }

    public MissionImpl(int id, List<Vector> objectives, Strategy strategy) {
        this.missionId = id;
        this.objectives = objectives;
        this.strategy = strategy;
    }
    @Override
    public int getId() {
        return this.missionId;
    }

    @Override
    public List<Vector> getObjectives() {
        // Should we make a copy to avoid unwanted modification outside the class ?
        return this.objectives;
    }

    @Override
    public Strategy getStrategy() {
        return this.strategy;
    }

    @Override
    public Vector pollObjective() {
        return this.objectives.remove(0);
    }

    @Override
    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public void changeObjectivesOrder(List<Vector> newOrder) {
        if (!this.objectives.containsAll(newOrder) && !newOrder.containsAll(this.objectives)) {
            throw new IllegalArgumentException("The list does not contains every original vector");
        }

        this.objectives.clear();
        this.objectives.addAll(newOrder);
    }
}
