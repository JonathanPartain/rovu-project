package rovu.centralstation.business;

import org.la4j.Vector;
import rovu.centralstation.Layer;
import rovu.centralstation.business.mission.strategy.StrategyType;
import rovu.centralstation.business.rover.Rover;
import rovu.centralstation.business.rover.RoverObserver;
import rovu.communication.Protocol;
import rovu.util.Pair;

import java.util.HashMap;
import java.util.Map;

public class MissionAdministratorImpl implements MissionAdministrator, RoverObserver {

    private final MissionProvider MISSION_PROVIDER;
    private Layer layerToNotify;

    public MissionAdministratorImpl(MissionProvider missionProvider){
        this.MISSION_PROVIDER = missionProvider;
    }

    public void setLayerToNotify(Layer layer) {
        this.layerToNotify = layer;
    }

    @Override
    public Pair<Vector, Integer> nextObjective(int roverId) {
        return MISSION_PROVIDER.nextObjective(roverId);
    }
    

    @Override
    public void supplyMission(Vector[] mission) {
        this.MISSION_PROVIDER.supplyMission(mission);
    }

    @Override
    public void supplyMission(Vector[] mission, StrategyType strategy) {

    }

    @Override
    public void onRoverChange(Rover newState) {
        if(newState.getObjective()==null){
            Pair<Vector, Integer> result = nextObjective(newState.getId());
            if (result != null) {
                Map<String, Object> data = new HashMap<>();

                data.put(Protocol.COMMAND, Protocol.Commands.NEXT_OBJECTIVE);
                data.put(Protocol.OBJECTIVE, result.getFirst());
                data.put(Protocol.DELAY, result.getSecond());
                data.put(Protocol.ROVER_ID, newState.getId());
                this.layerToNotify.processData(data);
            }
        }
    }
}
