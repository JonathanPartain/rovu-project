package rovu.centralstation.business;

import java.util.Map;

public interface SystemStatusObserver {

    void onSystemChange(Map<String,Object> data); //SystemStatus newState
}
