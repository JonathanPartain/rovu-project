package rovu.centralstation.business;

import org.la4j.Vector;
import rovu.centralstation.business.rover.Rover;
import rovu.communication.SeverityLevel;
import rovu.rover.Fault;

public interface RoverProvider {

     Rover.RoverStatus getRoverStatus(int roverID);
     void setRoverStatus(int roverId, Vector position, Vector objective, SeverityLevel faultSeverity);

     /**
      * Adds the rover to the {@link rovu.centralstation.business.rover.RoverModel}
      * @return ID of the added rover
      */
     int addRover();

}
