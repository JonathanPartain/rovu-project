package rovu.centralstation.business;

import java.util.Map;

public interface SystemStatusObservable {

    void notifyObservers(); //SystemStatus newState
    void subscribe(SystemStatusObserver observer);
    void unsubscribe(SystemStatusObserver observer);
}
