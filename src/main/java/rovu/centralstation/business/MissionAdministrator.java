package rovu.centralstation.business;

import org.la4j.Vector;
import rovu.centralstation.Layer;
import rovu.centralstation.business.mission.strategy.StrategyType;
import rovu.util.Pair;

public interface MissionAdministrator {
	Pair<Vector, Integer> nextObjective(int roverID);
	void supplyMission(Vector[] mission);
	void supplyMission(Vector[] mission, StrategyType strategy);
	void setLayerToNotify(Layer layer);
}
