package rovu.centralstation;

public interface TimerObservable extends Timer {
    void notifyObservers();
    void subscribe(TimerObserver timerObserver);
    void unsubscribe(TimerObserver timerObserver);
}
