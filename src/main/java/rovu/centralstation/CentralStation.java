package rovu.centralstation;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;
import org.xml.sax.SAXException;
import rovu.centralstation.business.*;
import rovu.centralstation.business.domain.DomainModel;
import rovu.centralstation.business.domain.MapFactory;
import rovu.centralstation.business.mission.MissionModel;
import rovu.centralstation.business.reward.RewardModel;
import rovu.centralstation.business.RoverController;
import rovu.centralstation.business.rover.RoverModel;
import rovu.centralstation.information.*;
import rovu.communication.Connection;
import rovu.communication.Master;
import rovu.communication.Protocol;
import rovu.communication.Slave;
import rovu.userinterface.Observer;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class CentralStation {
    private final InformationLayer<Map<String, Object>> informationLayer;


    public CentralStation(InformationLayer<Map<String, Object>> informationLayer,
                          BusinessLayerImpl businessLayer) {
        this.informationLayer = informationLayer;
        businessLayer.setPreviousLayer(informationLayer);
    }

    public Master<Map<String,Object>> getMasterConnection() {
        return this.informationLayer;
    }

  public static BusinessLayerImpl createBusinessLayer(int nbrRover,
                                                      File domainModelXML,
                                                      Map<String, Integer> pointsByCategory,
                                                      TimerObservable timer, SystemStatusObserver observerUI) throws IOException, SAXException, ParserConfigurationException {


        org.la4j.Vector start = new BasicVector(new double[]{0, 0});
        org.la4j.Vector end = new BasicVector(new double[]{10, 10});
        double density = 2;

        Graph<Vector, DefaultEdge> g = MapFactory.createMap(start, end, density);

      DomainModel domainModel = new DomainModel(domainModelXML);;
      RoverModel roverModel = new RoverModel(new ArrayList<>());
      MissionModel missionModel = new MissionModel(new LinkedList<>(), nbrRover);
      RewardModel rewardModel = new RewardModel(nbrRover, pointsByCategory);

        SystemStatus systemStatus = new SystemStatus(
                roverModel,
                domainModel,
                rewardModel,
                missionModel,
                timer
        );

        MissionAdministratorImpl missionAdministrator = new MissionAdministratorImpl(systemStatus);
        roverModel.subscribe(missionAdministrator);

        StrategyAdministrator strategyAdministrator = new StrategyAdministratorImpl(systemStatus);

        //TODO - SKA DETTA VERKLIGEN VARA KVAR??
        systemStatus.subscribe(observerUI);


    BusinessLayerImpl result = new BusinessLayerImpl(
            new RoverController(systemStatus),
            new AssignmentController(strategyAdministrator, missionAdministrator)
    );

        return result;
    }

    public static InformationLayer<Map<String, Object>> createInformationLayer(Layer businessLayer,
                                                                               TimerObservable timer) {
        Proxy<Map<String, Object>, Map<String, Object>> userProxy  = new ProxyImpl<>(msg -> msg, msg -> msg);
        Proxy<Map<String, Object>, Map<String, Object>> roverProxy = new ProxyImpl<>(msg -> msg, msg -> msg);

        return new InformationLayerImpl<>(userProxy,
                roverProxy,
                businessLayer,
                timer);
    }

}