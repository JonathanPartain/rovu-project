package rovu.rover;

import rovu.communication.SeverityLevel;

public enum Fault {
    NONE(SeverityLevel.NONE),
    CAMERA(SeverityLevel.NONE),
    ENGINE(SeverityLevel.NONE),
    NETWORK_DEVICE(SeverityLevel.NONE),
    PROXIMITY(SeverityLevel.NONE),
    SATELLITE(SeverityLevel.NONE),
    WHEEL(SeverityLevel.NONE);

    private SeverityLevel level;
    Fault(SeverityLevel level){this.level = level; }
    public void setLevel(SeverityLevel l){this.level = l; }
    public SeverityLevel getLevel() {return this.level; }
}
