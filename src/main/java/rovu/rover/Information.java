package rovu.rover;


import java.util.function.Function;

public interface Information<T extends Function> {

    public void extract(T informationConsumer);
}
