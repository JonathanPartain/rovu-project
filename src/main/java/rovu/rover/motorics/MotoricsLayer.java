package rovu.rover.motorics;

import org.la4j.Vector;
import rovu.communication.Protocol;
import rovu.rover.*;
import rovu.communication.SeverityLevel;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

public class MotoricsLayer extends AbstractLayer<Function<Integer, Function<Vector, Function<Vector, Function<SeverityLevel, Function<Double, Consumer<Double>>>>>>, Function > {

    private MotoricsManager motoricsManager;
    private CommunicationManager<Map<String, Object>> communicationManager;
    public MotoricsLayer(Layer<Function> layer, MotoricsManager motoricsManager, CommunicationManager<Map<String,Object>> communicationManager) {
        super(layer);
        this.motoricsManager = motoricsManager;
        this.communicationManager = communicationManager;

    }


    @Override
    public void handleInformation(Information<Function<Integer, Function<Vector, Function<Vector, Function<SeverityLevel, Function<Double, Consumer<Double>>>>>>> information) {
        information.extract(id -> pos -> objective -> severityLevel -> angle -> speed -> {

            this.motoricsManager.setRotationVelocity(angle);
            this.motoricsManager.setVelocity(speed);

            Map<String, Object> dataUpdate = new HashMap<>();
            dataUpdate.put(Protocol.COMMAND, Protocol.Commands.ROVER_UPDATE);
            dataUpdate.put(Protocol.ROVER_ID, id);
            dataUpdate.put(Protocol.FAULT, severityLevel);
            dataUpdate.put(Protocol.POSITION, pos);
            dataUpdate.put(Protocol.OBJECTIVE, objective);
            this.communicationManager.transmitData(dataUpdate);
        });
    }
}
