package rovu.rover.motorics;


public class MotoricsManagerImpl implements MotoricsManager {
    private HardwareConnection hardwareConnection;

    public MotoricsManagerImpl(HardwareConnection hardwareConnection) {
        this.hardwareConnection = hardwareConnection;
    }

    @Override
    public void setVelocity(double velocity) {
        this.hardwareConnection.setVelocity(velocity);
    }

    @Override
    public void setRotationVelocity(double velocity) {
        this.hardwareConnection.setRotationVelocity(velocity);
    }
}
