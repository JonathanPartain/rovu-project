package rovu.rover.motorics;

public interface MotoricsManager {
    void setVelocity(double velocity);
    void setRotationVelocity(double velocity);
}
