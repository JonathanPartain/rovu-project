package rovu.rover.motorics;

public interface CommunicationManager<T> {
    void transmitData(T data);
}
