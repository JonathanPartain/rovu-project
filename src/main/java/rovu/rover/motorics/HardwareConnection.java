package rovu.rover.motorics;

import rovu.rover.Rover;

public interface HardwareConnection {
	void setVelocity(double velocity);
	void setRotationVelocity(double velocity);
	void connect(Rover software);
}
