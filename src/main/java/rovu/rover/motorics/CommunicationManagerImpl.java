package rovu.rover.motorics;

import rovu.rover.NetworkTransmitter;

public class CommunicationManagerImpl<T> implements CommunicationManager<T> {

    private final NetworkTransmitter<T> networkTransmitter;

    public CommunicationManagerImpl(NetworkTransmitter<T> networkTransmitter) {
        this.networkTransmitter = networkTransmitter;
    }

    @Override
    public void transmitData(T data) {
       networkTransmitter.transmit(data);
    }
}
