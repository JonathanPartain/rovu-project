package rovu.rover.model;

import org.la4j.Vector;
import rovu.rover.Fault;

import java.util.Optional;

public class RoverModelImpl implements RoverModel {
  private Integer id;
  private Boolean[] environmentStatus;
  private Fault faultStatus;
  private Vector objective;
  private Vector position;
  private Vector direction;

  public RoverModelImpl(Boolean[] environmentStatus, Vector position, Vector direction) {
    this.environmentStatus = environmentStatus;
    this.faultStatus = Fault.NONE;
    this.objective = null;
    this.position = position;
    this.direction = direction;
  }


  @Override
  public Boolean[] getEnvironmentStatus() {
    return this.environmentStatus;
  }

  @Override
  public Optional<Fault> getFaultStatus() {
    return Optional.of(this.faultStatus);
  }

  @Override
  public Optional<Vector> getObjective() {
    return Optional.ofNullable(this.objective);
  }

  @Override
  public Vector getPosition() {
    return this.position;
  }

  @Override
  public Vector getDirection() {
    return this.direction;
  }

  @Override
  public Optional<Integer> getId() {
    return Optional.ofNullable(this.id);
  }

  @Override

  public void updateModel(Integer id,
                          Boolean[] environmentStatus,
                          Fault faultStatus,
                          Vector objective,
                          Vector position,
                          Vector direction) {
    // If message has been received
    if ( id != null ) {
      // Check if we have an assigned Id
      // if not, assign this handed Id
      // else update the objective given in the message
      if ( !this.getId().isPresent() ) {
        this.id = id;
      } else if ( this.getId().get().equals(id) ) {
        this.objective = objective;
        System.out.println("Objective received " + objective.toString());
        System.out.println("Position " + this.getPosition().toString());
      }
    }
    this.environmentStatus = environmentStatus;
    this.faultStatus = faultStatus;
    this.position = position;
    this.direction = direction;

    // We've reached our current objective with a small margin
    if ( this.getObjective().isPresent() && this.getObjective().get().equals(this.getPosition(), 0.01)) {
      this.objective = null;
      System.out.println("Objective reached at " + this.getPosition().toString());
    }

  }

}
