package rovu.rover.model;

import org.la4j.Vector;
import rovu.rover.AbstractLayer;
import rovu.rover.Fault;
import rovu.rover.Information;
import rovu.rover.Layer;

import java.util.function.Consumer;
import java.util.function.Function;

public class ModelLayer
        extends AbstractLayer<Function<Integer, Function<Boolean[], Function<Fault, Function<Vector, Function<Vector, Consumer<Vector>>>>>>, Function<Integer, Function<Boolean[], Function<Fault, Function<Vector, Function<Vector, Consumer<Vector>>>>>>> {
  private final RoverModel roverModel;

  public ModelLayer(RoverModel roverModel,
                    Layer<Function<Integer, Function<Boolean[], Function<Fault, Function<Vector, Function<Vector, Consumer<Vector>>>>>>> nextLayer) {

    super(nextLayer);
    this.roverModel = roverModel;
  }


  @Override
  public void handleInformation(Information<Function<Integer, Function<Boolean[], Function<Fault, Function<Vector, Function<Vector, Consumer<Vector>>>>>>> information) {
    information.extract(id -> env -> fault -> objective -> pos -> dir -> this.roverModel
            .updateModel(id, env, fault, objective, pos, dir));
    final Integer   id          = this.roverModel.getId().orElse(0);
    final Boolean[] environment = this.roverModel.getEnvironmentStatus();
    final Vector    position    = this.roverModel.getPosition();
    final Vector    direction   = this.roverModel.getDirection();
    final Vector    objective   = this.roverModel.getObjective().orElse(null);
    final Fault     fault       = this.roverModel.getFaultStatus().orElse(Fault.NONE);

    this.feedNextLayer(informationConsumer ->
                               informationConsumer.apply(id)
                                                  .apply(environment)
                                                  .apply(fault)
                                                  .apply(objective)
                                                  .apply(position)
                                                  .accept(direction));

  }
}
