package rovu.rover.model;

import org.la4j.Vector;
import rovu.rover.Fault;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface RoverModel {
  Boolean[] getEnvironmentStatus();

  Optional<Fault> getFaultStatus();

  Optional<Vector> getObjective();

  Vector getPosition();

  Vector getDirection();

  Optional<Integer> getId();

  void updateModel(Integer roverId,
                   Boolean[] environmentStatus,
                   Fault faultStatus,
                   Vector objective,
                   Vector position,
                   Vector direction);
}
