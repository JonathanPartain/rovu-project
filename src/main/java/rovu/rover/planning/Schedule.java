package rovu.rover.planning;

import rovu.communication.SeverityLevel;

public class Schedule {

    public final double angle;
    public final double speed;
    public final SeverityLevel severityLevel;


    public Schedule(double angle, double speed, SeverityLevel severityLevel) {
        this.angle = angle;
        this.speed = speed;
        this.severityLevel = severityLevel;
    }
}
