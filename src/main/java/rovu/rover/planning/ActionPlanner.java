package rovu.rover.planning;

import org.la4j.Vector;
import rovu.communication.SeverityLevel;
import rovu.rover.Fault;


import java.lang.Math;

import java.util.Optional;


public class ActionPlanner implements Planner {
    public static final double SPEED_ON = 1.5;
    public static final double SPEED_OFF = 0;
    public static final double ANGLE_PROPORTIONAL = 12;

    private final SeverityDeterminator<Fault, SeverityLevel> faultSeverityDeterminator;

    public ActionPlanner(SeverityDeterminator<Fault, SeverityLevel> faultSeverityDeterminator) {
        this.faultSeverityDeterminator = faultSeverityDeterminator;
    }


    public Schedule planSchedule(Boolean[] environmentStatus,
                                 Fault faultStatus,
                                 Vector objective,
                                 Vector position,
                                 Vector direction) {


        double angle = objective != null ? calculateAngle(objective, position, direction) : 0;
        double speed = objective != null ? calculateSpeed(angle, objective, position, direction) : SPEED_OFF;

        return new Schedule(angle*ANGLE_PROPORTIONAL, speed, faultSeverityDeterminator.determineSeverityLevel(faultStatus));
    }

    private double calculateSpeed(double angle, Vector objective, Vector position, Vector direction) {
        Vector posError = position.subtract(objective);
        double distanceError = posError.norm();

        double speed = Math.abs(angle) > 0 ? 1.0/(Math.abs(angle)) * distanceError * SPEED_ON : SPEED_ON;

        return Math.min(SPEED_ON, speed);
    }

    public double calculateAngle(double angle) {
        return angle*ANGLE_PROPORTIONAL;
    }

    private double calculateAngle(Vector objective, Vector position, Vector direction) {
        double angleBetween;
        Vector posToObj = objective.subtract(position);

        double cosAngle = posToObj.innerProduct(direction) / (posToObj.norm() * direction.norm());
        angleBetween = cosAngle != 0 ? Math.acos(cosAngle) : Math.PI / 2;

        // Normal vector (a,b) to use for ax + by = c <=> ax + by - c = 0
        Vector normalVector = getNormalVector(posToObj);
        // c in formula above to subtract for correct offset
        double offset = normalVector.innerProduct(position);

        //Position + small movement in the current direction
        Vector posPlusDirection = position.add(direction);

        //Calculate if new position is on the right or left side of objective direction
        //Right   = +1
        //Left    = -1
        //Center  = 0
        double side = Math.signum(normalVector.innerProduct(posPlusDirection) - offset);

        //if (side == 1) System.out.println("Right");
        //else if( side == -1 ) System.out.println("Left");

        // Rotate in opposite direction
        angleBetween = angleBetween * side;

        // If we are moving 180 degrees in the wrong direction, add 180 degrees
        if (side == 0 && objective.subtract(posPlusDirection).norm() > posToObj.norm()) {
            angleBetween += Math.PI;
        }
        return angleBetween;
    }

    /**
     * @param posToObj vector to rotate
     * @return posToObj rotated 90 degrees clockwise
     */
    private Vector getNormalVector(Vector posToObj) {
        //Create perpendicular vector for posToObjective
        Vector normalVector = posToObj.divide(posToObj.norm());
        normalVector.swapElements(0, 1);
        normalVector.set(1, -normalVector.get(1));
        return normalVector;
    }

    private SeverityLevel determineFaultSeverity(Fault fault) {
        return null;
    }

    private Optional<Vector> determineMovementDirection(Boolean[] environmentStatus) {  //Should optional be Vector instead?

        return null;
    }


}


