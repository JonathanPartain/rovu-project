package rovu.rover.planning;

public interface SeverityDeterminator<T,R> {
	R determineSeverityLevel(T fact);
}
