package rovu.rover.planning;

import org.la4j.Vector;
import rovu.communication.SeverityLevel;
import rovu.rover.AbstractLayer;
import rovu.rover.Fault;
import rovu.rover.Information;
import rovu.rover.Layer;

import java.util.Queue;
import java.util.function.Consumer;
import java.util.function.Function;


public class PlanningLayer extends AbstractLayer<Function<Integer, Function<Boolean[], Function<Fault, Function<Vector, Function<Vector, Consumer<Vector>>>>>>, Function<Integer, Function<Vector, Function<Vector, Function<SeverityLevel, Function<Double, Consumer<Double>>>>>>> {


    private final ActionPlanner actionPlanner;

    public PlanningLayer(ActionPlanner actionPlanner, Layer<Function<Integer, Function<Vector, Function<Vector, Function<SeverityLevel, Function<Double, Consumer<Double>>>>>>> nextLayer){
        super(nextLayer);
        this.actionPlanner = actionPlanner;
    }

    @Override
    public void handleInformation(Information<Function<Integer, Function<Boolean[], Function<Fault, Function<Vector, Function<Vector, Consumer<Vector>>>>>>> information) {
        information.extract(id -> env -> fault -> objective -> pos -> dir -> { Schedule schedule = this.actionPlanner.planSchedule(env, fault, objective, pos, dir);
            this.feedNextLayer(informationConsumer -> informationConsumer
                    .apply(id)
                    .apply(pos)
                    .apply(objective)
                    .apply(schedule.severityLevel)
                    .apply(schedule.angle)
                    .accept(schedule.speed)
            );
        });


    }
}

