package rovu.rover.planning;

import rovu.communication.SeverityLevel;
import rovu.rover.Fault;

public class FaultSeverityDeterminatior implements SeverityDeterminator<Fault, SeverityLevel> {
	@Override
	public SeverityLevel determineSeverityLevel(Fault fact) {
		return fact.getLevel();
	}


}
