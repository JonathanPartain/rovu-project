package rovu.rover.planning;
import org.la4j.Vector;
import rovu.rover.Fault;


public interface Planner<T> {


    Schedule planSchedule(Boolean[] environmentStatus, Fault faultStatus, Vector objective, Vector position, Vector direction);
}
