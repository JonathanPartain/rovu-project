package rovu.rover;


import java.util.Optional;

public interface NetworkReceiver<T> {
    Optional<T> receive();
}
