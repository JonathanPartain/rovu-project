package rovu.rover.information;

import java.util.Optional;

public interface CommunicationInterpreter<T> {
	Optional<T> readNetworkBuffer();

}
