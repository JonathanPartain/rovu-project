package rovu.rover.information;

import org.la4j.Vector;

public interface SatelliteSensor {
	Vector readPosition();
	Vector readDirection();
}
