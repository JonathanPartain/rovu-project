package rovu.rover.information;

import rovu.rover.Fault;

public interface FaultInterpreter {
	Fault read();
}
