package rovu.rover.information;

import org.la4j.Vector;
import rovu.communication.Protocol;
import rovu.rover.*;
import rovu.rover.Information;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class InformationLayer
        extends AbstractLayer<Function, Function<Integer,Function<Boolean[], Function<Fault, Function<Vector, Function<Vector, Consumer<Vector>>>>>>> {

  private final SenseInterpreter senseInterpreter;
  private final FaultInterpreter faultInterpreter;
  private final CommunicationInterpreter<Map<String, Object>> communicationInterpreter;

  public InformationLayer(SenseInterpreter senseInterpreter,
                          FaultInterpreter faultInterpreter,
                          CommunicationInterpreter<Map<String, Object>> communicationInterpreter,
                          Layer<Function<Integer,Function<Boolean[], Function<Fault, Function<Vector, Function<Vector, Consumer<Vector>>>>>>> nextLayer) {
    super(nextLayer);
    this.senseInterpreter = senseInterpreter;
    this.faultInterpreter = faultInterpreter;
    this.communicationInterpreter = communicationInterpreter;
  }

  public void handleInformation(Information information) {
    final Map<String, Object> message = this.communicationInterpreter.readNetworkBuffer().orElse(new HashMap<>());

    final Integer   connectionId = (Integer) this.parseMessage(message, Protocol.ROVER_ID);
    final Boolean[] environment  = this.senseInterpreter.readEnvironment();
    final Vector    position     = this.senseInterpreter.getPosition();
    final Vector    direction    = this.senseInterpreter.getDirection();
    final Vector    objective    = (Vector) this.parseMessage(message, Protocol.OBJECTIVE);
    final Fault     fault        = this.faultInterpreter.read();

    this.feedNextLayer(informationConsumer ->
                               informationConsumer.apply(connectionId)
                                                  .apply(environment)
                                                  .apply(fault)
                                                  .apply(objective)
                                                  .apply(position)
                                                  .accept(direction));
  }

  private Object parseMessage(Map<String, Object> message, String field) {
    return message.getOrDefault(field, null);
  }
}
