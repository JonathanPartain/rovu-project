package rovu.rover.information;

import org.la4j.Vector;

public interface SensorConnection {
	double[] readProximity();
	Vector readPosition();
	Vector readDirection();

}
