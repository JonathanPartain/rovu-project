package rovu.rover.information;

import org.la4j.Vector;

public class SatelliteSensorImpl implements SatelliteSensor {
    private final SensorConnection hardware;

    public SatelliteSensorImpl(SensorConnection hardware) {
        this.hardware = hardware;
    }

    public Vector readPosition(){
        return this.hardware.readPosition();
    }

    public Vector readDirection(){
        return this.hardware.readDirection();
    }
}
