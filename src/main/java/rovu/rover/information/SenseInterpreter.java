package rovu.rover.information;

import org.la4j.Vector;

public interface SenseInterpreter {
    Boolean[] readEnvironment(); //TODO: add environment status
    Vector getPosition();
    Vector getDirection();
}
