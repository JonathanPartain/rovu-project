package rovu.rover.information;

public class ProximitySensorImpl implements ProximitySensor {
    private final SensorConnection hardware;

    public ProximitySensorImpl(SensorConnection hardware) {
        this.hardware = hardware;
    }

    public double[] read(){
        return this.hardware.readProximity();
    }
}
