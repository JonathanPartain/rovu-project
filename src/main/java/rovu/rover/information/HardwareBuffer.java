package rovu.rover.information;


import rovu.rover.Fault;

public interface HardwareBuffer {
	Fault readFault();
}
