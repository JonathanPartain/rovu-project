package rovu.rover.information;

import rovu.rover.Fault;

public class FaultInterpreterImpl implements FaultInterpreter {
    private final HardwareBuffer hardwareBuffer;

    public FaultInterpreterImpl(HardwareBuffer hardwareBuffer) {
        this.hardwareBuffer = hardwareBuffer;
    }

    @Override
    public Fault read() {
        return this.hardwareBuffer.readFault();
    }
}
