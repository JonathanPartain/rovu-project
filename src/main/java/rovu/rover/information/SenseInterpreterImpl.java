package rovu.rover.information;

import org.la4j.Vector;

import java.awt.image.BufferedImage;

public class SenseInterpreterImpl implements SenseInterpreter {
	private final ProximitySensor proximitySensor;
	private final SatelliteSensor satelliteSensor;
	private final Camera camera;

	private final double MIN_DISTANCE = 1.0D;

	public SenseInterpreterImpl(ProximitySensor proximitySensor,
	                            SatelliteSensor satelliteSensor,
	                            Camera camera) {
		this.proximitySensor = proximitySensor;
		this.satelliteSensor = satelliteSensor;
		this.camera = camera;
	}

	public Boolean[] readEnvironment() {
		double[] proximityData = getProximityData();

		return interpretProximityData(proximityData);
	}

	@Override
	public Vector getPosition() {
		return this.satelliteSensor.readPosition();
	}

	@Override
	public Vector getDirection() {
		return this.satelliteSensor.readDirection();
	}

	private double[] getProximityData() {
		return this.proximitySensor.read();
	}

	private BufferedImage getCamera() {
		return this.camera.read();
	}

	private Boolean[] interpretProximityData(double[] proximityData) {
		int       nSensors   = proximityData.length;
		Boolean[] collisions = new Boolean[nSensors];

		for ( int i = 0; i < nSensors; i++ ) {
			collisions[i] = proximityData[i] < this.MIN_DISTANCE;
		}
		return collisions;
	}

	private double[] interpretCameraData(double[] cameraData) {
		return new double[0]; //TODO: Temporary
	}

}

