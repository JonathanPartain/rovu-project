package rovu.rover.information;

import java.awt.image.BufferedImage;

public interface Camera {
	BufferedImage read();
}
