package rovu.rover.information;

import rovu.rover.NetworkReceiver;

import java.util.Optional;

public class CommunicationInterpreterImpl<T> implements CommunicationInterpreter<T> {
    private final NetworkReceiver<T> networkReceiver;

    public CommunicationInterpreterImpl(NetworkReceiver<T> networkReceiver) {
        this.networkReceiver = networkReceiver;
    }

    public Optional<T> readNetworkBuffer() {
        return this.networkReceiver.receive();
    }
}
