package rovu.rover.information;

public interface ProximitySensor {
	double[] read();
}
