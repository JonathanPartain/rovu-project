package rovu.rover;

public interface NetworkTransmitter<T> {
    void transmit(T data);
}
