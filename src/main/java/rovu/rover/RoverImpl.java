package rovu.rover;

import rovu.rover.information.*;
import rovu.rover.motorics.HardwareConnection;


public class RoverImpl implements Rover {
  private final Layer layer;

  public RoverImpl(Layer layer) {
    this.layer = layer;
  }

  public void act() {
    this.layer.handleInformation(null);
  }

}
