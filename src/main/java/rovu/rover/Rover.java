package rovu.rover;

import rovu.communication.Decoder;
import rovu.communication.Encoder;
import rovu.communication.Master;
import rovu.communication.SeverityLevel;
import rovu.rover.information.*;
import rovu.rover.model.ModelLayer;
import rovu.rover.model.RoverModel;
import rovu.rover.model.RoverModelImpl;
import rovu.rover.motorics.*;
import rovu.rover.planning.ActionPlanner;
import rovu.rover.planning.FaultSeverityDeterminatior;
import rovu.rover.planning.PlanningLayer;
import rovu.rover.planning.SeverityDeterminator;

import java.util.LinkedList;
import java.util.Map;

public interface Rover {
  /**
   *
   * @param encoder Transformer of internal data to external data
   * @param decoder Transformer of external data to internal data
   * @param connection Connection to send status to
   * @param hardware Hardware to read from
   * //@param <I> Input type of communication
   * @param <O> Output type of communication
   * @param <T> Type of hardware
   * @return a Rover of type {@link RoverImpl}
   */
  static <O, T extends HardwareConnection & SensorConnection & HardwareBuffer> Rover create(Encoder<Map<String,Object>,O> encoder, Decoder<O,Map<String,Object>> decoder, Master<O> connection,
                                                                                               T hardware) {
    NetworkDevice<Map<String,Object>, O> netDevice = new NetworkDevice<>(encoder, decoder, new LinkedList<>(), connection);

    FaultInterpreter         faultInterpreter         = new FaultInterpreterImpl(hardware);
    CommunicationInterpreter<Map<String,Object>> communicationInterpreter = new CommunicationInterpreterImpl<>(netDevice);

    RoverModel roverModel = new RoverModelImpl(new Boolean[8], hardware.readPosition(), hardware.readDirection());
    SenseInterpreter senseInterpreter = new SenseInterpreterImpl(new ProximitySensorImpl(hardware),
                                                                 new SatelliteSensorImpl(hardware),
                                                                 new CameraImpl(hardware)
    );

    MotoricsManager motoricsManager = new MotoricsManagerImpl(hardware);
    CommunicationManager<Map<String,Object>> communicationManager = new CommunicationManagerImpl<>(netDevice);
    SeverityDeterminator<Fault, SeverityLevel> faultSeverityDeterminator = new FaultSeverityDeterminatior();

    Layer motoricsLayer    = new MotoricsLayer(null, motoricsManager, communicationManager);
    Layer planningLayer    = new PlanningLayer(new ActionPlanner(faultSeverityDeterminator), motoricsLayer);
    Layer modelLayer       = new ModelLayer(roverModel, planningLayer);
    Layer informationLayer = new InformationLayer(senseInterpreter, faultInterpreter, communicationInterpreter, modelLayer);

    Rover rover = new RoverImpl(informationLayer);
    hardware.connect(rover);

    return rover;
  }

  void act();
}
