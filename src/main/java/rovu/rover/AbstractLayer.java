package rovu.rover;

import java.util.function.Function;


public abstract class AbstractLayer<T extends Function, R extends Function> implements Layer<T> {
    private final Layer<R> nextLayer;

    public AbstractLayer(Layer<R> nextLayer) {
        this.nextLayer = nextLayer;
    }

    protected void feedNextLayer(Information<R> information) {
        if(this.nextLayer != null) this.nextLayer.handleInformation(information);

    }
}
