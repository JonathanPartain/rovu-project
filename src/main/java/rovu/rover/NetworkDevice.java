package rovu.rover;

import rovu.communication.*;

import java.util.Map;
import java.util.Optional;
import java.util.Queue;

public class NetworkDevice<I, E> implements NetworkReceiver<I>, NetworkTransmitter<I>, Slave<E> {
	private final Encoder<I, E> encoder;
	private final Decoder<E, I> decoder;
	private final Queue<E> messageBuffer;
	private Connection<E> connection;

	public NetworkDevice(Encoder<I, E> encoder,
                       Decoder<E, I> decoder,
                       Queue<E> messageBuffer,
                       Master<E> master) {
		this.encoder = encoder;
		this.decoder = decoder;
		this.messageBuffer = messageBuffer;
		master.connect(this);
	}

  @Override
  public void connect(Connection<E> master) {
    this.connection = master;
	  System.out.println("Rover connected to master");
  }

  @Override
	public Optional<I> receive() {
		E message = this.messageBuffer.poll();
		if(message == null) {
			return Optional.empty();
		}
		return Optional.of(this.decode(message));
	}

	@Override
	public void send(E data) {
		this.messageBuffer.add(data);
	}

	@Override
	public void transmit(I data) {
		E message = this.encode(data);
		this.connection.send(message);
	}

	private E encode(I message) {
		return this.encoder.encode(message);
	}

	private I decode(E message) {
		return this.decoder.decode(message);
	}
}
