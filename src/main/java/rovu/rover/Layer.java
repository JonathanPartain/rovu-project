package rovu.rover;

import java.util.function.Function;


public interface Layer<T extends Function> {
    void handleInformation(Information<T> information);

}
