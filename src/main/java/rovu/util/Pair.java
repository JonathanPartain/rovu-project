package rovu.util;

public class Pair<FIRST, SECOND> {
	private final FIRST first;
	private final SECOND second;

	public Pair(FIRST first, SECOND second) {
		this.first = first;
		this.second = second;
	}

	public FIRST getFirst() {
		return this.first;
	}

	public SECOND getSecond() {
		return this.second;
	}

	public boolean equals(Pair<FIRST, SECOND> other) {
		return this.first.equals(other.first) && this.second.equals(other.second);
	}

}
