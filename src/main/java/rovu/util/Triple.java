package rovu.util;

public class Triple<FIRST, SECOND, THIRD> {
    private final FIRST first;
    private final SECOND second;
    private final THIRD third;

    public Triple(FIRST first, SECOND second, THIRD third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public FIRST getFirst() {
        return this.first;
    }

    public SECOND getSecond() {
        return this.second;
    }

    public THIRD getThird() {
        return this.third;
    }

}
