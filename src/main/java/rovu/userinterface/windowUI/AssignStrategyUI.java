package rovu.userinterface.windowUI;


import rovu.centralstation.business.mission.Mission;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import rovu.centralstation.business.mission.MissionImpl;
import rovu.centralstation.business.mission.strategy.StrategyType;

import java.util.ArrayList;
import java.util.List;

public class AssignStrategyUI extends JInternalFrame {

    JPanel panel;
    List<Mission> currentMissions;
    JComboBox missionsToSelect;
    JComboBox strategies;
    JButton changeStrat;


    public AssignStrategyUI(List<Mission> missions, WindowUi windowUi) {
        super();
        setTitle("Change Strategy");
        this.panel = new JPanel(new FlowLayout());
        missionsToSelect = new JComboBox();
        this.currentMissions = new ArrayList<>();
        Mission mission = new MissionImpl(0,null, null);
        this.currentMissions.add(mission);

        if(!this.currentMissions.isEmpty()) {
            for (int i = 0; i < this.currentMissions.size(); i++) {
                missionsToSelect.addItem(i);
            }
        }



        //missionsToSelect.addItem(0);
        strategies = new JComboBox(StrategyType.values());

        changeStrat = new JButton("Change Strategy");
        changeStrat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Wolla. Send data about what strategy we want for chosen mission

                if(missionsToSelect.getSelectedItem() != null ) {
                    int tmp = (int) missionsToSelect.getSelectedItem();
                    windowUi.updateStrategy(currentMissions.get(tmp), (StrategyType) strategies.getSelectedItem());
                }
            }
        });
        missionsToSelect.setEditable(true); strategies.setEditable(true);
        missionsToSelect.setVisible(true); strategies.setVisible(true); changeStrat.setVisible(true);
        panel.add(missionsToSelect); panel.add(strategies); panel.add(changeStrat);

        this.add(panel);


    }

    public void repaint(List<Mission> missions){
        this.currentMissions = missions;
        missionsToSelect.removeAllItems();
        for (Mission m: missions) {
            missionsToSelect.addItem(m.getId());
        }
    }
}
