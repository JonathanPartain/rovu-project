package rovu.userinterface.windowUI;

import java.awt.*;

public class AreaUI {
    private final Shape shape;
    private final String name;
    private final Color color;

    public AreaUI(Shape shape, String name, Color color) {
        this.shape = shape;
        this.name = name;
        this.color = color;
    }

    public Shape getShape() {
        return shape;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }
}