package rovu.userinterface.windowUI;

import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;
import rovu.centralstation.business.mission.strategy.Strategy;
import rovu.centralstation.business.mission.strategy.StrategyType;
import rovu.rover.planning.ActionPlanner;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import static rovu.centralstation.business.mission.strategy.StrategyType.SHORTEST_PATH;

public class SupplyMissionUI  extends JInternalFrame{

    private final WindowUi windowUi;
    JPanel panel;
    //List<Vector> objectives = new LinkedList<Vector>();
    JTextField nextXObjective;
    JTextField nextYObjective;
    JComboBox objectivesToSelect;
    JComboBox strategies;


    protected SupplyMissionUI(WindowUi windowUi){
        super();
        this.windowUi = windowUi;
        setTitle("Supply Mission");
        this.panel = new JPanel(new FlowLayout());

        nextXObjective = new JTextField("X",3);
        panel.add(nextXObjective);

        nextYObjective = new JTextField("Y",3);
        panel.add(nextYObjective);


        JButton addButton = new JButton("Add Objective");
        panel.add(addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double xValue = Double.parseDouble(nextXObjective.getText());
                double yValue = Double.parseDouble(nextYObjective.getText());
                Vector newObj = new BasicVector(new double[]{xValue,yValue});
                //objectives.add(newObj);
                objectivesToSelect.addItem(newObj);
                //System.out.println("List: " + objectives.size() + " & ComBox: " + objectivesToSelect.getItemCount());

            }
        });

        objectivesToSelect = new JComboBox();
        objectivesToSelect.setEditable(true);
        objectivesToSelect.setVisible(true);
        panel.add(objectivesToSelect);


        JButton removeButton = new JButton("Remove Objective");
        panel.add(removeButton);
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int indexSelected = objectivesToSelect.getSelectedIndex();
                objectivesToSelect.removeItemAt(indexSelected);
                //objectives.remove(indexSelected);
                objectivesToSelect.updateUI();
                //System.out.println("List: " + objectives.size() + " & ComBox: " + objectivesToSelect.getItemCount());
            }
        });

        strategies = new JComboBox(StrategyType.values());
        strategies.setEditable(true);
        strategies.setVisible(true);
        panel.add(strategies);

        JButton supplyMissionButton = new JButton("Supply Mission");
        panel.add(supplyMissionButton);
        supplyMissionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Wolla. Send mission to central station
                List<Vector> objectives = new LinkedList<Vector>();
                for(int i = 0; i<objectivesToSelect.getItemCount(); i++){
                    objectives.add((Vector) objectivesToSelect.getItemAt(i));
                }
                System.out.println("objectives: " + objectives);
                System.out.println("Strategy: " + strategies.getSelectedItem());
                windowUi.sendMission(objectives, (StrategyType) strategies.getSelectedItem());
                privRepaint();
            }
        });


        this.add(panel);

    }

    private void privRepaint(){
        this.objectivesToSelect.removeAllItems();
    }


}
