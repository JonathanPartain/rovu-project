package rovu.userinterface.windowUI;

import java.awt.*;

public class Wall {
    private final Shape wall;

    public Wall(Shape wall) {
        this.wall = wall;
    }

    public Shape getShape() {
        return wall;
    }
}