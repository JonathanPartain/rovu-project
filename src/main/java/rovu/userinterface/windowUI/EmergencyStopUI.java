package rovu.userinterface.windowUI;

import rovu.centralstation.business.mission.strategy.StrategyType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import static rovu.centralstation.business.mission.strategy.StrategyType.SHORTEST_PATH;

public class EmergencyStopUI extends JInternalFrame {

    List<Rover> rovers;
    JPanel panel;
    JComboBox roversToSelect;

    public EmergencyStopUI(List<Rover> rovers, WindowUi windowUi) {
        super();

        this.rovers = rovers;

        setTitle("Emergency stop");
        this.panel = new JPanel(new FlowLayout());

        panel.add(new JLabel("Stop: "));
        roversToSelect = new JComboBox();

        repaint(rovers);

        roversToSelect.setEnabled(true);
        roversToSelect.setVisible(true);

        panel.add(roversToSelect);
        JButton stopRover = new JButton("STOP!");
        panel.add(stopRover);
        stopRover.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i = 0; i < rovers.size(); i++) {
                    //TODO - does this part work "roversToSelect.getSelectedItem().toString()"
                    if (rovers.get(i).getId() == (int) roversToSelect.getSelectedItem()) {
                        windowUi.emergencyStop(rovers.get(i).getId());
                    }
                }

            }
        });

        this.add(panel);
    }

    public void repaint(List<Rover> rovers){
        this.rovers = rovers;
        roversToSelect.removeAllItems();
        for (Rover r: rovers) {
            roversToSelect.addItem(r.getId());
        }
    }


}
