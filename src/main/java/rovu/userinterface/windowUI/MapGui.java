package rovu.userinterface.windowUI;

import javax.swing.*;
import javax.vecmath.Point2d;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MapGui extends JInternalFrame {
    DrawingArea drawingArea;
    LegendArea legendArea;

    List<AreaUI> physicalAreas, logicalAreas;
    List<Wall> walls;
    List<Rover> rovers;


    public MapGui() {
        super();
        this.rovers = new ArrayList<>();
        this.physicalAreas = new ArrayList<>();
        this.logicalAreas = new ArrayList<>();
        this.walls = new ArrayList<>();

        drawingArea = new DrawingArea(this.physicalAreas, this.logicalAreas, this.walls);
        drawingArea.setVisible(true);
        drawingArea.setSize(500, 500);
        this.getContentPane().add(drawingArea, BorderLayout.CENTER);

        legendArea = new LegendArea(this.physicalAreas, this.logicalAreas, drawingArea);
        legendArea.setVisible(true);
        this.getContentPane().add(legendArea, BorderLayout.EAST);
        update(rovers, walls, physicalAreas, logicalAreas);


        //this.getContentPane().add(new JRadioButton("physical areas"));
        //this.getContentPane().add(areasSelection);
        this.setTitle("Map");
    }


    private class LegendArea extends JPanel {
        DrawingArea drawingArea;
        ButtonGroup areasSelection;
        JRadioButtonMenuItem physicalAreasButton, logicalAreasButton;

        List<AreaUI> physicalAreas, logicalAreas, current;

        public LegendArea(List<AreaUI> physicalAreas, List<AreaUI> logicalAreas, DrawingArea drawingArea) {
            super(new GridLayout(0, 1));
            this.drawingArea = drawingArea;

            this.physicalAreas = physicalAreas;
            this.logicalAreas = logicalAreas;
            this.current = physicalAreas;

            areasSelection = new ButtonGroup();
            physicalAreasButton = new JRadioButtonMenuItem("Physicals Areas");
            logicalAreasButton = new JRadioButtonMenuItem("Logicals Areas");
            physicalAreasButton.setSelected(true);


            logicalAreasButton.addActionListener(actionEvent -> {
                if (current != logicalAreas) {
                    current = logicalAreas;
                    this.build();
                    drawingArea.switchCurrent();
                    drawingArea.repaint();
                }

            });
            physicalAreasButton.addActionListener(actionEvent -> {
                if (current != physicalAreas) {
                    current = physicalAreas;
                    this.build();
                    drawingArea.switchCurrent();
                    drawingArea.repaint();
                }
            });


            areasSelection.add(logicalAreasButton);
            areasSelection.add(physicalAreasButton);



            build();
        }

        private void build() {
            this.removeAll();

            for (AreaUI area: current) {
                this.add(new AreaLegendPanel(area));
            }


            this.add(physicalAreasButton);
            this.add(logicalAreasButton);
            this.validate();
            this.repaint();
        }

    }

    private class DrawingArea extends JPanel {
        List<AreaUI> physicalAreas, logicalAreas, current;
        List<Wall> walls;

        public DrawingArea(List<AreaUI> physicalAreas,List<AreaUI> logicalAreas, List<Wall> walls) {
            this.physicalAreas = physicalAreas;
            this.logicalAreas = logicalAreas;

            this.current = physicalAreas;
            this.walls = walls;
        }

        public void switchCurrent() {
            if (current == physicalAreas) {
                current = logicalAreas;
            } else {
                current = physicalAreas;
            }
        }
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            Graphics2D g2d = (Graphics2D) g;
            RenderingHints rh = new RenderingHints(
                    RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2d.setRenderingHints(rh);

            paintCurrent(g2d);
            paintWalls(g2d);
            paintRovers(g2d);
        }

        private void paintCurrent(Graphics2D g2d) {
            for (AreaUI area: current) {
                g2d.setColor(area.getColor());
                g2d.draw(area.getShape());
                g2d.fill(area.getShape());
            }
        }

        private void paintWalls(Graphics2D g2d) {
            for (Wall wall: walls) {
                g2d.setColor(Color.black);
                g2d.draw(wall.getShape());
                g2d.fill(wall.getShape());
            }
        }

        private void paintRovers(Graphics2D g2d) {
            g2d.setColor(Color.black);

            for (Rover rover: rovers) {
                Point2D position = rover.getPosition();

                Line2D.Double cross1, cross2;
                cross1 = new Line2D.Double(
                        position.getX() - 5,
                        position.getY() - 5,
                        position.getX() + 5,
                        position.getY() + 5
                );
                cross2 = new Line2D.Double(
                        position.getX() - 5,
                        position.getY() + 5,
                        position.getX() + 5,
                        position.getY() - 5
                );

                g2d.draw(cross1);
                g2d.draw(cross2);

                g2d.drawString(rover.getName(), (float)position.getX(), (float)position.getY() + 15);
            }
        }
    }

    private class AreaLegendPanel extends JPanel{
        AreaUI area;

        JLabel name;
        JLabel color;

        public AreaLegendPanel(AreaUI area) {
            this.setLayout(new FlowLayout());
            this.area = area;

            name = new JLabel(area.getName());
            color = new JLabel("■");
            color.setForeground(area.getColor());

            name.setVisible(true);
            color.setVisible(true);
            this.add(name);
            this.add(color);

            this.setVisible(true);
        }
    }

    public void update(List<Rover> rovers, List<Wall> walls, List<AreaUI> physical, List<AreaUI> logical){
        this.rovers = rovers;

        this.physicalAreas.clear();
        this.physicalAreas.addAll(physical);

        this.logicalAreas.clear();
        this.logicalAreas.addAll(logical);

        this.walls.clear();
        this.walls.addAll(walls);

        this.repaint();
        this.legendArea.repaint();
    }

}
