package rovu.userinterface.windowUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UserSettingsUI extends JInternalFrame {

    JPanel panel;
    JTextField currentUser;
    JButton changeUser;
    //boolean technicalUser;


    public UserSettingsUI(WindowUi windowUi) {
        super();
        setTitle("Set User");
        this.panel = new JPanel(new FlowLayout());
        currentUser = new JTextField("Non-technical");

        changeUser = new JButton("Change User");
        changeUser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean tmp = windowUi.getTechnicalOperator();
                if(tmp) {
                    currentUser.setText("Non-Technical");
                }else{
                    currentUser.setText("Technical");
                }
                windowUi.changeOperatorStatus();

            }
        });

        currentUser.setVisible(true); changeUser.setVisible(true);
        panel.add(currentUser); panel.add(changeUser);

        this.add(panel);


    }
}

