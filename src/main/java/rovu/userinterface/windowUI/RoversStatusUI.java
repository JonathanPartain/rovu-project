package rovu.userinterface.windowUI;

import rovu.centralstation.business.mission.Mission;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class RoversStatusUI extends JInternalFrame {
    //List<Rover> rovers; //Should not hold its own list probably, it should just have function that takes in a list and the rePaints
    JPanel panel;
    //List<JPanel> roverPanels;

    RoversStatusUI(List<Rover> rovers, WindowUi windowUi) {
        //this.rovers = rovers;

        panel = new JPanel();
        for (Rover r: rovers) {
            JPanel tmp = new RoverPanel(r);
            tmp.setVisible(true);
            panel.add(tmp);
        }
        this.setTitle("Rovers status");

        panel.setLayout(new GridLayout(0, 1));
        panel.setVisible(true);
        this.add(panel);
    }

    private void setRoverPanels(List<Rover> rovers){
        this.panel.removeAll();
        for (Rover r: rovers) {
            JPanel tmp = new RoverPanel(r);
            tmp.setVisible(true);
            this.panel.add(tmp);
        }
    }

    public void updateRovers(List<Rover> rovers){
        setRoverPanels(rovers);
    }

    private class RoverPanel extends JPanel{
        Rover rover;
        JLabel name;
        JLabel status;

        public RoverPanel(Rover rover) {
            this.setLayout(new FlowLayout());
            this.rover = rover;

            name = new JLabel(rover.getName());
            status = new JLabel("■");

            Color color = null;
            String toolTip = "";
            switch (rover.getFault()) {
                case NONE:
                    color = Color.green;
                    toolTip = "No problem";
                    break;
                case IMPAIRING:
                    color = Color.yellow;
                    toolTip = "Minor impairing";
                    break;
                case SEVERE:
                    color = Color.orange;
                    toolTip = "Severe impairing";
                    break;
                case CRITICAL:
                    color = Color.red;
                    toolTip = "Critical impairing";
                    break;
            }

            status.setForeground(color);
            status.setToolTipText(toolTip);
            name.setVisible(true);
            status.setVisible(true);
            this.add(name);
            this.add(status);
        }
    }


}
