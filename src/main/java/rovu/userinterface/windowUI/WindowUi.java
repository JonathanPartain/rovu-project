package rovu.userinterface.windowUI;

import org.la4j.Vector;
import rovu.centralstation.business.SystemStatus;
import rovu.centralstation.business.domain.Area;
import rovu.centralstation.business.mission.Mission;
import rovu.centralstation.business.mission.strategy.Strategy;
import rovu.centralstation.business.mission.strategy.StrategyType;
import rovu.communication.SeverityLevel;
import rovu.userinterface.UI;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class WindowUi extends JFrame {
    private final UI ui;
    private JDesktopPane desktop;
    private final MapGui mapGui;
    private final EmergencyStopUI emergencyStopUI;
    private final RoversStatusUI roversStatusUI;
    private final SupplyMissionUI supplyMissionUI;
    private final AssignStrategyUI assignStrategyUI;
    private final UserSettingsUI userSettingsUI;
    private boolean technicalOperator;
    private List<Rover> rovers;

    private final Color colors[] = {Color.blue, Color.red, Color.orange, Color.green, Color.yellow, Color.magenta, Color.cyan};


    public WindowUi(UI ui) { //WIll need to take in rovers
        desktop = new JDesktopPane();
        setContentPane(desktop);
        this.ui = ui;

        this.rovers = new ArrayList<>();


        this.mapGui = new MapGui();
        mapGui.setSize(600, 600);
        mapGui.setLocation(0,0);
        mapGui.setVisible(true);

        this.userSettingsUI = new UserSettingsUI(this);
        userSettingsUI.setSize(200, 200);
        userSettingsUI.setLocation(700,200);
        userSettingsUI.setVisible(true);


        this.roversStatusUI = new RoversStatusUI(rovers, this);
        roversStatusUI.setSize(200, 200);
        roversStatusUI.setLocation(700,0);
        roversStatusUI.setVisible(true);

        this.supplyMissionUI = new SupplyMissionUI(this);
        supplyMissionUI.setSize(250, 300);
        supplyMissionUI.setLocation(900, 410);


        this.assignStrategyUI = new AssignStrategyUI( null,this);
        assignStrategyUI.setSize(250, 200);
        assignStrategyUI.setLocation(900, 0);



        this.emergencyStopUI = new EmergencyStopUI(rovers, this);
        emergencyStopUI.setSize(250, 200);
        emergencyStopUI.setLocation(900, 205);


        desktop.add(emergencyStopUI);
        desktop.add(supplyMissionUI);
        desktop.add(assignStrategyUI);

        desktop.add(mapGui);
        desktop.add(roversStatusUI);
        desktop.add(userSettingsUI);
    }


    public void changeOperatorStatus(){
        this.technicalOperator = !this.technicalOperator;
        paintTechnical();
    }


    private void paintTechnical(){
        if(this.technicalOperator) {
            emergencyStopUI.setVisible(true);
            assignStrategyUI.setVisible(true);
            supplyMissionUI.setVisible(true);

        }else{
            emergencyStopUI.setVisible(false);
            assignStrategyUI.setVisible(false);
            supplyMissionUI.setVisible(false);
        }
    }


    protected boolean getTechnicalOperator(){
        return this.technicalOperator;
    }


    public void sendMission(List<Vector> mission, StrategyType strategy){
        System.out.println("Mission Supplied - mission first objective: (" + mission.get(0).get(0) + ")," + mission.get(0).get(1) +
                ". Strategy: " + strategy.name());

        this.ui.sendMission(mission, strategy);
    }


    public void updateStrategy(Mission mission, StrategyType strategy){
        System.out.println("Mission Updated - Id: " + mission.getId() + ". Objectives: " + mission.getObjectives()
        + ". OldStrat: " + mission.getStrategy() + ". NewStrat: " + strategy.name());
        this.ui.updateStrategy(mission.getId(), strategy);
    }



    public void emergencyStop(int roverId){
        System.out.println("Rover with id: " + roverId + " stopped!");
        this.ui.emergencyStop(roverId);
    }

    public void updateUI(
            List<Point2D.Double> roverPositions,
            List<Mission> missions,
            List<Mission> missionsNotAssigned,
            List<SeverityLevel> faults,
            Set<Area> physical,
            Set<Area> logicals,
            Set<Area> obstructed
        ){ //should also take in mission
        List<Rover> rovers = new ArrayList<Rover>();
        List<Wall> walls = new ArrayList<>();
        List<AreaUI> physicalAreas = new ArrayList<>();
        List<AreaUI> logicalAreas = new ArrayList<>();


        AffineTransform af = new AffineTransform();
        af.translate(200, 200);
        af.scale(20, 20);

        for (Area a: obstructed) {
            walls.add(new Wall(af.createTransformedShape(a.getBoundary())));
        }
        int cpt = 0;

        for (Area a: physical) {
            physicalAreas.add(new AreaUI(af.createTransformedShape(a.getBoundary()), a.getName(), this.colors[cpt]));
            cpt++;
        }

        cpt = 0;
        for (Area a: logicals) {
            logicalAreas.add(new AreaUI(af.createTransformedShape(a.getBoundary()), a.getName(), this.colors[cpt]));
            cpt++;
        }

        for(int i = 0; i<roverPositions.size(); i++){
            rovers.add(new Rover(
                    i,
                    new Point2D.Double(
                            (roverPositions.get(i).x *10) + 200,
                            (roverPositions.get(i).y *10) + 200),
                    SeverityLevel.NONE));
        }

        this.rovers.clear();
        this.rovers.addAll(rovers);
        emergencyStopUI.repaint(rovers);
        assignStrategyUI.repaint(missions);
        roversStatusUI.updateRovers(rovers);
        mapGui.update(
                rovers,
                walls,
                physicalAreas,
                logicalAreas
        );
    }
}
