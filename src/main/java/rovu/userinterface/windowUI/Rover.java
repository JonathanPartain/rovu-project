package rovu.userinterface.windowUI;

import rovu.communication.SeverityLevel;

import java.awt.geom.Point2D;

public class Rover {

    private final String name;
    private final int id;
    private Point2D.Double position;
    private SeverityLevel fault;

    public Rover(int id, Point2D.Double position) {
        this(id, position, SeverityLevel.NONE);
    }

    public Rover(int id, Point2D.Double position, SeverityLevel fault) {
        this.name = "Rover n°" + id;
        this.id = id;
        this.position = position;
        this.fault = fault;
    }

    public String getName() {
        return this.name;
    }


    public int getId() {
        return id;
    }

    public Point2D.Double getPosition() {
        return position;
    }

    public void setPosition(Point2D.Double position) {
        this.position = position;
    }

    public SeverityLevel getFault() {
        return this.fault;
    }

    public void setFault(SeverityLevel fault) {
        this.fault = fault;
    }
}
