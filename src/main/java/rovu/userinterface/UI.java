package rovu.userinterface;

import org.la4j.Vector;
import rovu.centralstation.TimerObservable;
import rovu.centralstation.TimerObserver;
import rovu.centralstation.business.mission.Mission;
import rovu.centralstation.business.mission.strategy.Strategy;
import rovu.centralstation.business.mission.strategy.StrategyType;
import rovu.centralstation.business.rover.Rover;
import rovu.centralstation.information.InformationLayer;
import rovu.userinterface.windowUI.WindowUi;

import javax.swing.*;
import java.awt.*;
import java.sql.Time;
import java.util.List;
import java.util.Map;

public class UI implements TimerObserver {

    WindowUi windowUi;
    private Observer getter; //must be assigned. Not sure if created in UI or passed through constructor
    private ActionGiver actionGiver;
    private int lastCalculationTime;

    public UI(TimerObservable timerObservable, InformationLayer<Map<String, Object>>  il, Observer getter){
        windowUi = new WindowUi(this);
        windowUi.setVisible(true);
        timerObservable.subscribe(this);
        this.lastCalculationTime = 0;
        this.getter = getter;
        actionGiver = new ActionGiver(il);




        windowUi.setTitle("GUI");
        windowUi.setExtendedState(JFrame.MAXIMIZED_BOTH);
        windowUi.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void sendMission(List<Vector> mission, StrategyType strategy){
        actionGiver.sendMission(mission, strategy);
    }

    public void updateStrategy(int missionId, StrategyType strategy){
        actionGiver.updateStrategy(missionId, strategy);
    }


    public void emergencyStop(int roverId){
        actionGiver.emergencyStop(roverId);
    }


    public void updateUI(){

        //Get data from observe and send it to windowUI
        windowUi.updateUI(getter.getRoverPositions(),
                getter.getMissions(),
                getter.getMissionsNotAssigned(),
                getter.getRoverFaults(),
                getter.getPhysicals(),
                getter.getLogicals(),
                getter.getObstructeds()
        );

    }

    @Override
    public void update(int time) {
        if (time - lastCalculationTime > 0.1) {
            lastCalculationTime = time;
            this.updateUI();
            System.out.println("UI updated");
        }

        System.out.println("!!!Current time: " + time);
    }


}
