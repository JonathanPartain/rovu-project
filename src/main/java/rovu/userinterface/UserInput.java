package rovu.userinterface;


public abstract class UserInput {

    public boolean technicalOperator;

    public abstract void supplyObjective();
    public abstract void stopRover(int roverId); //Rover rover istället för roverId i mpdellen, men behövs det?
    public abstract boolean connect();
    //public abstract void assignStrategy(Objective obj); //

}
