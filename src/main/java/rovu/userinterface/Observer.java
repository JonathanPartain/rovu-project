package rovu.userinterface;

import rovu.centralstation.business.SystemStatusObserver;
import rovu.centralstation.business.domain.Area;
import rovu.centralstation.business.mission.Mission;
import rovu.centralstation.business.rover.Rover;
import rovu.communication.SeverityLevel;

import java.awt.geom.Point2D;
import java.util.*;


public class Observer implements SystemStatusObserver{

    private List<Rover> rovers;
    private List<Point2D.Double> roverPositions;
    private List<Mission> missions;
    private List<Mission> missionsNotAssigned;
    private List<SeverityLevel> roverFaults;
    private Set<Area> logicals, physicals, obstructeds;


    public void onUpdate(Map<String,Object> data){ //Maybe split up in to two

        rovers = (List<Rover>) data.get("ROVERS");
        missions = (List<Mission>) data.get("MISSIONS");
        missionsNotAssigned = (List<Mission>) data.get("MISSIONS_NOT_ASSIGNED");
        physicals = (Set<Area>) data.get("PHYSICAL_AREAS");
        logicals = (Set<Area>) data.get("LOGICAL_AREAS");
        obstructeds = (Set<Area>) data.get("OBSTRUCTED_AREAS");

        /*rovers = new ArrayList<>();
        rovers.add(Rover.create(0));*/

        List<Point2D.Double> pos = new ArrayList<>();
        List<SeverityLevel> faults = new ArrayList<>();
//        System.out.println("rovers size: " + rovers.size());
        for(int i = 0; i<rovers.size();i++){
            if(rovers.get(i).getPosition() != null) {
                Point2D.Double tmpPos = new Point2D.Double(
                        rovers.get(i).getPosition().get(0),
                        rovers.get(i).getPosition().get(1));
                pos.add(tmpPos);
                SeverityLevel tmpFault = rovers.get(i).getFault();
                faults.add(tmpFault);
            }
        }
        roverPositions = pos;
        roverFaults = faults;



    }

    public List<Point2D.Double> getRoverPositions(){
        return this.roverPositions;
    };

    public List<SeverityLevel> getRoverFaults(){
        return roverFaults;
    };


    public List<Mission> getMissions(){
        return this.missions;
    };

    public List<Mission> getMissionsNotAssigned(){
        return this.missionsNotAssigned;
    };

    @Override
    public void onSystemChange(Map<String,Object> data) {
        this.onUpdate(data);
    }


    public Set<Area> getLogicals() {
        return logicals;
    }

    public Set<Area> getPhysicals() {
        return physicals;
    }

    public Set<Area> getObstructeds() {
        return obstructeds;
    }
}
