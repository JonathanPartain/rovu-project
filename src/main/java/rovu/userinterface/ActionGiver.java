package rovu.userinterface;

import org.la4j.Vector;
import rovu.centralstation.business.mission.strategy.StrategyType;
import rovu.centralstation.information.InformationLayer;
import rovu.communication.Protocol;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActionGiver {

    InformationLayer informationLayer;

    ActionGiver(InformationLayer<Map<String, Object>> il){
        this.informationLayer = il;
    }

    public void sendMission(List<Vector> mission, StrategyType strategy){
        Map<String,Object> data = new HashMap(3);
        data.put(Protocol.COMMAND, Protocol.Commands.MISSION_ASSIGNMENT);
        data.put(Protocol.MISSION,mission);
        data.put(Protocol.STRATEGY, strategy);
        informationLayer.processData(data);

    }

    public void updateStrategy(int missionId, StrategyType strategy){

        Map<String,Object> data = new HashMap(3);
        data.put(Protocol.COMMAND, Protocol.Commands.STRATEGY_UPDATE);
        data.put(Protocol.MISSION_ID,missionId);
        data.put(Protocol.STRATEGY, strategy);
        informationLayer.processData(data);
    }

    public void emergencyStop(int roverId){

        Map<String,Object> data = new HashMap(2);
        data.put(Protocol.COMMAND, Protocol.Commands.STOP_ROVER);
        data.put(Protocol.ROVER_ID,roverId);
        informationLayer.processData(data);

    }
}
