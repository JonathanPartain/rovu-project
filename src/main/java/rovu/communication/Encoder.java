package rovu.communication;

public interface Encoder<T, R> {
  R encode(T message);
}
