package rovu.communication;

public interface Decoder<T, R> {
  R decode(T message);
}
