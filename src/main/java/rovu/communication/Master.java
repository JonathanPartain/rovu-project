package rovu.communication;

public interface Master<T> {
  void connect (Slave<T> connection);
}
