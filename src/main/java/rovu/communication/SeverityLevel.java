package rovu.communication;

public enum SeverityLevel {
	NONE,
	IMPAIRING,
	SEVERE,
	CRITICAL
}
