package rovu.communication;

public class Protocol {
  public static final String FAULT = "Fault";
  public static final String POSITION = "Position";
  public static final String OBJECTIVE = "Objective";
  public static final String ROVER_ID = "RoverId";
  public static final String STRATEGY = "Strategy";
  public static final String MISSION = "Mission";
  public static final String COMMAND = "Command";
  public static final String MISSION_ID = "MissionId";
  public static final String DELAY = "Delay";
  public static final String ROVERS = "Rovers";
  public static final String AREAS = "Areas";
  public static final String WALLS = "Walls";

  public enum Commands {
    ROVER_UPDATE,
    ROVER_CONNECTION,
    MISSION_ASSIGNMENT,
    STOP_ROVER,
    STRATEGY_UPDATE,
    NEXT_OBJECTIVE,
    VISUALIZE,
  }
}
