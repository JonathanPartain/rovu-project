package rovu.communication;

public interface Connection<T> {
	void send(T data);
}
