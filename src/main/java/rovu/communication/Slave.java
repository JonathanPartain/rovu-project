package rovu.communication;

public interface Slave<T> extends Connection<T>{
  void connect(Connection<T> master);
}
