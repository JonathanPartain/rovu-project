package rovu.robotsimulator;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.la4j.Vector;
import rovu.centralstation.business.domain.Area;
import rovu.util.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PlatformEnvironment implements EnvironmentContext {
	private final Set<Area> physicalAreas;
	private final Set<Area> logicalAreas;
	private final Map<String, Set<Area>> categories;
	private final Graph<Vector, DefaultEdge> locationGrid;
	//private final LocationMasker locationMasker;

	public PlatformEnvironment() {
		this.physicalAreas = new HashSet<>();
		this.logicalAreas = new HashSet<>();
		this.categories = new HashMap<>();
		this.locationGrid = new SimpleGraph<>(DefaultEdge.class);
	}

	@Override
	public void addWall(Vector start, Vector end) {

	}

	@Override
	public void addArea(Vector start, Vector end, Pair<Vector, Vector>[] walls) {

	}
}
