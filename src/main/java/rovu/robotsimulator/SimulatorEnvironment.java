package rovu.robotsimulator;

import com.sun.org.apache.xml.internal.utils.DefaultErrorHandler;
import org.la4j.Vector;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import rovu.util.Pair;
import simbad.sim.Agent;
import simbad.sim.Box;
import simbad.sim.EnvironmentDescription;

import javax.vecmath.Color3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SimulatorEnvironment implements EnvironmentContext {
	private final EnvironmentDescription env;

	SimulatorEnvironment(EnvironmentDescription env) {
		this.env = env;
		this.env.floorColor = this.env.white;
		this.env.wallColor = this.env.white;
	}

	public void setWallColor(Color3f color) {
		this.env.wallColor = color;
	}

	@Override
	public void addWall(Vector start, Vector end) {
		Vector direction = end.subtract(start);
		Box    wall;
		float  width;
		float  height    = 2F;
		float  depth;

		width = (float) Math.abs(direction.get(0));
		depth = (float) Math.abs(direction.get(1));

		Vector3d corner = new Vector3d(start.get(0), 0D, start.get(1));
		Vector3d center = new Vector3d(start.get(0) + (direction.get(0) / 2), 0d, (start.get(1) + (direction.get(1) / 2)));
		Vector3f extent = new Vector3f(width, height, depth);

		wall = new Box(center, extent, this.env, this.env.wallColor);

		this.env.add(wall);
	}

	@Override
	public void addArea(Vector start, Vector end, Pair<Vector, Vector>[] walls) {
		for ( Pair<Vector, Vector> wall :
						walls ) {
			this.addWall(wall.getFirst(), wall.getSecond());
		}
	}

	public EnvironmentDescription getEnv() {
		return env;
	}

	public Set<Agent> loadEnvironmentFromXMLFile(File source) throws SAXException, ParserConfigurationException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setIgnoringElementContentWhitespace(true);


		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		File xsd = new File("src/main/java/rovu/robotsimulator/simulatorEnvironment.xsd");
		final Schema schema = sf.newSchema(xsd);
		dbf.setSchema(schema);
		DocumentBuilder db = dbf.newDocumentBuilder();
		db.setErrorHandler(new DefaultErrorHandler());
		Document doc = db.parse(source);

		NodeList walls = doc.getElementsByTagName("wall");
		NodeList rovers = doc.getElementsByTagName("rover");

		for (int i = 0; i < walls.getLength(); i++) {
			Vector start = getVector(walls.item(i).getFirstChild()), end = getVector(walls.item(i).getLastChild());

			Element element = (Element)walls.item(i);
			String stringColor = element.getAttribute("color");
			Color3f color;

			switch (stringColor) {
				case "blue":
					color = new Color3f(Color.blue);
					break;
				case "red":
					color = new Color3f(Color.red);
					break;
				case "white":
					color = new Color3f(Color.white);
					break;
				default:
					color = new Color3f(Color.black);
					break;
			}

			this.setWallColor(color);
			this.addWall(start, end);
		}

		Set<Agent> result = new HashSet<>();
		for (int i = 0; i < rovers.getLength(); i++) {
			Vector position = getVector(rovers.item(i).getFirstChild());

			Element element = (Element)rovers.item(i);
			String name = element.getAttribute("color");

			result.add(RoverFactory.createRover(position.get(0), position.get(1), name));
		}

		return result;
	}

	private Vector getVector(Node node) {
		Node x = node.getFirstChild();
		Node y = node.getLastChild();

		double xF = Double.parseDouble(x.getTextContent());
		double yF = -Double.parseDouble(y.getTextContent());

		return Vector.fromArray(new double[]{xF,yF});
	}



}
