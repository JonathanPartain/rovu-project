package rovu.robotsimulator;

public class DoorTooSmallException extends Exception {

    public DoorTooSmallException() {}

    public DoorTooSmallException(String message) {
        super(message);
    }

}
