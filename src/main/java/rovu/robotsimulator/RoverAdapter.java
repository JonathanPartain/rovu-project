package rovu.robotsimulator;

import org.la4j.Matrix;
import org.la4j.Vector;
import rovu.rover.Fault;
import rovu.rover.information.HardwareBuffer;
import rovu.rover.information.SensorConnection;
import rovu.rover.motorics.HardwareConnection;

import javax.vecmath.Matrix3d;
import javax.vecmath.Vector3d;

public class RoverAdapter implements HardwareConnection, SensorConnection, HardwareBuffer {
	private final Rover rover;

	public RoverAdapter(Rover rover) {
		this.rover = rover;
	}

	@Override
	public Fault readFault() {
		return Fault.NONE;
	}

	@Override
	public double[] readProximity() {
		return rover.readRangeSensorBelt();
	}

	@Override
	public Vector readPosition() {
		return this.transformSpace(this.rover.getPosition());
	}

	@Override
	public Vector readDirection() {
		Matrix3d rotationMatrix = new Matrix3d();
		this.rover.getRotation().get(rotationMatrix);

		Matrix rotation = transformSpace(rotationMatrix);

		return Vector.fromArray(new double[]{1, 0}).multiply(rotation);
	}

	@Override
	public void setVelocity(double velocity) {
		rover.setTranslationalVelocity(velocity);
	}

	@Override
	public void setRotationVelocity(double velocity) {
		rover.setRotationalVelocity(velocity);
	}

	@Override
	public void connect(rovu.rover.Rover software) {
		this.rover.connect(software);
	}

	private Vector transformSpace(Vector3d vector) {
		return Vector.fromArray(new double[]{vector.x, -vector.z});
	}

	private Vector3d transformSpace(Vector vector) {
		return new Vector3d(vector.get(0), 0, -vector.get(1));
	}

	private Matrix transformSpace(Matrix3d matrix) {
		return Matrix.from2DArray(new double[][]{
				{matrix.m00, -matrix.m20},
				{matrix.m02, -matrix.m22}
		});
	}
}
