package rovu.robotsimulator;


import rovu.centralstation.TimerObservable;
import rovu.centralstation.TimerObserver;
import simbad.sim.Agent;

import javax.media.j3d.BranchGroup;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;

public class SecondsTimerAgent extends Agent implements TimerObservable {


    private List<TimerObserver> timerObservers;
    private int seconds;
    private int frameCounter;
    public SecondsTimerAgent(Vector3d pos, String name) {
        super(pos, name);
        seconds = 0;
        frameCounter = 0;
        timerObservers = new ArrayList<>();
        // this.detach();
    }
    @Override
    protected void integratesPositionChange(double dt) {
        // Do nothing
    }

    protected void updateSensors(double elapsedSecond, BranchGroup pickableSceneBranch) {
        // Do nothing
    }

    @Override
    protected void create3D() {
        // Do nothing
    }
    @Override
    protected void performBehavior() {
        frameCounter++;

        // In normal mode 20 FPS
        if (frameCounter == 20) {
            seconds++;
            notifyObservers();
            frameCounter = 0;
        }
    }

    public int getTime() {
        return seconds;
    }

    @Override
    public void notifyObservers() {
        for (TimerObserver timerObserver : timerObservers) {
            timerObserver.update(this.seconds);
        }
    }

    @Override
    public void subscribe(TimerObserver timerObserver) {
        timerObservers.add(timerObserver);
    }

    @Override
    public void unsubscribe(TimerObserver timerObserver) {
        timerObservers.remove(timerObserver);
    }
}
