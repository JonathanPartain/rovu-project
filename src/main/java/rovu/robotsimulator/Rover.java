package rovu.robotsimulator;

import javax.media.j3d.Transform3D;
import javax.vecmath.Vector3d;

public interface Rover {
	Vector3d getPosition();
	Transform3D getRotation();
	double[] readRangeSensorBelt();
	void setTranslationalVelocity(double velocity);
	void setRotationalVelocity(double velocity);
	void connect(rovu.rover.Rover softwareConnection);
}
