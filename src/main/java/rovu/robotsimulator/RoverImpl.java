package rovu.robotsimulator;

import simbad.sim.*;

import javax.vecmath.Vector3d;

public class RoverImpl extends Agent implements Rover {

	private final RangeSensorBelt sonars;
	private final CameraSensor camera;
	private double velocity;
	private double rotationalVelocity;
	private rovu.rover.Rover softwareConnection;

	public RoverImpl(Vector3d pos, String name, int nbProximity) {
		super(pos, name);
		// Add camera
		this.camera = RobotFactory.addCameraSensor(this);
		// Add sonars
		this.sonars = RobotFactory.addSonarBeltSensor(this, nbProximity);

		this.velocity = 0;
		this.rotationalVelocity = 0;
		this.softwareConnection = null;
	}

	public RoverImpl(Vector3d pos, String name) {
		super(pos, name);
		// Add camera
		this.camera = RobotFactory.addCameraSensor(this);
		// Add sonars
		this.sonars = RobotFactory.addSonarBeltSensor(this, 8);

//		this.radius = 0.2f;
		this.velocity = 0;
		this.rotationalVelocity = 0;
		this.softwareConnection = null;
	}

	@Override
	public void initBehavior() {
		// nothing particular in this case
		setRotationalVelocity(0);
		setTranslationalVelocity(0);
	}

	public double[] readRangeSensorBelt() {
		int      nSonars       = this.sonars.getNumSensors();
		double[] sensorResults = new double[nSonars];
		for ( int i = 0; i < nSonars; i++ ) {
			sensorResults[i] = this.sonars.getMeasurement(i);
		}
		return sensorResults;
	}

	@Override
	public void connect(rovu.rover.Rover softwareConnection) {
		if (this.softwareConnection != null) throw new IllegalAccessError("Connection already established");
		this.softwareConnection = softwareConnection;
	}

	@Override
	protected void performBehavior() {
		this.softwareConnection.act();
	}

	@Override
	public String toString() {
		return "Rover " + name;
	}
}
