package rovu.robotsimulator;

import org.la4j.Vector;
import rovu.util.Pair;
import simbad.sim.EnvironmentDescription;

import java.util.List;

public class SimulatorFactory extends EnvironmentFactory<SimulatorEnvironment, EnvironmentDescription> {
	public SimulatorFactory(SimulatorEnvironment ctx) {
		super(ctx);
	}

	@Override
	public void addArea(Vector from, Vector to, String name) { }

	@Override
	public EnvironmentDescription getEnvironment() {
		return this.ctx.getEnv();
	}
}
