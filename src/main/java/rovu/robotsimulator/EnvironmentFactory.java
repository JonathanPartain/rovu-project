package rovu.robotsimulator;

import org.la4j.Vector;

public abstract class EnvironmentFactory<T extends EnvironmentContext, R> {

	protected final T ctx;

	public EnvironmentFactory(T ctx) {
		this.ctx = ctx;
	}

	public void addWall(Vector from, Vector to) {
		this.ctx.addWall(from, to);
	}

	public abstract void addArea(Vector from, Vector to, String name);

	public abstract R getEnvironment();

}
