package rovu.robotsimulator;

import org.la4j.Vector;


import rovu.userinterface.Observer;
import rovu.userinterface.UI;
import rovu.userinterface.windowUI.WindowUi;

import org.xml.sax.SAXException;
import rovu.centralstation.CentralStation;
import rovu.centralstation.business.BusinessLayerImpl;
import rovu.centralstation.business.domain.DomainModel;
import rovu.centralstation.information.InformationLayer;
import rovu.communication.Connection;
import rovu.communication.Master;
import rovu.communication.Protocol;
import rovu.rover.Rover;
import rovu.centralstation.TimerObservable;

import rovu.userinterface.windowUI.WindowUi;
import simbad.sim.*;

import rovu.communication.Connection;
import rovu.userinterface.UI;
import rovu.userinterface.windowUI.WindowUi;
import simbad.sim.Agent;
import simbad.sim.EnvironmentDescription;


import javax.swing.*;
import javax.vecmath.Color3f;

import javax.vecmath.Vector3d;
import java.awt.Color;


import javax.vecmath.Vector3d;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.awt.Color;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException, SAXException, ParserConfigurationException {
        Observer observerUI = new Observer();

        EnvironmentDescription e = new EnvironmentDescription();
        e.worldSize = 20F;
        e.worldViewPoint = 1;
        Color color = Color.GRAY;

        SimulatorEnvironment ctx     = new SimulatorEnvironment(e);
        Set<Agent> robots = ctx.loadEnvironmentFromXMLFile(new File("assets/simulatorEnvironmentUniversity.xml"));
        SimulatorFactory     factory = new SimulatorFactory(ctx);


        SecondsTimerAgent timer   = new SecondsTimerAgent(new Vector3d(2, 0, 4), "timer");
        File domainModel = new File("assets/domainModelUniversity.xml");
        BusinessLayerImpl                     bl = CentralStation.createBusinessLayer(robots.size(),
                                                                                      domainModel,
                                                                                      new HashMap<>(), timer, observerUI);
        InformationLayer<Map<String, Object>> il = CentralStation.createInformationLayer(bl, timer);

        Map<String,Object> firstMission = new HashMap<>();
        firstMission.put(Protocol.COMMAND, Protocol.Commands.MISSION_ASSIGNMENT);
        // firstMission.put(Protocol.MISSION, new Vector[] {Vector.fromArray(new double[]{0,0})});
        firstMission.put(Protocol.MISSION, new Vector[] {
                Vector.fromArray(new double[]{2, 4}),
                Vector.fromArray(new double[]{0,-5})
        });
        CentralStation                        cs = new CentralStation(il, bl);

        Set<RoverAdapter> roverAdapters = new HashSet<>();

        robots.forEach(robot -> roverAdapters.add(new RoverAdapter((RoverImpl)robot)));

        Set<Rover> rovers = new HashSet<>();
        roverAdapters.forEach(roverAdapter -> rovers.add(Rover.create(msg -> msg, msg -> msg, il, roverAdapter)));

        robots.add(timer);

        SimulatorMonitor<Agent> controller = new SimulatorMonitor<>(robots, ctx.getEnv());
        bl.processData(firstMission);
		UI ui = new UI(timer,il, observerUI);

    }

    /**
     * @param factory
     * @param topLeft
     * @param botRight
     * @param doorWidth
     * @throws Exception
     */
    private static void generateDefaultRoom(SimulatorFactory factory,
                                            Vector topLeft,
                                            Vector botRight,
                                            double doorWidth) throws DoorTooSmallException {
        int x = 0;
        int y = 1;

        double height = topLeft.get(y) - botRight.get(y);
        double width = botRight.get(x) - topLeft.get(x);

        /**
         * get a quarter of the width and height, and remove half of the doorWidth from it
         * that means that extending a wall from the center and out towards a wall, and from the corresponding wall
         * towards the center will create a "door" with a width of doorWidth
         *
         */

        double vert = (height / 4.0) - (doorWidth / 2.0);
        double horiz = (width / 4.0) - (doorWidth / 2.0);
        Vector center = (topLeft.add(botRight)).divide(2.0);

        if ( vert <= 0 || horiz <= 0 ) {
            throw new DoorTooSmallException("Door width is too small, will not fit in the room");
        }

        // top horizontal wall
        factory.addWall(Vector.fromArray(new double[]{topLeft.get(x), topLeft.get(y) + 0.1}),
                        Vector.fromArray(new double[]{botRight.get(x), topLeft.get(y) - 0.1}));
        // bottom horizontal wall
        factory.addWall(Vector.fromArray(new double[]{topLeft.get(x), botRight.get(y) + 0.1}),
                        Vector.fromArray(new double[]{botRight.get(x), botRight.get(y)}));

        // left vertical wall
        factory.addWall(Vector.fromArray(new double[]{topLeft.get(x) - 0.1, topLeft.get(y)}),
                        Vector.fromArray(new double[]{topLeft.get(x) + 0.1, botRight.get(y)}));
        // right vertical wall
        factory.addWall(Vector.fromArray(new double[]{botRight.get(x) - 0.1, topLeft.get(y)}),
                        Vector.fromArray(new double[]{botRight.get(x) + 0.1, botRight.get(y)}));


        //center cross
        // up
        factory.addWall(Vector.fromArray(new double[]{center.get(x) - 0.1, center.get(y)}),
                        Vector.fromArray(new double[]{center.get(x) + 0.1, center.get(y) + vert}));
        // down
        factory.addWall(Vector.fromArray(new double[]{center.get(x) - 0.1, center.get(y)}),
                        Vector.fromArray(new double[]{center.get(x) + 0.1, center.get(y) - vert}));

        // left
        factory.addWall(Vector.fromArray(new double[]{center.get(x) - horiz, center.get(y) + 0.1}),
                        Vector.fromArray(new double[]{center.get(x), center.get(y) - 0.1}));
        // right
        factory.addWall(Vector.fromArray(new double[]{center.get(x) + horiz, center.get(y) + 0.1}),
                        Vector.fromArray(new double[]{center.get(x), center.get(y) - 0.1}));

        // "doors"
        // top
        factory.addWall(Vector.fromArray(new double[]{center.get(x) - 0.1, topLeft.get(y)}),
                        Vector.fromArray(new double[]{center.get(x) + 0.1, topLeft.get(y) - vert}));
        // bottom
        factory.addWall(Vector.fromArray(new double[]{center.get(x) - 0.1, botRight.get(y)}),
                        Vector.fromArray(new double[]{center.get(x) + 0.1, botRight.get(y) + vert}));
        // left
        factory.addWall(Vector.fromArray(new double[]{topLeft.get(x), center.get(y) + 0.1}),
                        Vector.fromArray(new double[]{topLeft.get(x) + horiz, center.get(y) - 0.1}));
        // right
        factory.addWall(Vector.fromArray(new double[]{botRight.get(x), center.get(y) + 0.1}),
                        Vector.fromArray(new double[]{botRight.get(x) - horiz, center.get(y) - 0.1}));
    }


}
