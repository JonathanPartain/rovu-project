package rovu.robotsimulator;

import org.la4j.Vector;
import rovu.util.Pair;

public interface EnvironmentContext {
	void addWall(Vector start, Vector end);
	void addArea(Vector start, Vector end, Pair<Vector, Vector>[] walls);
}
