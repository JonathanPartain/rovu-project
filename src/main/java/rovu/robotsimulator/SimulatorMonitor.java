package rovu.robotsimulator;

import java.util.Set;
import project.AbstractSimulatorMonitor;
import simbad.gui.Simbad;
import simbad.sim.Agent;
import simbad.sim.EnvironmentDescription;

public class SimulatorMonitor <T extends Agent> {
    private Simbad frame;

    public SimulatorMonitor(Set<T> robots, EnvironmentDescription env) {
        robots.forEach((env::add));
        this.frame = new Simbad(env, false);
        this.frame.getWorld().getCanvas3D().getView().setBackClipDistance(200);
        this.frame.getSimulator().setVirtualTimeFactor(1);
    }
}