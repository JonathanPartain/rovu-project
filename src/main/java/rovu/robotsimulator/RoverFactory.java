package rovu.robotsimulator;

import org.la4j.Vector;

import javax.vecmath.Vector3d;

public class RoverFactory {
	public static RoverImpl createRover(final double x, final double y, final String name) {
		final Vector3d pos = new Vector3d(x, 0.0D, y);
		return new RoverImpl(pos, name);
	}

}
