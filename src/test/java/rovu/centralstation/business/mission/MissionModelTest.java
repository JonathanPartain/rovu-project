package rovu.centralstation.business.mission;

import org.junit.Test;
import org.la4j.Vector;

import java.util.*;

import static org.junit.Assert.*;

public class MissionModelTest {

    @Test
    public void updateMission() {
        Queue<Mission> missionQueue = new LinkedList<>();
        List<Vector> objectives = new ArrayList<>();
        int roverId = 0;

        double[] array = {0, 1};
        objectives.add(Vector.fromArray(array));
        objectives.add(Vector.fromArray(array));

        MissionModel model = new MissionModel(missionQueue, 2);
        model.assignMission(new MissionImpl(1, objectives), roverId);

        model.updateMission(roverId);
        assertTrue(model.hasMission(roverId));

        model.updateMission(roverId);
        assertFalse(model.hasMission(roverId));
    }

    @Test
    public void getMissions() {
        Queue<Mission> missionQueue = new LinkedList<>();
        missionQueue.add(new MissionImpl(1, new ArrayList<>()));
        missionQueue.add(new MissionImpl(2, new ArrayList<>()));
        missionQueue.add(new MissionImpl(3, new ArrayList<>()));

        MissionModel model = new MissionModel(missionQueue, 2);
        Iterator<Mission> iteratorMission = model.getMissions();

        int i = 0;
        while (iteratorMission.hasNext()) {
            assertEquals(iteratorMission.next(), ((LinkedList<Mission>) missionQueue).get(i));
            i++;
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void supplyMission() {
        Queue<Mission> missionQueue = new LinkedList<>();
        List<Vector> objectives = new ArrayList<>();

        MissionModel model = new MissionModel(missionQueue, 2);



        model.supplyMission(objectives);
        Iterator<Mission> iteratorMission = model.getMissions();

        Mission mission = iteratorMission.next();
        assertEquals(mission.getObjectives(), objectives);

        // throw error because we do not allow empty missions
        model.supplyMission(new ArrayList<>());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void assignMission() {
        Queue<Mission> missionQueue = new LinkedList<>();
        List<Vector> objectives = new ArrayList<>();
        int roverId = 0;

        MissionModel model = new MissionModel(missionQueue, 2);
        model.assignMission(new MissionImpl(1, objectives), roverId);

        // throw error because the rover already has a mission assigned
        model.assignMission(new MissionImpl(1, objectives), roverId);
    }

    @Test
    public void hasMission() {
        Queue<Mission> missionQueue = new LinkedList<>();
        List<Vector> objectives = new ArrayList<>();
        int roverId = 0;

        MissionModel model = new MissionModel(missionQueue, 2);
        model.assignMission(new MissionImpl(1, objectives), roverId);

        assertTrue(model.hasMission(roverId));
        assertFalse(model.hasMission(roverId+1));
    }

    @Test
    public void getMission() {
        Queue<Mission> missionQueue = new LinkedList<>();
        Mission mission = new MissionImpl(1, new ArrayList<>());
        int roverId = 0;

        MissionModel model = new MissionModel(missionQueue, 2);
        model.assignMission(mission, roverId);

        assertEquals(mission, model.getMissionByRoverId(roverId));
        assertNull(model.getMissionByRoverId(roverId+1));
    }
}