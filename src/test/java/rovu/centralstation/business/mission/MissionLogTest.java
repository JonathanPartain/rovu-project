package rovu.centralstation.business.mission;

import org.junit.Test;
import org.la4j.Vector;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MissionLogTest {

    @Test
    public void getMission() {
        List<Vector> objectives = new ArrayList<>();
        Mission mission = new MissionImpl(1, objectives);

        MissionLog mLog = new MissionLog(mission);

        assertEquals(mission, mLog.getMission());
    }
}