package rovu.centralstation.business.mission.strategy;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.Test;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;
import rovu.centralstation.business.domain.MapFactory;
import rovu.util.Pair;

import java.util.*;

import static org.junit.Assert.*;

public class FastestPathTest {
	private FastestPath fastestPath = new FastestPath();

	@Test
	public void apply() {
		Graph<Vector, DefaultEdge> g = MapFactory.createMap(new BasicVector(new double[]{-10, -10}), new BasicVector(new double[]{4, 4}), 1);
		Vector roverLocation = new BasicVector(new double[]{0, 0});
		List<Vector> MissionObjectives = new ArrayList<>();
		Vector o1 = new BasicVector(new double[]{1, 4});
		Vector o2 = new BasicVector(new double[]{2, 0});
		MissionObjectives.add(o1);
		MissionObjectives.add(o2);

		Pair<List<Vector>, List<Queue<Vector>>> objectivePaths = fastestPath.apply(roverLocation, MissionObjectives, g);

		List<Vector> orderedObjectives = objectivePaths.getFirst();
		List<Queue<Vector>> paths = objectivePaths.getSecond();

		assertSame(orderedObjectives.get(0), o2);
		assertSame(orderedObjectives.get(1), o1);

		assertEquals(2, paths.get(0).size());
		assertEquals(5, paths.get(1).size());
	}

	@Test
	public void apply2() {
		Graph<Vector, DefaultEdge> g = MapFactory.createMap(new BasicVector(new double[]{-10, -10}), new BasicVector(new double[]{4, 4}), 1);
		Vector roverLocation = new BasicVector(new double[]{-5, 3});
		List<Vector> MissionObjectives = new ArrayList<>();
		Vector o1 = new BasicVector(new double[]{0, 0});
		MissionObjectives.add(o1);

		Pair<List<Vector>, List<Queue<Vector>>> objectivePaths = fastestPath.apply(roverLocation, MissionObjectives, g);

		List<Vector> orderedObjectives = objectivePaths.getFirst();
		List<Queue<Vector>> paths = objectivePaths.getSecond();

		System.out.println(orderedObjectives);
		System.out.println(paths);
	}

	@Test
	public void apply3() {
		Graph<Vector, DefaultEdge> g = MapFactory.createMap(new BasicVector(new double[]{-10, -10}), new BasicVector(new double[]{10, 10}), 1);
		Vector roverLocation = new BasicVector(new double[]{4, 4});
		List<Vector> MissionObjectives = new ArrayList<>();
		Vector o1 = new BasicVector(new double[]{5, 5});
		Vector o2 = new BasicVector(new double[]{8, 6});
		MissionObjectives.add(o1);
		MissionObjectives.add(o2);

		Pair<List<Vector>, List<Queue<Vector>>> objectivePaths = fastestPath.apply(roverLocation, MissionObjectives, g);

		List<Vector> orderedObjectives = objectivePaths.getFirst();
		List<Queue<Vector>> paths = objectivePaths.getSecond();

		System.out.println(orderedObjectives);
		System.out.println(paths);
	}
}