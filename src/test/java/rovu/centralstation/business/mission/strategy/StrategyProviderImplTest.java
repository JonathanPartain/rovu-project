package rovu.centralstation.business.mission.strategy;

import org.junit.Test;

import static org.junit.Assert.*;

public class StrategyProviderImplTest {

    @Test
    public void testReturnStrategyExpected() {
        StrategyProvider provider = new StrategyProviderImpl();

        Strategy fastest = provider.getStrategy(StrategyType.FASTEST_PATH);
        Strategy shortest = provider.getStrategy(StrategyType.SHORTEST_PATH);
        Strategy rewarding = provider.getStrategy(StrategyType.REWARDING_PATH);

        assertTrue(fastest instanceof FastestPath);
        assertTrue(shortest instanceof ShortestPath);
        assertTrue(rewarding instanceof RewardingPath);
    }

    @Test
    public void testDoNotDuplicateStrategy() {
        StrategyProvider provider = new StrategyProviderImpl();

        Strategy fastest1, fastest2 ;
        fastest1 = provider.getStrategy(StrategyType.FASTEST_PATH);
        fastest2 = provider.getStrategy(StrategyType.FASTEST_PATH);

        Strategy shortest1, shortest2 ;
        shortest1 = provider.getStrategy(StrategyType.SHORTEST_PATH);
        shortest2 = provider.getStrategy(StrategyType.SHORTEST_PATH);

        Strategy rewarding1, rewarding2 ;
        rewarding1 = provider.getStrategy(StrategyType.REWARDING_PATH);
        rewarding2 = provider.getStrategy(StrategyType.REWARDING_PATH);

        assertEquals(fastest1, fastest2);
        assertEquals(shortest1, shortest2);
        assertEquals(rewarding1, rewarding2);
    }
}