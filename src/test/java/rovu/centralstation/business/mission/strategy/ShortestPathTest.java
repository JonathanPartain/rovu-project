package rovu.centralstation.business.mission.strategy;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.Before;
import org.junit.Test;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;
import rovu.centralstation.business.domain.Area;
import rovu.centralstation.business.domain.LocationMasker;
import rovu.centralstation.business.domain.MapFactory;
import rovu.util.Pair;

import java.awt.*;
import java.util.*;
import java.util.List;

import static org.junit.Assert.*;

public class ShortestPathTest {
	private final ShortestPath shortestPath = new ShortestPath();
	private Graph<Vector, DefaultEdge> currentMap;
	private Graph<Vector, DefaultEdge> originalMap;
	private LocationMasker masker;

	@Before
	public void init() {
		originalMap = MapFactory.createMap(new BasicVector(new double[]{0, 0}), new BasicVector(new double[]{4, 4}), 1);
		shortestPath.setOriginalEnvironment(originalMap);
	}

	private Graph<Vector, DefaultEdge> initMasker() {
		Area area = new Area(new Rectangle(0, 0, 2, 2), "area");
		Set<Area> physicalAreas = new HashSet<>();
		physicalAreas.add(area);
		ArrayList<String> areaNames = new ArrayList<String>();
		areaNames.add(area.getName());
		this.masker = new LocationMasker(areaNames, 1);
		Vector v1 = new BasicVector(new double[]{0, 0});
		Vector v2 = new BasicVector(new double[]{2, 0});
		List<Pair<Vector, Queue<Vector>>> roverToLocationList = new ArrayList<>();
		Queue<Vector> path = new LinkedList<>();
		path.add(v2);
		roverToLocationList.add(new Pair<>(v1, path));
		this.masker.updateMap(physicalAreas, roverToLocationList);
		return this.masker.calculateMap(physicalAreas, originalMap);
	}

	@Test
	public void applySchedulePossible() {
		currentMap = originalMap;
		Vector roverLocation = new BasicVector(new double[]{0, 0});
		List<Vector> MissionObjectives = new ArrayList<>();
		Vector o1 = new BasicVector(new double[]{1, 4});
		Vector o2 = new BasicVector(new double[]{2, 0});
		MissionObjectives.add(o1);
		MissionObjectives.add(o2);

		Pair<List<Vector>, List<Queue<Vector>>> objectivePaths = shortestPath.apply(roverLocation, MissionObjectives, currentMap);

		List<Vector> orderedObjectives = objectivePaths.getFirst();
		List<Queue<Vector>> paths = objectivePaths.getSecond();

		assertSame(orderedObjectives.get(0), o2);
		assertSame(orderedObjectives.get(1), o1);

		assertEquals(2, paths.get(0).size());
		assertEquals(5, paths.get(1).size());
	}

	@Test
	public void applyScheduleImpossible() {
		currentMap = initMasker();

		Vector roverLocation = new BasicVector(new double[]{0, 0});
		List<Vector> MissionObjectives = new ArrayList<>();
		Vector o1 = new BasicVector(new double[]{1, 4});
		Vector o2 = new BasicVector(new double[]{2, 0});
		MissionObjectives.add(o1);
		MissionObjectives.add(o2);

		Pair<List<Vector>, List<Queue<Vector>>> objectivePaths = shortestPath.apply(roverLocation, MissionObjectives, currentMap);

		List<Vector> orderedObjectives = objectivePaths.getFirst();
		List<Queue<Vector>> paths = objectivePaths.getSecond();

		assertSame(orderedObjectives, MissionObjectives);

		for (Queue<Vector> path : paths) {
			assertEquals(0, path.size());
		}
	}
}