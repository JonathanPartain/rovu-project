package rovu.centralstation.business.mission;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class MissionFactoryTest {

    @Test
    public void testMissionIdAreDistinctAscendingOrder() {
        Mission[] missions = new Mission[10];

        for (int i = 0; i < missions.length; i++) {
            missions[i] = MissionFactory.createMission(new ArrayList<>());
        }

        int id = missions[0].getId();
        for (int i = 1; i < missions.length; i++) {
            assertTrue(id < missions[i].getId());
            id = missions[i].getId();
        }
    }
}