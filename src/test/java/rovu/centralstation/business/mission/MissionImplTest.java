package rovu.centralstation.business.mission;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.Test;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;
import rovu.centralstation.business.mission.strategy.Strategy;
import rovu.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import static org.junit.Assert.*;

public class MissionImplTest {

	@Test
	public void getId() {
		final int id = 1;
		List<Vector> objectives = new ArrayList<>();
		Mission mission = new MissionImpl(id, objectives);

		assertEquals(id, mission.getId());
	}

	@Test
	public void getObjectives() {
		final int id = 1;
		List<Vector> objectives = new ArrayList<>();
		Mission mission = new MissionImpl(id, objectives);

		assertEquals(objectives, mission.getObjectives());
	}

	@Test
	public void getStrategy() {
		final int id = 1;
		List<Vector> objectives = new ArrayList<>();
		Strategy strategy = new Strategy() {
			@Override
			public Pair<List<Vector>, List<Queue<Vector>>> apply(Vector roverLocation, List<Vector> MissionObjectives, Graph<Vector, DefaultEdge> currentEnvironment) {
				return null;
			}
		};
		Mission mission = new MissionImpl(id, objectives, strategy);

		assertEquals(strategy, mission.getStrategy());
	}

	@Test
	public void setStrategy() {
		final int id = 1;
		List<Vector> objectives = new ArrayList<>();
		Strategy strategy = new Strategy() {
			@Override
			public Pair<List<Vector>, List<Queue<Vector>>> apply(Vector roverLocation, List<Vector> MissionObjectives, Graph<Vector, DefaultEdge> currentEnvironment) {
				return null;
			}
		};
		Mission mission = new MissionImpl(id, objectives);
		mission.setStrategy(strategy);

		assertEquals(strategy, mission.getStrategy());
	}

	@Test
	public void changeObjectivesOrder() {
		boolean errorCatched = false;

		final int id = 42;
		Vector v1 = new BasicVector(new double[]{4, 4});
		Vector v2 = new BasicVector(new double[]{6, 6});
		Vector v3 = new BasicVector(new double[]{16, 8});
		List<Vector> objectives = new ArrayList<>();
		List<Vector> newOrder = new ArrayList<>();
		List<Vector> newObjectives = new ArrayList<>();

		objectives.add(v1);
		objectives.add(v2);

		newOrder.add(v2);
		newOrder.add(v1);

		newObjectives.add(v1);
		newObjectives.add(v3);

		Mission mission = new MissionImpl(id, objectives);

		try {
			mission.changeObjectivesOrder(newOrder);
		} catch (Exception e) {
			errorCatched = true;
		}

		assertFalse(errorCatched);

		try {
			mission.changeObjectivesOrder(newObjectives);
		} catch (Exception e) {
			errorCatched = true;
		}

		assertTrue(errorCatched);
	}
}