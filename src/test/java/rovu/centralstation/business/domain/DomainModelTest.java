package rovu.centralstation.business.domain;

import org.junit.Test;
import org.la4j.Vector;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static org.junit.Assert.*;

public class DomainModelTest {

    @Test
    public void ConstructorFromFile() throws ParserConfigurationException, SAXException, IOException {
        File origin = new File("src/test/java/rovu/centralstation/business/domain/domainModel.xml");

        DomainModel domainModel = new DomainModel(origin);

        double[] insidePoint = new double[2];
        List<Area> physicalsList = new ArrayList<>(), logicalsList = new ArrayList<>();
        physicalsList.addAll(domainModel.getPhysicalAreas());
        logicalsList.addAll(domainModel.getLogicalAreas());

        Map<String, Set<Area>> categories = domainModel.getCategories();

        assertEquals(1, categories.size());
        assertEquals(2, physicalsList.size());
        assertEquals(1, logicalsList.size());

        assertEquals(1, categories.get("category 1").size());
        Area area = (Area)(categories.get("category 1").toArray()[0]);
        assertEquals("P1", area.getName());

        // Converted from a set to a list for the test purpose so the order may not always be correct (e.g in debug mode)
        assertEquals("P2", physicalsList.get(0).getName());
        insidePoint[0] = 5;
        insidePoint[1] = 5;
        assertTrue(physicalsList.get(0).containsLocation(Vector.fromArray(insidePoint)));

        assertEquals("P1", physicalsList.get(1).getName());
        insidePoint[0] = 2.5;
        insidePoint[1] = 6;
        assertTrue(physicalsList.get(1).containsLocation(Vector.fromArray(insidePoint)));

        assertEquals("L1", logicalsList.get(0).getName());
        insidePoint[0] = 1;
        insidePoint[1] = 1;
        assertTrue(logicalsList.get(0).containsLocation(Vector.fromArray(insidePoint)));

        assertEquals(441, domainModel.getCurrentMap().vertexSet().size());
    }
}