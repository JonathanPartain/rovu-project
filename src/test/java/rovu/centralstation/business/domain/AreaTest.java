package rovu.centralstation.business.domain;

import org.junit.Test;
import org.la4j.Vector;
import rovu.centralstation.business.domain.Area;

import java.awt.*;
import java.awt.geom.Ellipse2D;

import static org.junit.Assert.*;

public class AreaTest {

    @Test
    public void containsLocationWithCircle() {
        // x and y are left top corner of the rectangular boundary
        float x = 0f, y = 0f, radius = 10f;
        float centerX = x + radius/2, centerY = y + radius/2;
        Shape circle = new Ellipse2D.Float(x, y, radius, radius);

        Area     area            = new Area(circle, "circle");
        double[] insidePoint     = {centerX, centerY};
        double[] outsidePoint    = {x, y};
        Vector   insideLocation  = Vector.fromArray(insidePoint);
        Vector   outsideLocation = Vector.fromArray(outsidePoint);

        assertTrue(area.containsLocation(insideLocation));
        assertFalse(area.containsLocation(outsideLocation));
    }

    @Test
    public void containsLocationWithRectangle() {
        float x = 0f, y = 0f, width = 10f, height = 20;
        float centerX = x + width/2, centerY = y + height/2;
        Shape rectangle = new Rectangle.Float(x, y, width, height);

        Area area = new Area(rectangle, "rectangle");
        double[] insidePoint = {centerX, centerY};
        double[] outsidePoint = {x-1, y-1};
        Vector insideLocation = Vector.fromArray(insidePoint);
        Vector outsideLocation = Vector.fromArray(outsidePoint);

        assertTrue(area.containsLocation(insideLocation));
        assertFalse(area.containsLocation(outsideLocation));
    }

    @Test
    public void containsLocationWithPolygon() {
        // isosceles triangle
        int[] xCoordinates = {0, 1, 2}, yCoordinates = {0, 2, 0};
        Shape triangle = new Polygon(xCoordinates, yCoordinates, 3);

        Area area = new Area(triangle, "triangle");
        double[] insidePoint = {1, 1};
        double[] outsidePoint = {1, -2};
        Vector insideLocation = Vector.fromArray(insidePoint);
        Vector outsideLocation = Vector.fromArray(outsidePoint);

        assertTrue(area.containsLocation(insideLocation));
        assertFalse(area.containsLocation(outsideLocation));
    }

    @Test
    public void testGetName() {
        String name = "My Awesome Area";
        Area area = new Area(null, name);

        assertEquals(area.getName(), name);
    }

}