package rovu.centralstation.business.domain;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.Before;
import org.junit.Test;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;
import rovu.centralstation.business.domain.Area;
import rovu.centralstation.business.domain.LocationMasker;
import rovu.centralstation.business.domain.MapFactory;
import rovu.util.Pair;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class LocationMaskerTest {

    private Area area1 = new Area(new Rectangle(0, 0, 10, 10), "area1");
    private Area area2 = new Area(new Rectangle(10, 0, 10, 10), "area2");
    private Set<Area> physicalAreas = new HashSet<>();
    private LocationMasker locationMasker;
    List<Pair<Vector, Queue<Vector>>> roverToLocationList = new ArrayList<>();
    Queue<Vector> path = new LinkedList<>();

    @Before
    public void initialize() {
        physicalAreas.add(area1);
        physicalAreas.add(area2);
        ArrayList<String> areaNames = new ArrayList<String>();
        areaNames.add(area1.getName());
        areaNames.add(area2.getName());
        locationMasker = new LocationMasker(areaNames, 1);

    }

    @Test
    public void calculateMap() {
        Graph<Vector, DefaultEdge> g = MapFactory.createMap(new BasicVector(new double[]{0, 0}), new BasicVector(new double[]{20, 10}), 2);

        Vector roverLocation = new BasicVector(new double[]{4, 4});
        Vector roverPathLocation = new BasicVector(new double[]{6, 6});
        Vector area2location = new BasicVector(new double[]{16, 8});
        path.add(roverPathLocation);
        roverToLocationList.add(new Pair<>(roverLocation, path));

        locationMasker.updateMap(physicalAreas, roverToLocationList);

        Graph<Vector, DefaultEdge> subgraph = locationMasker.calculateMap(physicalAreas, g);

        Set<Vector> locationsInArea1 = subgraph.vertexSet().stream().filter(area1::containsLocation).collect(Collectors.toSet());
        Set<Vector> locationsInArea2 = subgraph.vertexSet().stream().filter(area2::containsLocation).collect(Collectors.toSet());
        Set<Vector> locationsInArea2g = g.vertexSet().stream().filter(area2::containsLocation).collect(Collectors.toSet());

        assertTrue(g.vertexSet().size() > subgraph.vertexSet().size());

        assertTrue(locationsInArea1.isEmpty());

        assertTrue(g.vertexSet().contains(roverLocation));
        assertTrue(g.vertexSet().contains(roverPathLocation));

        assertTrue(locationsInArea2.size() == locationsInArea2g.size());
        assertFalse(locationsInArea2.isEmpty());
        assertTrue(locationsInArea2.contains(area2location) && locationsInArea2g.contains(area2location));

    }

    @Test
    public void calculateMapWithLocationsOutsideOfAreas() {
        Graph<Vector, DefaultEdge> g = MapFactory.createMap(new BasicVector(new double[]{0, 0}), new BasicVector(new double[]{20, 12}), 2);

        Vector roverLocation = new BasicVector(new double[]{4, 4});
        Vector roverPathLocation = new BasicVector(new double[]{6, 6});
        Vector area2location = new BasicVector(new double[]{16, 8});
        Vector outsideAreaLocation = new BasicVector(new double[]{18, 12});
        path.add(roverPathLocation);
        roverToLocationList.add(new Pair<>(roverLocation, path));

        locationMasker.updateMap(physicalAreas, roverToLocationList);

        Graph<Vector, DefaultEdge> subgraph = locationMasker.calculateMap(physicalAreas, g);

        Set<Vector> locationsInArea1 = subgraph.vertexSet().stream().filter(area1::containsLocation).collect(Collectors.toSet());
        Set<Vector> locationsInArea2 = subgraph.vertexSet().stream().filter(area2::containsLocation).collect(Collectors.toSet());
        Set<Vector> locationsInArea2g = g.vertexSet().stream().filter(area2::containsLocation).collect(Collectors.toSet());

        assertTrue(g.vertexSet().size() > subgraph.vertexSet().size());

        assertTrue(locationsInArea1.isEmpty());

        assertTrue(g.vertexSet().contains(roverLocation));
        assertTrue(g.vertexSet().contains(roverPathLocation));

        assertTrue(locationsInArea2.size() == locationsInArea2g.size());
        assertFalse(locationsInArea2.isEmpty());
        assertTrue(locationsInArea2.contains(area2location) && locationsInArea2g.contains(area2location));

        assertTrue(subgraph.vertexSet().contains(outsideAreaLocation));

    }

}