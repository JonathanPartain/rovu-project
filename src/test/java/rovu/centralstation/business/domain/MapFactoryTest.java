package rovu.centralstation.business.domain;

import com.sun.org.apache.xml.internal.serializer.SerializationHandler;
import org.jgrapht.Graph;
import org.jgrapht.graph.AsSubgraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.Test;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;
import rovu.centralstation.business.domain.MapFactory;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class MapFactoryTest {

	@Test
	public void createMap() {
		Vector start = new BasicVector(new double[]{0, 0});
		Vector end = new BasicVector(new double[]{10, 10});
		double density = 2;

		Graph<Vector, DefaultEdge> g = MapFactory.createMap(start, end, density);

		int nbrOfVertices = g.vertexSet().size();
		assertEquals(36, nbrOfVertices);

		//Checks that the nodes has the correct neighbors
		for (Vector v : g.vertexSet()) {
			double vx = v.get(0);
			double vy = v.get(1);
			boolean isOutMost = vx == 0 || vx == 10 || vy == 0 || vy == 10;
			boolean isCorner = (vx == 0 && vy == 0) || (vx == 0 && vy == 10) || (vx == 10 && vy == 0) || (vx == 10 && vy == 10);
			if (isCorner) {
				assertTrue(g.degreeOf(v) == 2);
			} else if (isOutMost) {
				assertTrue(g.degreeOf(v) == 3);
			} else {
				assertTrue(g.degreeOf(v) == 4);
			}
		}
	}

	@Test
	public void createMapWithObstacles() {
		Vector start = new BasicVector(new double[]{0, 0});
		Vector end = new BasicVector(new double[]{3, 3});
		double density = 1;
		Set<Area> obstacles = new HashSet<>();
		obstacles.add(new Area(new Rectangle2D.Double(-1, -1, 2.5, 2.5), "A wall	"));
		Graph<Vector, DefaultEdge> gObstacles = MapFactory.createMapWithObstacle(start, end, density, obstacles);

		// We removed vertices (0,0), (0,1), (1,0) and (1,1)
		int nbrOfVertices = gObstacles.vertexSet().size();
		assertEquals(12, nbrOfVertices);
	}

	@Test
	public void createSubGraph() {
		Vector start = new BasicVector(new double[]{0, 0});
		Vector end = new BasicVector(new double[]{10, 10});
		double density = 2;
		Graph<Vector, DefaultEdge> g = MapFactory.createMap(start, end, density);

		Set<Vector> vertices = g.vertexSet().stream().filter(v -> v.get(0) < 5).collect(Collectors.toSet());
		AsSubgraph<Vector, DefaultEdge> subgraph = new AsSubgraph<>(g, vertices);

		assertEquals(18, subgraph.vertexSet().size());
		assertEquals(27, subgraph.edgeSet().size());
	}
}