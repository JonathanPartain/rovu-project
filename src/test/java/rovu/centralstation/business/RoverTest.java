package rovu.centralstation.business;

import org.junit.Test;
import org.la4j.Vector;
import rovu.centralstation.business.domain.Area;
import rovu.centralstation.business.rover.Rover;

import java.awt.*;
import java.util.LinkedList;
import java.util.Queue;

import static org.junit.Assert.*;

public class RoverTest {

    /*@Test
    public void getSetObjective() {
        double[] objective = {23.0, -8.2};
        Vector objectiveVector = Vector.fromArray(objective);
        final int roverId = 12;
        Rover rover = new Rover(roverId);

        assertNull(rover.getObjective());

        rover.setObjective(objectiveVector);
        assertEquals(objectiveVector, rover.getObjective());
    }*/

   /* @Test
    public void getSetPosition() {
        double[] position = {23.0, -8.2};
        Vector positionVector = Vector.fromArray(position);
        final int roverId = 12;
        Rover rover = new Rover(roverId);

        assertNull(rover.getPosition());

        rover.setPosition(positionVector);
        assertEquals(positionVector, rover.getPosition());
    }
*/
    @Test
    public void getSetCurrentPhysicalArea() {
        // Create Area
        float x         = 0f, y = 0f, width = 10f, height = 20;
        float centerX   = x + width/2, centerY = y + height/2;
        Shape rectangle = new Rectangle.Float(x, y, width, height);
        Area  area      = new Area(rectangle, "rectangle");

        // Create Rover
        Rover     rover   = Rover.create(1);


        assertNull(rover.getCurrentPhysicalArea());

        rover.setCurrentPhysicalArea(area);
        assertEquals(area, rover.getCurrentPhysicalArea());
    }

    /*@Test
    public void getSetFault() {
        final int roverId = 12;
        Rover rover = new Rover(roverId);

        assertNull(rover.getFaultSeverity());

        rover.setFault(Fault.CAMERA);
        assertEquals(Fault.CAMERA, rover.getFaultSeverity());

        rover.setFault(Fault.WHEEL);
        assertEquals(Fault.WHEEL, rover.getFaultSeverity());
    }*/

    @Test
    public void getSetCurrentPath() {
        final int roverId = 12;
        Rover rover = Rover.create(roverId);

        assertTrue(rover.getCurrentPath().isEmpty());

        Queue<Vector> path = new LinkedList<>();
        rover.setCurrentPath(path);
        assertEquals(path, rover.getCurrentPath());
    }

    @Test
    public void getSetPaths() {
        Rover rover = Rover.create(1);

        assertTrue(rover.getPaths().isEmpty());

        Queue<Queue<Vector>> paths = new LinkedList<>();
        rover.setPaths(paths);
        assertEquals(paths, rover.getPaths());
    }

    @Test
    public void getId() {
        final int roverId = 1;
        Rover rover = Rover.create(roverId);

        assertEquals(roverId, rover.getId());
    }
}