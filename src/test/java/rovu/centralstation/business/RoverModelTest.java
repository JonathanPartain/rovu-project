package rovu.centralstation.business;

import org.junit.Test;
import org.la4j.Vector;
import rovu.centralstation.business.rover.Rover;
import rovu.centralstation.business.rover.RoverModel;
import rovu.centralstation.business.rover.RoverObserver;

import java.util.*;

import static org.junit.Assert.*;

public class RoverModelTest {

    // Class use to test the observer pattern, just keep a reference to the notified array
    class MyObserver implements RoverObserver {
        boolean[] notified;

        public MyObserver(boolean[] notified) {
            this.notified = notified;
        }

        @Override
        public void onRoverChange(Rover newState) {
            notified[0] = true;
        }
    }

    @Test
    public void getRover() {
        List<RoverObserver> observers = new ArrayList<>();
        RoverModel model = new RoverModel(observers);

        model.createRover();
        model.createRover();
        int roverId = 1;
        Rover rover = model.getRover(roverId).get();

        assertEquals(roverId, rover.getId());

        /*
        throw null pointer because id are from 0 to 3;
        roverId = 4;
        rover = model.getRover(roverId);
        */
    }

    @Test
    public void getAllRovers() {
        List<RoverObserver> observers = new ArrayList<>();
        int nbrRover = 4;
        RoverModel model = new RoverModel(observers);
        model.createRover();
        model.createRover();
        model.createRover();
        model.createRover();
        Map<Integer, Rover> rovers = model.getAllRovers();

        assertEquals(rovers.size(), nbrRover);

        int[] expectedIds = {0, 1, 2, 3};
        int[] realIds = new int[nbrRover];

        Set<Integer> keys = rovers.keySet();

        int i = 0;
        for ( Integer k : keys ) {
            realIds[i] = k;
            i++;
        }

        assertArrayEquals(expectedIds, realIds);
        for ( int id : expectedIds ) {
            assertEquals(id, rovers.get(id).getId());
        }

    }

    @Test
    public void notifyObservers() {
        List<RoverObserver> observers = new ArrayList<>();
        boolean[] notified = new boolean[1];
        notified[0] = false;
        observers.add(new MyObserver(notified));

        int nbrRover = 1;
        RoverModel model = new RoverModel(observers);
        model.notifyObservers(null);


        assertTrue(notified[0]);
    }

    @Test
    public void subscribe() {
        List<RoverObserver> observers = new ArrayList<>();
        boolean[] notified = new boolean[1];
        notified[0] = false;
        int nbrRover = 1;
        RoverModel model = new RoverModel(observers);
        MyObserver myObserver = new MyObserver(notified);

        model.notifyObservers(null);
        assertFalse(notified[0]);

        model.subscribe(myObserver);
        model.notifyObservers(null);
        assertTrue(notified[0]);
    }

    @Test
    public void unsubscribe() {
        List<RoverObserver> observers = new ArrayList<>();
        boolean[] notified = new boolean[1];
        notified[0] = false;
        MyObserver myObserver = new MyObserver(notified);
        observers.add(myObserver);
        int nbrRover = 1;
        RoverModel model = new RoverModel(observers);

        model.unsubscribe(myObserver);
        model.notifyObservers(null);
        assertFalse(notified[0]);
    }

    @Test
    public void assignPathSchedule() {
        List<RoverObserver> observers = new ArrayList<>();
        RoverModel model = new RoverModel(observers);

        model.createRover();
        Queue<Vector> path = new LinkedList<>();
        Queue<Queue<Vector>> paths = new LinkedList<>();
        model.assignPathSchedule(0, path, paths);

        Rover rover = model.getRover(0).get();

        assertEquals(path, rover.getCurrentPath());
        assertEquals(paths, rover.getPaths());
    }

    /*@Test
    public void getLocations() {
        List<RoverObserver> observers = new ArrayList<>();
        int nbrRover = 2;
        RoverModel model = new RoverModel(observers, nbrRover);

        Rover rover0 = model.getRover(0);
        double[] position0 = {23.0, -8.2};
        Vector positionVector0 = Vector.fromArray(position0);
        rover0.setPosition(positionVector0);

        Rover rover1 = model.getRover(1);
        double[] position1 = {56.2, 152.2};
        Vector positionVector1 = Vector.fromArray(position1);
        rover1.setPosition(positionVector1);

        List<Vector> locations = model.getLocations();

        assertEquals(locations.size(), nbrRover);
        assertEquals(locations.get(0), rover0.getPosition());
        assertEquals(locations.get(1), rover1.getPosition());
    }*/
}