package rovu.centralstation.business;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.Before;
import org.junit.Test;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;
import rovu.centralstation.TimerObservable;
import rovu.centralstation.TimerObserver;
import rovu.centralstation.business.domain.Area;
import rovu.centralstation.business.domain.DomainModel;
import rovu.centralstation.business.domain.LocationMasker;
import rovu.centralstation.business.domain.MapFactory;
import rovu.centralstation.business.mission.Mission;
import rovu.centralstation.business.mission.MissionImpl;
import rovu.centralstation.business.mission.MissionModel;
import rovu.centralstation.business.reward.RewardModel;
import rovu.centralstation.business.rover.Rover;
import rovu.centralstation.business.rover.RoverModel;
import rovu.centralstation.business.rover.RoverObserver;
import rovu.util.Pair;

import java.awt.*;
import java.util.*;
import java.util.List;

import static org.junit.Assert.*;

public class SystemStatusTest {

    private SystemStatus systemStatus;
    private MissionModel missionModel;
    private RewardModel rewardModel;
    private DomainModel domainModel;
    private RoverModel roverModel;

    private final int nbrRovers = 3;

    @Before
    public void initialize() {
        Area                  area1         = new Area(new Rectangle(0, 0, 10, 10), "physical 1");
        Area                  area2         = new Area(new Rectangle(10, 0, 20, 10), "physical 2");
        Area                  area3         = new Area(new Rectangle(5, 5, 7, 7), "logical 1");
        Set<Area>             physicalAreas = new HashSet<>();
        Set<Area>             logicalAreas  = new HashSet<>();
        Map<String,Set<Area>> categories    = new HashMap<>();
        LocationMasker        locationMasker;


        physicalAreas.add(area1);
        physicalAreas.add(area2);

        logicalAreas.add(area3);

        categories.put("category 1", physicalAreas);
        categories.put("category 2", logicalAreas);

        ArrayList<String> areaNames = new ArrayList<String>();
        areaNames.add(area1.getName());
        areaNames.add(area2.getName());
        areaNames.add(area3.getName());
        locationMasker = new LocationMasker(areaNames, 1);

        Graph<Vector, DefaultEdge> map = MapFactory.createMap(new BasicVector(new double[]{-10, -10}), new BasicVector(new double[]{10, 10}), 1);


        this.domainModel = new DomainModel(
                physicalAreas,
                logicalAreas,
                categories,
                map,
                locationMasker
        );

        List<RoverObserver> roverObservers = new ArrayList<>();
        this.roverModel = new RoverModel(roverObservers);
        this.roverModel.createRover();
        this.roverModel.createRover();
        this.roverModel.createRover();

        Rover             rover1  = roverModel.getRover(0).get();
        Rover             rover2  = roverModel.getRover(1).get();
        Rover             rover3  = roverModel.getRover(2).get();
        Vector            pos1    = new BasicVector(new double[]{-5, 3});
        Vector            pos2    = new BasicVector(new double[]{3, 5});
        Vector            pos3    = new BasicVector(new double[]{7, 6});
        Rover.RoverStatus status1 = Rover.createRoverStatus(rover1.getObjective(), pos1, rover1.getFault());
        Rover.RoverStatus status2 = Rover.createRoverStatus(rover2.getObjective(), pos2, rover2.getFault());
        Rover.RoverStatus status3 = Rover.createRoverStatus(rover3.getObjective(), pos3, rover3.getFault());
        rover1.setRoverStatus(status1);
        rover2.setRoverStatus(status2);
        rover3.setRoverStatus(status3);

        roverModel.setCurrentArea(0, area1);
        roverModel.setCurrentArea(1, area1);
        roverModel.setCurrentArea(2, area1);

        Map<String, Integer> pointsPerCategory = new HashMap<>();
        pointsPerCategory.put("category 1", 10);
        pointsPerCategory.put("category 2", 20);
        this.rewardModel = new RewardModel(nbrRovers, pointsPerCategory);

        Queue<Mission> missionQueue = new LinkedList<>();
        List<Vector> objectives = new ArrayList<>();
        this.missionModel = new MissionModel(missionQueue, nbrRovers);

        double[] array = {0, 1};
        objectives.add(Vector.fromArray(array));
        objectives.add(Vector.fromArray(array));
        missionModel.assignMission(new MissionImpl(2, objectives), 1);


        this.systemStatus = new SystemStatus(
                roverModel,
                domainModel,
                rewardModel,
                missionModel,
                new TimerObservable() {
                    @Override
                    public void notifyObservers() {

                    }

                    @Override
                    public void subscribe(TimerObserver timerObserver) {

                    }

                    @Override
                    public void unsubscribe(TimerObserver timerObserver) {

                    }

                    @Override
                    public int getTime() {
                        return 0;
                    }
                }
        );
    }

    @Test
    public void nextObjectiveWhenARoverCurrentPathIsNotEmptyNoDelay() {
        final int roverId = 1;
        Rover rover = roverModel.getRover(roverId).get();
        Queue<Vector> currentPath = new LinkedList<>();
        Vector nextObjective = new BasicVector(new double[]{4, 4});
        currentPath.add(nextObjective);
        rover.setCurrentPath(currentPath);

        Pair<Vector, Integer> result = systemStatus.nextObjective(roverId);

        assertEquals(result.getFirst(), nextObjective);
        assertEquals(0, (int)result.getSecond());
        assertTrue(currentPath.isEmpty());
    }

    @Test
    public void nextObjectiveWhenARoverCurrentPathIsNotEmptyWithDelay() {
        final int roverId = 1;
        Rover rover = roverModel.getRover(roverId).get();
        Queue<Vector> currentPath = new LinkedList<>();
        Vector nextObjective = new BasicVector(new double[]{4, 4});
        currentPath.add(nextObjective);
        rover.setCurrentPath(currentPath);
        rover.setCurrentPhysicalArea(new Area(new Rectangle(-10, -10, -5, -5), "left area"));

        Pair<Vector, Integer> result = systemStatus.nextObjective(roverId);


        assertEquals(result.getFirst(), nextObjective);
        assertEquals(2, (int)result.getSecond());
        assertTrue(currentPath.isEmpty());
    }

    @Test
    public void nextObjectiveWhenARoverCurrentPathIsEmptyButNotPathsNoDelay() {
        final int roverId = 1;
        Rover rover = roverModel.getRover(roverId).get();
        Queue<Vector> emptyCurrentPath = new LinkedList<>();
        Queue<Vector> path = new LinkedList<>();
        Queue<Queue<Vector>> paths = new LinkedList<>();
        Vector nextObjective = new BasicVector(new double[]{4, 4});
        path.add(nextObjective);
        paths.add(path);
        roverModel.assignPathSchedule(roverId, emptyCurrentPath, paths);

        Pair<Vector, Integer> result = systemStatus.nextObjective(roverId);

        assertEquals(result.getFirst(), nextObjective);
        assertEquals(0, (int)result.getSecond());
        Queue<Vector> currentPath = rover.getCurrentPath();
        assertTrue(currentPath.isEmpty());
    }

    @Test
    public void nextObjectiveWhenARoverPathsIsEmptyAndNoMoreMission() {
        final int roverId = 2;
        Rover rover = roverModel.getRover(roverId).get();
        Queue<Vector> emptyCurrentPath = new LinkedList<>();
        Queue<Queue<Vector>> emptyPaths = new LinkedList<>();
        roverModel.assignPathSchedule(roverId, emptyCurrentPath, emptyPaths);

        Pair<Vector, Integer> result = systemStatus.nextObjective(roverId);

        assertNull(result);
    }

    @Test
    public void nextObjectiveWhenARoverPathsIsEmptyAndMissionInQueue() {
        final int roverId = 2;
        Rover rover = roverModel.getRover(roverId).get();
        Queue<Vector> emptyCurrentPath = new LinkedList<>();
        Queue<Queue<Vector>> emptyPaths = new LinkedList<>();
        roverModel.assignPathSchedule(roverId, emptyCurrentPath, emptyPaths);

        Pair<Vector, Integer> result = systemStatus.nextObjective(roverId);

        assertNull(result);
    }

    @Test
    public void nextObjectiveWhenARoverPathsIsEmptyAndOneMissionInTheQueue() {
        final int roverId = 2;
        Rover rover = roverModel.getRover(roverId).get();
        Vector pos = new BasicVector(new double[]{4, 4});
        Rover.RoverStatus status = Rover.createRoverStatus(rover.getObjective(), pos, rover.getFault());
        rover.setRoverStatus(status);
        Queue<Vector> emptyCurrentPath = new LinkedList<>();
        Queue<Queue<Vector>> emptyPaths = new LinkedList<>();
        roverModel.assignPathSchedule(roverId, emptyCurrentPath, emptyPaths);
        Vector objective1 = new BasicVector(new double[]{5, 5});
        Vector objective2 = new BasicVector(new double[]{8, 6});
        List<Vector> missionList = new ArrayList<>();
        missionList.add(objective1);
        missionList.add(objective2);

        missionModel.supplyMission(missionList);

        Pair<Vector, Integer> result = systemStatus.nextObjective(roverId);

        // assertEquals(result.getFirst(), nextObjective);
        assertEquals(0, (int)result.getSecond());
    }

    @Test
    public void nextObjectiveWhenARoverCurrentPathIsEmptyButNotPathsWithDelay() {
        final int roverId = 1;
        Rover rover = roverModel.getRover(roverId).get();
        rover.setCurrentPhysicalArea(new Area(new Rectangle(-10, -10, -5, -5), "left area"));
        Queue<Vector> emptyCurrentPath = new LinkedList<>();
        Queue<Vector> path = new LinkedList<>();
        Queue<Queue<Vector>> paths = new LinkedList<>();
        Vector nextObjective = new BasicVector(new double[]{4, 4});
        path.add(nextObjective);
        paths.add(path);
        roverModel.assignPathSchedule(roverId, emptyCurrentPath, paths);

        Pair<Vector, Integer> result = systemStatus.nextObjective(roverId);

        assertEquals(result.getFirst(), nextObjective);
        assertEquals(2, (int)result.getSecond());
        Queue<Vector> currentPath = rover.getCurrentPath();
        assertTrue(currentPath.isEmpty());
    }

    @Test
    public void awardPoints() {
        // Switch the reward procedure to logical and award 20 points for every robot in a logical area
        systemStatus.awardPoints();
        int[] result = rewardModel.getAllPointsPerRoverId();

        assertEquals(result.length, nbrRovers);

        assertEquals(0, result[0]);
        assertEquals(0, result[1]);
        assertEquals(20, result[2]);

        // Switch the reward procedure to physical and award 10 points for every robot in a physical area
        systemStatus.awardPoints();
        result = rewardModel.getAllPointsPerRoverId();

        assertEquals(result.length, nbrRovers);

        assertEquals(0, result[0]);
        assertEquals(10, result[1]);
        assertEquals(30, result[2]);
    }
}