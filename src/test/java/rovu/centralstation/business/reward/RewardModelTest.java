package rovu.centralstation.business.reward;

import org.junit.Test;
import org.la4j.Vector;
import rovu.centralstation.business.domain.Area;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.*;

import static org.junit.Assert.assertTrue;

public class RewardModelTest {


    @Test
    public void shouldSwitchToLogical(){
        float x = 0f, y = 0f, radius = 10f;
        float centerX = x + radius/2, centerY = y + radius/2;
        Shape circle = new Ellipse2D.Float(x, y, radius, radius);

        Area area = new Area(circle, "circle");

        Set<Area> physicalAreas = new HashSet<>();
        Set<Area> logicalAreas = new HashSet<>();
        logicalAreas.add(area);
        ArrayList roverLocations = new ArrayList<Vector>();
        double[] logicalPosition = {centerX, centerY};
        Vector logicalLocation = Vector.fromArray(logicalPosition);
        roverLocations.add(logicalLocation);

        Map<String, Integer> pointsPerCategory = new HashMap<>();
        pointsPerCategory.put("category 1", 10);
        pointsPerCategory.put("category 2", 20);
        RewardModel rewardModel = new RewardModel(1, pointsPerCategory);

        boolean shouldSwitch = rewardModel.shouldSwitch(roverLocations,physicalAreas,logicalAreas);

        assertTrue(shouldSwitch);
    }

    @Test
    public void shouldSwitchToPhysical(){
        float x = 0f, y = 0f, radius = 10f;
        float centerX = x + radius/2, centerY = y + radius/2;
        Shape circle = new Ellipse2D.Float(x, y, radius, radius);

        Area area = new Area(circle, "circle");

        Set<Area> physicalAreas = new HashSet<>();
        Set<Area> logicalAreas = new HashSet<>();
        physicalAreas.add(area);
        ArrayList roverLocations = new ArrayList<Vector>();
        double[] position = {centerX, centerY};
        Vector location = Vector.fromArray(position);
        roverLocations.add(location);

        Map<String, Integer> pointsPerCategory = new HashMap<>();
        pointsPerCategory.put("category 1", 10);
        pointsPerCategory.put("category 2", 20);
        RewardModel rewardModel = new RewardModel(1, pointsPerCategory); //RewardModel has Physical procedure as current when created
        rewardModel.switchProcedure(); //Therefore switch to make Logical active

        boolean shouldSwitch = rewardModel.shouldSwitch(roverLocations,physicalAreas,logicalAreas);

        assertTrue(shouldSwitch);
    }


    @Test
    public void shouldNotSwitchToPhysical(){
        float x = 0f, y = 0f, radius = 10f;
        float centerX = x + radius/2, centerY = y + radius/2;
        Shape circle = new Ellipse2D.Float(x, y, radius, radius);

        Area area = new Area(circle, "circle");
        Set<Area> physicalAreas = new HashSet<>();
        Set<Area> logicalAreas = new HashSet<>();
        physicalAreas.add(area);
        ArrayList roverLocations = new ArrayList<Vector>();
        //Position outside area
        double[] position = {x, y};
        Vector location = Vector.fromArray(position);
        roverLocations.add(location);

        Map<String, Integer> pointsPerCategory = new HashMap<>();
        pointsPerCategory.put("category 1", 10);
        pointsPerCategory.put("category 2", 20);
        RewardModel rewardModel = new RewardModel(1, pointsPerCategory); //RewardModel has Physical procedure as current when created
        rewardModel.switchProcedure(); //Therefore switch to make Logical active

        boolean shouldSwitch = rewardModel.shouldSwitch(roverLocations,physicalAreas,logicalAreas);

        assertTrue(!shouldSwitch);
    }


    @Test
    public void shouldSwitchAdvanced(){
        Shape circle1 = new Ellipse2D.Float(0f, 0f, 10f, 10f);
        Shape circle2 = new Ellipse2D.Float(3f, 3f, 10f, 10f);
        Shape rectangle1 = new Ellipse2D.Float(5f, 5f, 5f, 5f);
        Shape rectangle2 = new Ellipse2D.Float(10f, 10f, 6f, 10f);
        Area areaC1 = new Area(circle1, "circle1");
        Area areaC2 = new Area(circle2, "circle2");
        Area areaR1 = new Area(rectangle1, "rectangle1");
        Area areaR2 = new Area(rectangle2, "rectangle2");


        Set<Area> physicalAreas = new HashSet<>();
        Set<Area> logicalAreas = new HashSet<>();

        physicalAreas.add(areaC1);
        physicalAreas.add(areaR1);
        logicalAreas.add(areaC2);
        logicalAreas.add(areaR2);

        double[] position = {5f, 5f};
        Vector location = Vector.fromArray(position);
        ArrayList roverLocations = new ArrayList<Vector>();
        roverLocations.add(location);


        Map<String, Integer> pointsPerCategory = new HashMap<>();
        pointsPerCategory.put("category 1", 10);
        pointsPerCategory.put("category 2", 20);
        RewardModel rewardModel = new RewardModel(1, pointsPerCategory);
        boolean shouldSwitch = rewardModel.shouldSwitch(roverLocations,physicalAreas,logicalAreas);

        assertTrue(shouldSwitch); //Test switch to logical
        rewardModel.switchProcedure(); // Switch to logical

        boolean shouldSwitch2 = rewardModel.shouldSwitch(roverLocations,physicalAreas,logicalAreas);
        assertTrue(shouldSwitch2); //Test switch to physical


    }



}
