package rovu.robotsimulator;



import org.junit.Test;
import org.la4j.Vector;

import javax.media.j3d.Transform3D;
import javax.vecmath.Vector3d;

import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.is;


public class RoverAdapterTest {

	@Test
	public void readDirection() {
		Rover roverStub = new Rover() {
			@Override
			public Vector3d getPosition() {
				return null;
			}

			@Override
			public Transform3D getRotation() {
				Transform3D matrix = new Transform3D();
				matrix.rotX(Math.PI);
				matrix.rotZ(Math.PI);
				return matrix;
			}

			@Override
			public double[] readRangeSensorBelt() {
				return new double[0];
			}

			@Override
			public void setTranslationalVelocity(double velocity) {

			}

			@Override
			public void setRotationalVelocity(double velocity) {

			}

			@Override
			public void connect(rovu.rover.Rover softwareConnection) {

			}
		};

		final RoverAdapter ra = new RoverAdapter(roverStub);

		Vector direction = ra.readDirection();

		//assertThat(direction.get(0), is(1.0));
		//assertThat(direction.get(1), is(0.0));
	}
}