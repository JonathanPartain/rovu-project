package rovu.robotsimulator;

import org.junit.Test;

import javax.vecmath.Vector3d;

import static org.junit.Assert.*;

public class SecondsTimerAgentTest {

    @Test
    public void TestSecondsIncreasesEvery20Updates() {
        SecondsTimerAgent timerAgent = new SecondsTimerAgent(new Vector3d(), "timer");

        assertEquals(0, timerAgent.getTime());

        for (int i = 0; i < 20; i++) {
            timerAgent.performBehavior();
        }

        assertEquals(1, timerAgent.getTime());

        for (int i = 0; i < 20; i++) {
            timerAgent.performBehavior();
        }

        assertEquals(2, timerAgent.getTime());
    }
}