package rovu.rover;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import rovu.centralstation.TimerObservable;
import rovu.centralstation.TimerObserver;
import rovu.centralstation.information.InformationLayerImpl;
import rovu.communication.Connection;
import rovu.communication.Master;
import rovu.communication.Slave;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;

import static org.junit.Assert.*;

public class NetworkDeviceTest {

  private NetworkDevice<Integer, Integer> networkDevice;
  private Master<Integer> master;
  private Connection<Integer> connection;
  @Before
  public void initialize() {
    connection = Mockito.mock(InformationLayerImpl.class);

    master = new Master<Integer>() {
      Slave<Integer> slave;
      @Override
      public void connect(Slave<Integer> slave) {
        this.slave = slave;
        slave.connect(connection);
      }
    };
    this.networkDevice = new NetworkDevice<Integer, Integer>(x -> x,
                                                             x -> x,
                                                             new LinkedList<>(),
                                                             master);
  }

  @Test
  public void receive() {
  }

  @Test
  public void send() {
    networkDevice.send(1);

    int x = networkDevice.receive().get();
    assertEquals(x, 1);

  }

  @Test
  public void transmit() {
    networkDevice.transmit(1);

    Mockito.verify(connection).send(1);
  }
}