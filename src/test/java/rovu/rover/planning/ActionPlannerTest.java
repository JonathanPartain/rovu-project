package rovu.rover.planning;

import org.junit.Test;

import org.la4j.Vector;
import rovu.rover.Fault;

import static org.junit.Assert.*;

public class ActionPlannerTest {

    //Used to experiment with PlanSchedule Algorithm faster
    @Test
    public void testCalc() {

        double[] objectiveArray = {2, 4};
        Vector objective = Vector.fromArray(objectiveArray);


        double[] positionArray = {0, 0};
        Vector position = Vector.fromArray(positionArray);

        double[] directionArray = {1, 2};
        Vector direction = Vector.fromArray(directionArray);

        Vector posToObj = objective.subtract(position);


        double cosAngle = posToObj.innerProduct(direction) / (posToObj.norm() * direction.norm());

        cosAngle = Math.ceil(cosAngle * 1000) / 1000;

        double angleBetween = cosAngle != 0
                              ? Math.toDegrees(Math.acos(cosAngle))
                              : 90;



        Vector perpendicular = posToObj.copy();
        perpendicular.swapElements(0, 1);
        perpendicular.set(1, -posToObj.get(0));

        //Position + small movement in the current direction
        Vector posPlusDirection = position.add(direction.divide(1000));

        //Calculate angle that let us now if we should turn left or right
        double tmp = perpendicular.innerProduct(posPlusDirection) / (posPlusDirection.length() * perpendicular.length());
        double tmp2 = Math.toDegrees(Math.acos(tmp));


        //Angle (-180,180]
        if ( (tmp2 > 0 && tmp2 < 90) || tmp2 < -270 ) { // => 90<tmp2<
            // Turn left
            angleBetween *= -1;
        }

    }

    //Test when direction is ortoghonal to the left
    @Test
    public void planOrtLeft() {
        ActionPlanner AP = new ActionPlanner(new FaultSeverityDeterminatior());

        double[] objectiveArray = {0, 1};
        Vector objective = Vector.fromArray(objectiveArray);


        double[] positionArray = {0, 0};
        Vector position = Vector.fromArray(positionArray);

        double[] directionArray = {-1, 0};
        Vector direction = Vector.fromArray(directionArray);


        Schedule s = AP.planSchedule(null, Fault.NONE, objective, position, direction);
        double kbf = s.angle;
        assertEquals(kbf , AP.calculateAngle(-Math.PI/2), 1e-2);

    }

    //Direction is ortoghonal to the right
    @Test
    public void planOrtRight() {
        ActionPlanner AP = new ActionPlanner(new FaultSeverityDeterminatior());

        double[] objectiveArray = {0, 1};
        Vector objective = Vector.fromArray(objectiveArray);


        double[] positionArray = {0, 0};
        Vector position = Vector.fromArray(positionArray);

        double[] directionArray = {1, 0};
        Vector direction = Vector.fromArray(directionArray);


        Schedule s = AP.planSchedule(null, Fault.NONE, objective, position, direction);
        double kbf = s.angle;
        assertEquals(kbf, AP.calculateAngle(Math.PI/2), 1e-1);

    }

    //Direction is towards objective
    @Test
    public void testDirectionCorrect() {
        ActionPlanner AP2 = new ActionPlanner(new FaultSeverityDeterminatior());

        double[] objectiveArray2 = {2, 4};
        Vector objective2 = Vector.fromArray(objectiveArray2);


        double[] positionArray2 = {0, 0};
        Vector position2 = Vector.fromArray(positionArray2);

        double[] directionArray2 = {1, 2};
        Vector direction2 = Vector.fromArray(directionArray2);


        Schedule s2 = AP2.planSchedule(null, Fault.NONE, objective2, position2, direction2);
        double kbf2 = Math.toDegrees(s2.angle);
        assertTrue(kbf2 < 0.01);

    }


    //Direction is 45 degrees to the right
    @Test
    public void testDirection45Right() {
        ActionPlanner AP2 = new ActionPlanner(new FaultSeverityDeterminatior());

        double[] objectiveArray2 = {0, 2};
        Vector objective2 = Vector.fromArray(objectiveArray2);


        double[] positionArray2 = {0, 0};
        Vector position2 = Vector.fromArray(positionArray2);

        double[] directionArray2 = {1, 1};
        Vector direction2 = Vector.fromArray(directionArray2);


        Schedule s2 = AP2.planSchedule(null, Fault.NONE, objective2, position2, direction2);
        double kbf2 = (s2.angle);

        assertEquals(kbf2 , AP2.calculateAngle(Math.PI / 4), 1e-2);

    }

    //Direction is 135 degrees to the right
    @Test
    public void testDirection135Right() {
        ActionPlanner AP2 = new ActionPlanner(new FaultSeverityDeterminatior());

        double[] objectiveArray2 = {0, 2};
        Vector objective2 = Vector.fromArray(objectiveArray2);


        double[] positionArray2 = {0, 0};
        Vector position2 = Vector.fromArray(positionArray2);

        double[] directionArray2 = {1, -1};
        Vector direction2 = Vector.fromArray(directionArray2);


        Schedule s2 = AP2.planSchedule(null, Fault.NONE, objective2, position2, direction2);
        double kbf2 = s2.angle;

        assertEquals(kbf2, AP2.calculateAngle(Math.PI * 3 / 4), 0.01);

    }

    //Direction is 45 degrees to the left
    @Test
    public void testDirection45Left() {
        ActionPlanner AP2 = new ActionPlanner(new FaultSeverityDeterminatior());

        double[] objectiveArray2 = {0, 2};
        Vector objective2 = Vector.fromArray(objectiveArray2);


        double[] positionArray2 = {0, 0};
        Vector position2 = Vector.fromArray(positionArray2);

        double[] directionArray2 = {-1, 1};
        Vector direction2 = Vector.fromArray(directionArray2);


        Schedule s2 = AP2.planSchedule(null, Fault.NONE, objective2, position2, direction2);
        double kbf2 = s2.angle;
        assertEquals(kbf2, AP2.calculateAngle(-Math.PI / 4), 1e-2);

    }

    //Direction is 135 degrees to the left
    @Test
    public void testDirection135Left() {
        ActionPlanner AP2 = new ActionPlanner(new FaultSeverityDeterminatior());

        double[] objectiveArray2 = {0, 2};
        Vector objective2 = Vector.fromArray(objectiveArray2);


        double[] positionArray2 = {0, 0};
        Vector position2 = Vector.fromArray(positionArray2);

        double[] directionArray2 = {-1, -1};
        Vector direction2 = Vector.fromArray(directionArray2);


        Schedule s2 = AP2.planSchedule(null, Fault.NONE, objective2, position2, direction2);
        double kbf2 = s2.angle;
        assertEquals(kbf2 , AP2.calculateAngle(-Math.PI * 3 / 4), 0.01);


    }


    //Direction is 180 degrees from the objective (opposite)
    @Test
    public void testDirectionOposite() { //Direction is towards objective
        ActionPlanner AP2 = new ActionPlanner(new FaultSeverityDeterminatior());

        double[] objectiveArray2 = {0, 2};
        Vector objective2 = Vector.fromArray(objectiveArray2);


        double[] positionArray2 = {0, 0};
        Vector position2 = Vector.fromArray(positionArray2);

        double[] directionArray2 = {0, -2};
        Vector direction2 = Vector.fromArray(directionArray2);


        Schedule s2 = AP2.planSchedule(null, Fault.NONE, objective2, position2, direction2);
        double kbf2 = s2.angle;
        assertEquals(kbf2, AP2.calculateAngle(Math.PI), 0.01);

    }

}