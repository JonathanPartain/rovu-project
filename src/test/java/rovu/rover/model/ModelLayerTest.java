package rovu.rover.model;

import org.junit.Before;
import org.junit.Test;
import org.la4j.Vector;
import org.mockito.invocation.InvocationOnMock;
import rovu.communication.Protocol;

import rovu.rover.Fault;
import rovu.rover.information.CommunicationInterpreter;
import rovu.rover.information.FaultInterpreter;
import rovu.rover.information.InformationLayer;
import rovu.rover.information.SenseInterpreter;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

//import static org.mockito.Mockito.*;

public class ModelLayerTest {
	private InformationLayer informationLayer;
	private ModelLayer modelLayer;
	RoverModel rm;

	@Before
	public void buildUp() {
		rm = new RoverModelImpl(new Boolean[]{true},
		                        Vector.fromArray(new double[]{1.0, 0.0}),
		                        Vector.fromArray(new double[]{0.0, -1.0}));
		modelLayer = new ModelLayer(rm, null);

		informationLayer = new InformationLayer(new SenseInterpreter() {
			@Override
			public Boolean[] readEnvironment() {
				return new Boolean[]{false};
			}

			@Override
			public Vector getPosition() {
				return Vector.fromArray(new double[]{0.0, 1.0});
			}

			@Override
			public Vector getDirection() {
				return Vector.fromArray(new double[]{0.0, 1.0});
			}
		}, new FaultInterpreter() {
			@Override
			public Fault read() {
				return Fault.CAMERA;
			}
		}, new CommunicationInterpreter() {
			@Override
			public Optional<Map<String,Object>> readNetworkBuffer() {
				Vector vec = Vector.fromArray(new double[]{1.0, 0.0});
				Integer id = 2;
				Map<String,Object> map = new HashMap<>();
				map.put(Protocol.ROVER_ID, id);
				map.put(Protocol.OBJECTIVE, vec);
				return Optional.of(map);
			}
		}, modelLayer);


	}

	@Test
	public void handleInformation() {

		assertThat(rm.getId().isPresent(), is(false));
		assertThat(rm.getEnvironmentStatus(), is(new Boolean[]{true}));
		assertThat(rm.getFaultStatus().get(), is(Fault.NONE));
		assertThat(rm.getPosition(), is(Vector.fromArray(new double[]{1.0, 0.0})));
		assertThat(rm.getDirection(), is(Vector.fromArray(new double[]{0, -1})));
		assertThat(rm.getObjective().isPresent(), is(false));

		// Double call in order to first set id, and then accept incoming messages.
		informationLayer.handleInformation(null);
		informationLayer.handleInformation(null);

		assertThat(rm.getId().isPresent(), is(true));
		assertThat(rm.getId().get(), is(2));
		assertThat(rm.getEnvironmentStatus(), is(new Boolean[]{false}));
		assertThat(rm.getFaultStatus().get(), is(Fault.CAMERA));
		assertThat(rm.getPosition(), is(Vector.fromArray(new double[]{0, 1})));
		assertThat(rm.getDirection(), is(Vector.fromArray(new double[]{0, 1})));
		assertThat(rm.getObjective().isPresent(), is(true));
		assertThat(rm.getObjective().get(), is(Vector.fromArray(new double[]{1.0, 0.0})));
	}
}